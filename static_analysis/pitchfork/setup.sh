#!/bin/bash
# Pitchfork build script
# Henry Zhu, UPenn, July 2018
# Nik Sultana, UPenn, June 2019
#
# Copyright 2021 University of Pennsylvania
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

program_name="pitchfork"
cmake_flags="-DLLVM_USE_LINKER=gold -DCMAKE_BUILD_TYPE=Debug -DBUILD_SHARED_LIBS=true"

#######################################
# LLVM/CLANG
#######################################
#references to this directory
TESTS_DIR=$(pwd)/tests
PROGRAM_DIR=$(pwd)

LLVM_VERSION="6.0.0" #version of llvm to fetch

LLVM_DOWNLOADS_PAGE=http://releases.llvm.org/${LLVM_VERSION}

LLVM_UNCOMPRESSED_FOLDER=llvm-${LLVM_VERSION}.src
LLVM_TAR_SOURCE=${LLVM_UNCOMPRESSED_FOLDER}.tar.xz
LLVM_SOURCE_PAGE=${LLVM_DOWNLOADS_PAGE}/${LLVM_TAR_SOURCE}

CLANG_UNCOMPRESSED_FOLDER=cfe-${LLVM_VERSION}.src
CLANG_TAR_SOURCE=${CLANG_UNCOMPRESSED_FOLDER}.tar.xz
CLANG_SOURCE_PAGE=${LLVM_DOWNLOADS_PAGE}/${CLANG_TAR_SOURCE}

CLANG_TOOLS_EXTRA_UNCOMPRESSED_FOLDER=clang-tools-extra-${LLVM_VERSION}.src
CLANG_TOOLS_EXTRA_TAR_SOURCE=${CLANG_TOOLS_EXTRA_UNCOMPRESSED_FOLDER}.tar.xz
CLANG_TOOLS_EXTRA_SOURCE_PAGE=${LLVM_DOWNLOADS_PAGE}/${CLANG_TOOLS_EXTRA_TAR_SOURCE}

print_usage() {
	echo "Usage:"
	echo "./setup.sh CTAGS"
	echo "./setup.sh SETUP \"DIR\" where DIR is the directory that you wish to install ${program_name}"
	echo "./setup.sh REBUILD \"DIR\" where DIR is the directory that you have installed ${program_name}"
	echo "./setup.sh YCM \"DIR\" where DIR is the directory where you've installed ${program_name}"
}

#check for clang's path
if [ "$#" -ge 1 ]; then
	clang_path="$2"
	clang_extra_path=$clang_path/llvm/tools/clang/tools/extra
	clang_cmake_lists_path=$clang_extra_path/CMakeLists.txt
	clang_program_path=$clang_extra_path/$program_name
	clang_build_path=$clang_path/build
	clang_bin_path=$clang_build_path/bin

	if [ "$1" = "SETUP" ]; then
		if [ ! "$#" -ge 2 ]
		then
			print_usage
			exit 1
		fi

		#make clang folder
		mkdir $clang_path
		mkdir $clang_build_path
		mkdir $clang_bin_path

		#fetch llvm/clang source code and uncompress into correct directories
		cd $clang_path; \
		wget $LLVM_SOURCE_PAGE; \
		tar -xvf $LLVM_TAR_SOURCE; \
		mv $LLVM_UNCOMPRESSED_FOLDER llvm; \
		cd llvm/tools; \

		wget $CLANG_SOURCE_PAGE; \
		tar -xvf $CLANG_TAR_SOURCE; \
		mv $CLANG_UNCOMPRESSED_FOLDER clang; \
		cd clang/tools; \

		wget $CLANG_TOOLS_EXTRA_SOURCE_PAGE; \
		tar -xvf $CLANG_TOOLS_EXTRA_TAR_SOURCE; \
		mv $CLANG_TOOLS_EXTRA_UNCOMPRESSED_FOLDER extra; \

		#make links to this directory
		ln -s $PROGRAM_DIR $clang_program_path

		#add to CMakeList.txt the program
		echo "add_subdirectory(${program_name})" >> $clang_cmake_lists_path

		#make links to tools in /build/bin directory
		#FIXME currently copies files
		for file in $TESTS_DIR/*; do
			cp $file $clang_bin_path
		done

		#fetch ninja and compile it
		cd $clang_path; \
		git clone https://github.com/ninja-build/ninja.git; \
		cd ninja; \
		git checkout release; \
		./configure.py --bootstrap; \
		cp -f ninja $clang_build_path; \

		cd $clang_build_path; \
		cmake -G Ninja ../llvm ${cmake_flags}; \
		./ninja clang; \
		./ninja $program_name; \

	elif [ "$1" = "REBUILD" ]; then
		if [ ! "$#" -ge 2 ]
		then
			print_usage
			exit 1
		fi

		cd $clang_build_path; \
		cmake -G Ninja ../llvm ${cmake_flags}; \
		./ninja $program_name; \

	elif [ "$1" = "CTAGS" ]; then
		ctags -R --exclude=.git $PROGRAM_DIR
		mv tags $PROGRAM_DIR
		ls $PROGRAM_DIR/tags

	elif [ "$1" = "YCM" ]; then
		mkdir -p $PROGRAM_DIR/build
		cd $PROGRAM_DIR/build
		ln -s $clang_build_path/compile_commands.json

	else
		print_usage
	fi
else
	print_usage
fi
