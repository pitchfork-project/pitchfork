/*
   Copyright 2021 University of Pennsylvania
   Pitchfork project. Nik Sultana, UPenn, 2019

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
#pragma once

#include "clang/AST/AST.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/AST/Stmt.h"
#include "clang/ASTMatchers/ASTMatchFinder.h"
#include "clang/Basic/Diagnostic.h"
#include "clang/Basic/DiagnosticOptions.h"
#include "clang/Basic/FileManager.h"
#include "clang/Basic/SourceManager.h"
#include "clang/Basic/TargetInfo.h"
#include "clang/Basic/TargetOptions.h"
#include "clang/Basic/Version.h"
#include "clang/Frontend/ASTConsumers.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/FrontendActions.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Tooling/Tooling.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Support/FileSystem.h"
#include "llvm/Support/Signals.h"
#include "llvm/Support/raw_ostream.h"

#include <queue>
#include <tuple>
#include <vector>

using namespace llvm;

const int CP_MAX_CODELINE_LENGTH = 50;

extern cl::list<std::string> CompartName;
extern cl::list<std::string> CompartUID;
extern cl::list<std::string> CompartGID;
extern cl::list<std::string> CompartChroot;
extern cl::opt<std::string> MainCompartment;

extern cl::opt<bool> Parse;

extern cl::OptionCategory PitchforkCategory;
extern cl::extrahelp CommonHelp;
extern cl::extrahelp MoreHelp;

extern cl::opt<std::string> PathToDemarshallingScript;
extern cl::opt<std::string> DemarshallingSysPath;
extern cl::opt<std::string> InterfaceGenerationOutputScript;
extern cl::opt<bool> OnlyListOutputs;
extern cl::opt<std::string> OutputDirectory;
extern cl::opt<bool> Overwrite;
extern cl::opt<bool> Quiet;

extern cl::opt<bool> FilterInputFiles;

void PrintVersion(raw_ostream& OS);
