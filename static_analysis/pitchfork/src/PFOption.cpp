/*
   Copyright 2021 University of Pennsylvania
   Pitchfork project. Nik Sultana, UPenn, 2019

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
#include "PFOption.h"

static const std::string PitchforkVersion("0.1");

cl::OptionCategory PitchforkCategory("Pitchfork options");
cl::extrahelp CommonHelp(clang::tooling::CommonOptionsParser::HelpMessage);
cl::extrahelp MoreHelp("\nAutomated program splitting\n");

cl::list<std::string> CompartName(
    "compart", cl::desc("Declare compartment"),
    cl::cat(PitchforkCategory));
cl::list<std::string> CompartUID(
    "compart_uid", cl::desc("Declare compartment's UID"),
    cl::cat(PitchforkCategory));
cl::list<std::string> CompartGID(
    "compart_gid", cl::desc("Declare compartment's GID"),
    cl::cat(PitchforkCategory));
cl::list<std::string> CompartChroot(
    "compart_chroot", cl::desc("Declare compartment's chroot"),
    cl::cat(PitchforkCategory));
cl::opt<std::string> MainCompartment(
    "compart_main", cl::desc("Main compartment"),
    cl::cat(PitchforkCategory));

cl::opt<bool> Parse("parse",
                    cl::desc("Simply check that the source code can be parsed"),
                    cl::init(false), cl::cat(PitchforkCategory));

cl::opt<bool> ShowStacks(
    "show-stacks", cl::desc("Show stacks when reachability is determined"),
    cl::init(false), cl::cat(PitchforkCategory));

cl::opt<bool> ShowTraces(
    "show-traces", cl::desc("Show traces when reachability is determined"),
    cl::init(false), cl::cat(PitchforkCategory));

cl::opt<bool> NoStackDeduplication("no-stack-deduplication",
                                   cl::desc("Don't deduplicate stacks"),
                                   cl::init(false), cl::cat(PitchforkCategory));

cl::opt<bool> StrictStackFactoring(
    "strict-stack-factoring",
    cl::desc("Do consider traces when factoring out duplicate stacks"),
    cl::init(false), cl::cat(PitchforkCategory));

cl::opt<bool> NoStackSubsumption("no-stack-subsumption",
                                 cl::desc("Don't filter out subsumed stacks"),
                                 cl::init(false), cl::cat(PitchforkCategory));

cl::opt<bool> StrictStackSubsumption(
    "strict-stack-subsumption",
    cl::desc("Do consider traces when subsuming stacks"), cl::init(false),
    cl::cat(PitchforkCategory));

cl::opt<std::string> PathToDemarshallingScript(
    "demarshalling_generator", cl::desc("Path to demarshalling generator script"),
    cl::cat(PitchforkCategory));

cl::opt<std::string> DemarshallingSysPath(
    "demarshalling_syspath", cl::desc("Path of demarshalling system dependencies"),
    cl::cat(PitchforkCategory));

cl::opt<std::string> InterfaceGenerationOutputScript(
    "interface_generator", cl::desc("Interface generator script to be generated"),
    cl::cat(PitchforkCategory));

cl::opt<bool> OnlyListOutputs(
    "only-list-outputs",
    cl::desc("Only list outputs that would be produced, without producing them"), cl::init(false),
    cl::cat(PitchforkCategory));

cl::opt<std::string> OutputDirectory(
    "output_directory", cl::desc("Directory that is to contain output files"),
    cl::cat(PitchforkCategory));

cl::opt<bool> Overwrite(
    "overwrite",
    cl::desc("Overwrite output files if they already exist"), cl::init(false),
    cl::cat(PitchforkCategory));

cl::opt<bool> Quiet(
    "quiet",
    cl::desc("Restrict output to stdout (but not to stderr)"), cl::init(false),
    cl::cat(PitchforkCategory));

cl::opt<bool> FilterInputFiles(
    "filter-input-files",
    cl::desc("Filter out source files involving compartmentalisation"), cl::init(false),
    cl::cat(PitchforkCategory));

void PrintVersion(raw_ostream& OS) {
  OS << "Pitchfork v" << PitchforkVersion << " on "
     << clang::getClangFullVersion() << '\n';
}
