/*
   Copyright 2021 University of Pennsylvania
   Pitchfork project. Nik Sultana, UPenn, 2019

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
#include "PFMatcher.h"
#include "PFCtx.h"
#include "PFHelper.h"
#include "PFOption.h"

namespace pitchfork {
using namespace clang;

using namespace clang::ast_matchers;

void ResFunctionFinder::run(const MatchFinder::MatchResult &Result) {
  if (const CallExpr *FS = Result.Nodes.getNodeAs<clang::CallExpr>(label)) {
    clang::SourceManager *SM = Result.SourceManager;
    clang::LangOptions LO;

    std::string compart_name = Lexer::getSourceText(
     CharSourceRange::getTokenRange(FS->getArg(0)->getSourceRange()), *SM, LO);
#if PF_VERBOSE
    outs() << FS->getCalleeDecl()->getAsFunction()->getName() << "|" << FS->getNumArgs() << "| -> " << compart_name << "\n";
#endif // PF_VERBOSE

    ResFunctionStmts_p->push_back(FS);
  }
}

StatementMatcher ResFunctionFinder::makeMatcher(std::string to_match) {
  return callExpr(callee(functionDecl(hasName(to_match)))).bind(label);
}

}  // namespace pitchfork
