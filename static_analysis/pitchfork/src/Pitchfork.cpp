/*
   Copyright 2021 University of Pennsylvania
   Pitchfork project. Nik Sultana, UPenn, 2019

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#include <unordered_set>
#include <boost/filesystem.hpp>

#include "PFCtx.h"
#include "PFHelper.h"
#include "PFMatcher.h"
#include "PFOption.h"

#include "clang/AST/AST.h"
#include "clang/AST/Stmt.h"
#include "clang/ASTMatchers/ASTMatchFinder.h"
#include "clang/Basic/Module.h"
#include "clang/Basic/SourceManager.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/FrontendActions.h"
#include "clang/Lex/PPCallbacks.h"
#include "clang/Parse/ParseAST.h"
#include "clang/Rewrite/Core/Rewriter.h"
#include "clang/Tooling/Core/Replacement.h"
#include "clang/Tooling/JSONCompilationDatabase.h"
#include "clang/Tooling/Refactoring.h"

using namespace clang;
using namespace clang::ast_matchers;

// FIXME move a bunch of declarations out of this file

std::unordered_set<std::string> global_output_files;

struct PitchforkState {
  std::vector<SourceRange> PF_Include_Range;
  std::list<std::string> FileIncludes;
  std::vector<const Stmt *> StartStmts;
  std::vector<const Stmt *> EndStmts;
  std::vector<const FunctionDecl *> Functions;
  LangOptions lang_opt;
  clang::SourceManager *SM;

  std::string global_header_contents;
  boost::filesystem::path global_header_file_path;
  std::string pitchfork_init_contents;
  boost::filesystem::path pitchfork_init_file_path;
  bool updated_main = false;
  boost::filesystem::path global_header_core_file_path;
  std::string global_header_core_contents;
  boost::filesystem::path pitchfork_init_core_file_path;
  std::string pitchfork_init_core_contents;

  std::list<std::string> type_decls; // FIXME remove this and instead generate only the listing of relevant type declarations for each *_input.c file
};

class FunctionEnumerator : public MatchFinder::MatchCallback {
  std::string label;
  PitchforkState *state = nullptr;

 public:
  FunctionEnumerator() = delete;
  FunctionEnumerator(PitchforkState *state, std::string l) : label(l), state(state){};

  virtual void run(const MatchFinder::MatchResult &Result);

  DeclarationMatcher makeMatcher();
};

struct Compart {
  std::string name;
  std::string uid;
  std::string gid;
  std::string chroot;
};
std::map<std::string,Compart> declared_compartments;
void parse_compartment_declarations(void) {
  std::map<std::string,Compart>::iterator it;

  bool found_main_compartment = false;
  for (std::string compart_name : CompartName) {
    size_t idx = compart_name.find(":");
    if (idx != std::string::npos) {
      errs().changeColor(raw_ostream::RED); // FIXME apply this to more instances of errs()
      errs() << "Compartment '" << compart_name << "' isn't a valid name -- cannot contain ':'\n";
      errs().resetColor();
      exit(1); // FIXME vary return codes
    }

    it = declared_compartments.find(compart_name);
    if (it == declared_compartments.end()) {
      Compart c;
      c.name = compart_name;
      declared_compartments[compart_name] = c;
    } else {
      errs().changeColor(raw_ostream::RED); // FIXME apply this to more instances of errs()
      errs() << "Compartment '" << compart_name << "' declared more than once\n";
      errs().resetColor();
      exit(1); // FIXME vary return codes
    }

#if PF_VERBOSE
    outs() << "Compart=" << compart_name;
#endif // PF_VERBOSE
    if (MainCompartment == compart_name) {
      assert(!found_main_compartment);
#if PF_VERBOSE
      outs() << " (main)\n";
#endif // PF_VERBOSE
      found_main_compartment = true;
    } else {
#if PF_VERBOSE
      outs() << "\n";
#endif // PF_VERBOSE
    }
  }

  if (!found_main_compartment) {
    errs() << "Main compartment (-" << MainCompartment.ArgStr << " " << MainCompartment << ") was not one of the declared compartments.\n";
    exit(1); // FIXME vary return codes
  }

  for (std::string compart_uid : CompartUID) {
    size_t idx = compart_uid.find(":");
    if (idx == std::string::npos) {
      errs() << "Compartment UID '" << compart_uid << "' doesn't contain a ':'-terminated compartment name\n";
      exit(1); // FIXME vary return codes
    }
    std::string compart_name = compart_uid.substr(0, idx);
    std::string uid = compart_uid.substr(idx + 1, compart_uid.length());
#if PF_VERBOSE
    outs() << "Compart=" << compart_name << " UID=" << uid + "\n";
#endif // PF_VERBOSE
    it = declared_compartments.find(compart_name);
    if (it == declared_compartments.end()) {
      errs() << "Compartment '" << compart_name << "' in compart_uid '" << compart_uid << "' not previously declared\n";
      exit(1); // FIXME vary return codes
    } else {
      declared_compartments[compart_name].uid = uid;
    }
  }

  for (std::string compart_gid : CompartGID) {
    size_t idx = compart_gid.find(":");
    if (idx == std::string::npos) {
      errs() << "Compartment GID '" << compart_gid << "' doesn't contain a ':'-terminated compartment name\n";
      exit(1); // FIXME vary return codes
    }
    std::string compart_name = compart_gid.substr(0, idx);
    std::string gid = compart_gid.substr(idx + 1, compart_gid.length());
#if PF_VERBOSE
    outs() << "Compart=" << compart_name << " GID=" << gid + "\n";
#endif // PF_VERBOSE
    it = declared_compartments.find(compart_name);
    if (it == declared_compartments.end()) {
      errs() << "Compartment '" << compart_name << "' in compart_gid '" << compart_gid << "' not previously declared\n";
      exit(1); // FIXME vary return codes
    } else {
      declared_compartments[compart_name].gid = gid;
    }
  }

  for (std::string compart_chroot : CompartChroot) {
    size_t idx = compart_chroot.find(":");
    if (idx == std::string::npos) {
      errs() << "Compartment Chroot '" << compart_chroot << "' doesn't contain a ':'-terminated compartment name\n";
      exit(1); // FIXME vary return codes
    }
    std::string compart_name = compart_chroot.substr(0, idx);
    std::string chroot = compart_chroot.substr(idx + 1, compart_chroot.length());
#if PF_VERBOSE
    outs() << "Compart=" << compart_name << " Chroot=" << chroot + "\n";
#endif // PF_VERBOSE
    it = declared_compartments.find(compart_name);
    if (it == declared_compartments.end()) {
      errs() << "Compartment '" << compart_name << "' in compart_chroot '" << compart_chroot << "' not previously declared\n";
      exit(1); // FIXME vary return codes
    } else {
      declared_compartments[compart_name].chroot = chroot;
    }
  }

  bool failed = false;
  for (const std::pair<std::string,Compart> &entry : declared_compartments) {
    if (entry.second.uid.empty()) {
      failed = true;
      errs() << "Compartment '" << entry.first << "' is missing uid spec\n";
    }
    if (entry.second.gid.empty()) {
      failed = true;
      errs() << "Compartment '" << entry.first << "' is missing gid spec\n";
    }
    if (entry.second.chroot.empty()) {
      failed = true;
      errs() << "Compartment '" << entry.first << "' is missing chroot spec\n";
    }
  }

  if (failed) {
    exit(1); // FIXME vary return codes
  }
}

void FunctionEnumerator::run(const MatchFinder::MatchResult &Result) {
  if (const FunctionDecl *FS =
          Result.Nodes.getNodeAs<clang::FunctionDecl>(label)) {
    if (FS->hasBody()) {
#if PF_VERBOSE
    clang::SourceManager *SM = Result.SourceManager;
    clang::SourceLocation begin(FS->getLocStart());
    clang::SourceLocation pre_end(FS->getLocEnd());
    clang::SourceLocation end(
        clang::Lexer::getLocForEndOfToken(pre_end, 0, *SM, state->lang_opt));
    std::string desc = "\"" + FS->getNameAsString() + "\" (" + label + ")";
      outs() << "Found " << desc << " at " << begin.printToString(*SM)
             << " (running until " << end.printToString(*SM) << ")\n";
#endif // PF_VERBOSE
      state->Functions.push_back(FS);
#if PF_VERBOSE > 2
      if (nullptr != BeginFunctionStmt) {
        errs() << "warning: Found multiple instances matching " << desc << ". "
               << "Keeping the first one found\n";
      } else {
        BeginFunctionStmt = FS->getBody();
        BeginFunctionSM = SM;
      }
#endif // PF_VERBOSE
    } else {
#if PF_VERBOSE > 2
      outs() << "(Only a declaration) Found " << desc << " at "
             << begin.printToString(*SM) << " (to " << end.printToString(*SM)
             << ")\n";
#endif // PF_VERBOSE
    }
  }
}

DeclarationMatcher FunctionEnumerator::makeMatcher() {
  return functionDecl().bind(label);
}

enum class Modifier {None, Address, Deref};

std::string decl_to_string (const VarDecl* vdecl, Modifier modifier = Modifier::None, bool with_types = true) {
  clang::ASTContext& ctx = vdecl->getASTContext();
  QualType ty = vdecl->VarDecl::getType().getCanonicalType();
  std::string ty_s = ty.getAsString(ty.split(), ctx.getPrintingPolicy());

  std::string result;
  if (with_types) {
    result += ty_s + " ";
  }

  switch (modifier) {
  case Modifier::Address:
    result += "&";
    break;
  case Modifier::Deref:
    result += "*";
    break;
  case Modifier::None:
    break;
  }

  result += vdecl->getNameAsString();

  return result;
}

std::string decls_to_string (std::unordered_set<const VarDecl*> decls, Modifier modifier = Modifier::None, bool with_types = true) {
  std::string result;
  for (auto it = decls.begin(); it != decls.end(); ++it) {
    if (it != decls.begin()) {
      result += ", ";
    }
    result += decl_to_string(*it, modifier, with_types);
  }
  return result;
}

std::unordered_set<const VarDecl*> free_variables (const std::unordered_set<const VarDecl*> &occurrences, const std::unordered_set<const VarDecl*> &declarations) {
  std::unordered_set<const VarDecl*> result;

  // FIXME inefficient
  for (const VarDecl* occur : occurrences) {
    bool found = false;
    for (const VarDecl* decl : declarations) {
      if (decl->getNameAsString() == occur->getNameAsString()) {
        found = true;
        break;
      }
    }

    if (!found) {
      result.insert(occur);
    }
  }

  return result;
}

void analyse_expr (PitchforkState *state, const Expr *expr, std::unordered_set<const VarDecl*> *occurrences) {
#if 0
  errs() << "eee:" << expr->getStmtClassName() << "\n";
  if (expr != nullptr) {
    errs() << pitchfork::PFHelper::summarise_sourcecode(state->lang_opt, state->SM, 80, expr) << "\n";
  }
#endif
  if (isa<CallExpr>(expr)) {
    const CallExpr *ce = cast<CallExpr>(expr);

    // FIXME also track any symbols whose declarations need to be brought into scope?  
    for (const Expr* arg : ce->arguments()) {
      analyse_expr (state, arg, occurrences);
    }
  } else if (isa<BinaryOperator>(expr)) {
    analyse_expr (state, cast<BinaryOperator>(expr)->getLHS(), occurrences);
    analyse_expr (state, cast<BinaryOperator>(expr)->getRHS(), occurrences);
  } else if (isa<ImplicitCastExpr>(expr)) {
    const ImplicitCastExpr *ice = cast<ImplicitCastExpr>(expr);
    analyse_expr (state, ice->getSubExpr(), occurrences);
  } else if (isa<UnaryOperator>(expr)) {
    const UnaryOperator *unexpr = cast<UnaryOperator>(expr);
    analyse_expr (state, unexpr->getSubExpr(), occurrences);
  } else if (isa<IntegerLiteral>(expr) || isa<clang::StringLiteral>(expr)) {
    // Do nothing
  } else if (isa<DeclRefExpr>(expr)) {
    const DeclRefExpr *dre = cast<DeclRefExpr>(expr);
    if (isa<VarDecl>(dre->getDecl())) {
      const VarDecl *vdecl = cast<VarDecl>(dre->getDecl());
      occurrences->insert(vdecl);
    }
  }
}

struct Segment {
  std::string compart_name;
  uint8_t segment_id;
  const FunctionDecl * containing_function_definition;
  const Stmt * start_stmt;
  const Stmt * end_stmt;
  std::list<const Stmt *> top_level_trace;
  std::unordered_set<const VarDecl*> free_vars;
  std::unordered_set<const VarDecl*> bound_vars;
  std::string to_string() {
    std::string result = compart_name;
    result += "(" + std::to_string(segment_id) + ")";
    result += "\n";
    result += "Length: " + std::to_string(top_level_trace.size());
    result += "\n";
    result += "Free: " + decls_to_string (free_vars);
    result += "\n";
    result += "Bound: " + decls_to_string (bound_vars);
    result += "\n";
    return result;
  }
};

void analyse_stmt (bool in_a_loop, PitchforkState *state, const Stmt *stmt, std::unordered_set<const VarDecl*> *occurrences, std::unordered_set<const VarDecl*> *declarations, std::unordered_set<std::string> *labels, std::unordered_set<std::string> *gotos) {
#if 1
  errs() << "sss:" << stmt->getStmtClassName() << "\n";
  if (stmt != nullptr) {
    errs() << pitchfork::PFHelper::summarise_sourcecode(state->lang_opt, state->SM, 80, stmt) << "\n";
  }
#endif
  if (isa<CompoundStmt>(stmt)) {
errs() << ":compound:" << "\n";     
    const CompoundStmt *block = cast<CompoundStmt>(stmt);
    for (Stmt *s : block->body()) {
      analyse_stmt(in_a_loop, state, s, occurrences, declarations, labels, gotos);
    }
  } else if (isa<DeclStmt>(stmt)) {
errs() << ":decl:" << "\n";     
    const DeclStmt *decls = cast<DeclStmt>(stmt);
    for (const Decl* decl : decls->decls()) {
      if (isa<VarDecl>(decl)) {
        const VarDecl *vdecl = cast<VarDecl>(decl);

        // FIXME inefficient
        bool found = false;
        for (const VarDecl* past_decl : *declarations) {
          if (past_decl->getNameAsString() == vdecl->getNameAsString()) {
            found = true;
            break;
          }
        }

        if (!found) {
          declarations->insert(vdecl);
        }

        if (vdecl->hasInit()) {
          analyse_expr(state, vdecl->getInit(), occurrences);
        }
      }
    }
  } else if (isa<IfStmt>(stmt)) {
errs() << ":if:" << "\n";     
    const IfStmt *ifstmt = cast<IfStmt>(stmt);
    const Expr *cond = ifstmt->getCond();
    analyse_expr (state, cond, occurrences);
    if (ifstmt->getThen() != nullptr) {
      analyse_stmt(in_a_loop, state, ifstmt->getThen(), occurrences, declarations, labels, gotos);
    }
    if (ifstmt->getElse() != nullptr) {
      analyse_stmt(in_a_loop, state, ifstmt->getElse(), occurrences, declarations, labels, gotos);
    }
  } else if (isa<ForStmt>(stmt)) {
errs() << ":for:" << "\n";     
    const ForStmt *forstmt = cast<ForStmt>(stmt);
    const Expr *cond = forstmt->getCond();
    analyse_expr (state, cond, occurrences);
    analyse_stmt(true, state, forstmt->getBody(), occurrences, declarations, labels, gotos);
  } else if (isa<WhileStmt>(stmt)) {
errs() << ":while:" << "\n";     
    const WhileStmt *whilestmt = cast<WhileStmt>(stmt);
    const Expr *cond = whilestmt->getCond();
    analyse_expr(state, cond, occurrences);
    analyse_stmt(true, state, whilestmt->getBody(), occurrences, declarations, labels, gotos);
  } else if (isa<DoStmt>(stmt)) {
errs() << ":do:" << "\n";     
    const DoStmt *dostmt = cast<DoStmt>(stmt);
    const Expr *cond = dostmt->getCond();
    analyse_expr(state, cond, occurrences);
    analyse_stmt(true, state, dostmt->getBody(), occurrences, declarations, labels, gotos);
  } else if (isa<Expr>(stmt)) {
errs() << ":expr:" << "\n";     
    analyse_expr(state, cast<Expr>(stmt), occurrences);
  } else if (isa<ReturnStmt>(stmt)) {
errs() << ":return:" << "\n";     
    // FIXME print location of this line
    errs() << "'return' statement not allowed" << "\n";
    exit(1);
  } else if (isa<GotoStmt>(stmt)) {
errs() << ":goto:" << "\n";     
    const GotoStmt *gotostmt = cast<GotoStmt>(stmt);
    gotos->insert(gotostmt->getLabel()->getName());
  } else if (isa<LabelStmt>(stmt)) {
errs() << ":label:" << "\n";     
    const LabelStmt *labelstmt = cast<LabelStmt>(stmt);
    labels->insert(labelstmt->getName());
    analyse_stmt(in_a_loop, state, labelstmt->getSubStmt(), occurrences, declarations, labels, gotos);
  } else if (isa<BreakStmt>(stmt) || isa<ContinueStmt>(stmt)) {
    if (!in_a_loop) {
      // FIXME print location of this line
      errs() << "'break'/'continue' statements not allowed" << "\n";
      exit(1);
    }
  } else {
    errs() << "???" << stmt->getStmtClassName() << "\n";
  }
}

void analyse_segment (struct PitchforkState *state, Segment *segment) {
  std::unordered_set<const VarDecl*> occurrences;
  std::unordered_set<const VarDecl*> declarations;
  std::unordered_set<std::string> labels;
  std::unordered_set<std::string> gotos;

#if PF_VERBOSE
  outs() << "Segment:\n";
#endif // PF_VERBOSE
  for (const Stmt *stmt : segment->top_level_trace) {
#if PF_VERBOSE
    outs() << Lexer::getSourceText(
      CharSourceRange::getTokenRange(stmt->getSourceRange()), state.SM, LangOptions()) << "\n";
    errs() << ":::" << stmt->getStmtClassName() << "\n";
#endif // PF_VERBOSE
    analyse_stmt(false/*assume we're not in a loop to begin with, to disallow continue/break statements*/, state, stmt, &occurrences, &declarations, &labels, &gotos);
  }

#if PF_VERBOSE
  outs() << "Occurrences: ";
  for (const VarDecl* vdecl : occurrences) {
    outs() << decl_to_string(vdecl) << " ";
  }
  outs() << "\n";

  outs() << "Declarations: ";
  for (const VarDecl* vdecl : declarations) {
    outs() << decl_to_string(vdecl) << " ";
  }
  outs() << "\n";
#endif // PF_VERBOSE

  std::unordered_set<const VarDecl*> frees = free_variables (occurrences, declarations);
  segment->free_vars = frees;
  segment->bound_vars = declarations;
}

const CallExpr * stmt_is_call (const Stmt *stmt) {
  if (isa<CallExpr>(stmt)) {
    const CallExpr *ce = cast<CallExpr>(stmt);
    return ce;
  }
  return nullptr;
}

const CallExpr * stmt_is_pitchfork_start (const Stmt *stmt) {
  const CallExpr *ce = stmt_is_call(stmt);

  if (ce == nullptr) {
    return nullptr;
  }

  const FunctionDecl *FD = ce->getDirectCallee();
  if (FD->getNameAsString() == "pitchfork_start"/*FIXME const*/) {
    return ce;
  }
  return nullptr;
}

// FIXME DRY principle
const CallExpr * stmt_is_pitchfork_end (const Stmt *stmt) {
  const CallExpr *ce = stmt_is_call(stmt);

  if (ce == nullptr) {
    return nullptr;
  }

  const FunctionDecl *FD = ce->getDirectCallee();
  if (FD->getNameAsString() == "pitchfork_end"/*FIXME const*/) {
    return ce;
  }
  return nullptr;
}

/*
for each coupled start & end
  check that they're in the same scope
*/
bool check_for_irregular_annotation (clang::LangOptions *lang_opt, clang::SourceManager *_SM, const Stmt *sought_stmt, const Stmt *stmt) {
  const CallExpr *sought_ce = stmt_is_pitchfork_end(sought_stmt);
  if (sought_ce == nullptr) {
    std::string sought = Lexer::getSourceText(
       CharSourceRange::getTokenRange(sought_stmt->getSourceRange()), *_SM, *lang_opt);
    errs() << "invalid sought? " << sought << "\n";
  }
  assert(sought_ce != nullptr);

  std::string sought_compart_name = Lexer::getSourceText(
     CharSourceRange::getTokenRange(sought_ce->getArg(0)->getSourceRange()), *_SM, *lang_opt);

  const CallExpr *ce = stmt_is_pitchfork_start(stmt);
  if (ce != nullptr) {
    std::string compart_name = Lexer::getSourceText(
       CharSourceRange::getTokenRange(ce->getArg(0)->getSourceRange()), *_SM, *lang_opt);

    errs() << "start(" << compart_name << ") following start(" << sought_compart_name << "\n";

    exit(1); // FIXME vary return codes
  }

  ce = stmt_is_pitchfork_end(stmt);
  if (ce != nullptr) {
    std::string compart_name = Lexer::getSourceText(
       CharSourceRange::getTokenRange(ce->getArg(0)->getSourceRange()), *_SM, *lang_opt);

    if (sought_compart_name == compart_name) {
      if (sought_stmt == stmt) {
        return false;
      } else {
        return true;
      }
    } else {
      errs() << "end(" << compart_name << ") following start(" << sought_compart_name << "\n";
      exit(1); // FIXME vary return codes
    }
  }

  return false;
}

/*
check if there are any unpaired "starts"
check if there are any unpaired "ends"
*/
void check_annotations (PitchforkState *state, std::vector<Segment> &segments) {
  for (const Stmt *start_stmt : state->StartStmts) {
    bool found = false;
    for (Segment &segment : segments) {
      if (segment.start_stmt == start_stmt) {
        found = true;
        break;
      }
    }
    if (!found) {
      std::string stmt_s = Lexer::getSourceText(
         CharSourceRange::getTokenRange(start_stmt->getSourceRange()), *state->SM, state->lang_opt);
      errs() << "Could not include in segment: " << stmt_s << "\n"; // FIXME add position info
      exit(1); // FIXME vary return codes
    }
  }

  // FIXME DRY principle
  for (const Stmt *end_stmt : state->EndStmts) {
    bool found = false;
    for (Segment &segment : segments) {
      if (segment.end_stmt == end_stmt) {
        found = true;
        break;
      }
    }
    if (!found) {
      std::string stmt_s = Lexer::getSourceText(
         CharSourceRange::getTokenRange(end_stmt->getSourceRange()), *state->SM, state->lang_opt);
      errs() << "Could not include in segment: " << stmt_s << "\n"; // FIXME add position info
      exit(1); // FIXME vary return codes
    }
  }
}

std::vector<Segment> gather_segments(PitchforkState *state) {
  uint8_t no_segments = 0;
  std::vector<Segment> result;
  for (const FunctionDecl *function : state->Functions) {
#if PF_VERBOSE
    outs() << "function = " << function->getName() << "\n";
#endif
    for (const Stmt *start_stmt : state->StartStmts) {
      pitchfork::AnalysisCtxt ac(function->getBody());
      ac.trace = false;
      ac.resume_include_found = false;

      int reaches = reachable_stmt(&state->lang_opt, state->SM, start_stmt, ac);
#if PF_VERBOSE
      outs() << "           = " << pitchfork::PFHelper::summarise_sourcecode(state->lang_opt, state->SM, 80, start_stmt) << "\n";
      outs() << "  reaches1 = " << std::to_string(reaches) << "\n";
#endif

      if (0 == reaches) {
        // The start_stmt is not in this function.
        continue;
      }

      // from start_stmt,try to reach the first end statement
      pitchfork::AnalysisCtxt *new_ac = new pitchfork::AnalysisCtxt(ac);

      // Pick a point at which the start has been statement, to resume from there. The same end point should be reached from all paths leading from the start.
      // To ensure that the right point is "resumed", prune away other points.

      std::list<pitchfork::AnalysisCtxt::Stack*> *stacks = new std::list<pitchfork::AnalysisCtxt::Stack*>();
      for (pitchfork::AnalysisCtxt::Stack* stack : *new_ac->stacks()) {
        pitchfork::AnalysisCtxt::Ctxt* ctxt = (*stack->frames).front();
        if (ctxt->found) {
          ctxt->do_trace = true;
          stacks->push_back(stack);
          break;
        }
      }
      assert(stacks->size() == 1);
      delete new_ac;
      new_ac = new pitchfork::AnalysisCtxt(stacks);

      new_ac->reset();
      new_ac->trace = false;
      new_ac->resume_include_found = false;

      const CallExpr *start_ce = stmt_is_pitchfork_start(start_stmt);
      assert(start_ce != nullptr);
      std::string start_compart_name = Lexer::getSourceText(
        CharSourceRange::getTokenRange(start_ce->getArg(0)->getSourceRange()), *state->SM, state->lang_opt);

      for (const Stmt *end_stmt : state->EndStmts) {
        const CallExpr *end_ce = stmt_is_pitchfork_end(end_stmt);
        assert(end_ce != nullptr);
        std::string end_compart_name = Lexer::getSourceText(
         CharSourceRange::getTokenRange(end_ce->getArg(0)->getSourceRange()), *state->SM, state->lang_opt);

        if (start_compart_name != end_compart_name) {
          continue;
        }

        pitchfork::AnalysisCtxt *possible_segment = new pitchfork::AnalysisCtxt(*new_ac);

        std::list<const Stmt *> segment_stmts;
        reaches = reachable_stmt(&state->lang_opt, state->SM, end_stmt, *possible_segment, &segment_stmts, &check_for_irregular_annotation, false);
#if PF_VERBOSE
        outs() << "             = " << pitchfork::PFHelper::summarise_sourcecode(state->lang_opt, state->SM, 80, end_stmt) << "\n";
        outs() << "    reaches2 = " << std::to_string(reaches) << "\n";
#endif
        if (reaches == 0) {
          continue;
        }

        const CallExpr *ce = stmt_is_pitchfork_start(start_stmt);
        assert(ce != nullptr);
        std::string compart_name = Lexer::getSourceText(
           CharSourceRange::getTokenRange(ce->getArg(0)->getSourceRange()), *state->SM, state->lang_opt);

# pragma clang diagnostic push
# pragma clang diagnostic ignored "-Wc99-extensions"
        struct Segment preresult =
          {.compart_name = compart_name.substr(1, compart_name.length() - 2)/*remove quotes*/,
           .segment_id = no_segments++,
           .containing_function_definition = function,
           .start_stmt = start_stmt,
           .end_stmt = end_stmt,
           .top_level_trace = segment_stmts,
           .free_vars = std::unordered_set<const VarDecl*>(),
           .bound_vars = std::unordered_set<const VarDecl*>()};
# pragma clang diagnostic pop
        result.push_back(preresult);
      }
    }
  }

#if PF_VERBOSE
  outs() << "No. segments found: " << std::to_string(result.size()) << "\n";
#endif // PF_VERBOSE
  return result;
}

const FunctionDecl* get_main_function(PitchforkState *state) {
  for (const FunctionDecl * fd : state->Functions) {
    if (fd->getNameAsString() == "main") {
      return fd;
    }
  }
  return nullptr;
}

struct FileRec {
  bool updated_includes = false;
  boost::filesystem::path original_file_path;
  boost::filesystem::path target_file_path;
  clang::FileID file_id;
  std::string interface_file_name;
  boost::filesystem::path interface_file_path;
  std::string interface_file_contents;
};

void init_file_rec(PitchforkState *state, std::map<std::string,struct FileRec> &file_recs, std::string filename, SourceLocation loc)
{
  boost::filesystem::path file_path(filename);
  file_recs[filename] = {};
  file_recs[filename].updated_includes = false;
  file_recs[filename].original_file_path = file_path;

  std::string output_file_name_prefix = ""/*FIXME const*/; // FIXME was "pitchfork_"/*FIXME const*/;

  std::string output_file_name = output_file_name_prefix + file_path.stem().string() + file_path.extension().string();
  file_recs[filename].target_file_path = file_path.parent_path() / boost::filesystem::path(output_file_name);
  file_recs[filename].file_id = state->SM->getFileID(loc);

  file_recs[filename].interface_file_name = "pitchfork_ix_"/*FIXME const*/ + file_recs[filename].original_file_path.stem().string() + file_recs[filename].original_file_path.extension().string();
  file_recs[filename].interface_file_path = boost::filesystem::path(OutputDirectory) / boost::filesystem::path(file_recs[filename].interface_file_name);
}

void transform_main(PitchforkState *state, const FunctionDecl * fd, Rewriter *rewriter) {
  // Update the "main" function to initialise & start compartments
  // #include a fresh .c file instead of adding lots of code

  assert(isa<CompoundStmt>(fd->getBody()));

  if (!state->updated_main) {
    state->updated_main = true;
    const CompoundStmt *block = cast<CompoundStmt>(fd->getBody());
    Stmt *first_line = nullptr;
    for (Stmt *stmt : block->body()) {
      if (first_line == nullptr) {
        first_line = stmt;
        break;
      }
    }
    StringRef prefix = Lexer::getIndentationForLine(first_line->getLocStart(), *state->SM);

    state->pitchfork_init_contents =
       "compart_init(NO_COMPARTS, comparts, default_config);\n";
    state->pitchfork_init_contents +=
       "#include \"" + state->pitchfork_init_core_file_path.string() + "\"\n";
    state->pitchfork_init_contents +=
       "compart_start(\"" + MainCompartment + "\");";

    rewriter->InsertTextBefore(fd->getDefinition()->getLocStart(), StringRef("#include \"" + state->global_header_file_path.string() + "\"\n"));

    rewriter->InsertTextBefore(first_line->getLocStart(), StringRef("#include \"" + state->pitchfork_init_file_path.string()  + "\"\n" + prefix.str()));
  }
}

void update_includes(Rewriter *rewriter, PitchforkState *state, struct FileRec *file_rec) {
  if (!file_rec->updated_includes) {
    for (auto &PFIR : state->PF_Include_Range) {
      if (!Quiet) {
        outs().changeColor(raw_ostream::YELLOW); // FIXME apply this to more instances of outs()
        outs() << "rewriting #include of file: " << rewriter->getSourceMgr().getFilename(PFIR.getBegin()) << "\n";
        outs().resetColor();
      }
      rewriter->RemoveText(PFIR);

      auto eof = state->SM->getLocForEndOfFile(state->SM->getFileID(PFIR.getBegin()));
      rewriter->InsertTextAfter(eof, StringRef("\n#include \"" + file_rec->interface_file_path.string() + "\"\n"));
      // NOTE we put the segment function declaration immediately before the function containing the call to that function -- this is done in update_segment()
    }
  }
  file_rec->updated_includes = true;
}

void update_segment(Rewriter *rewriter, PitchforkState *state, Segment segment, std::string interface_function_name) {
  SourceRange src_range;
  src_range = segment.end_stmt->getSourceRange();
  src_range.setBegin(segment.start_stmt->getLocStart());

  bool found = false;
  int idx = 1;
  while (!found) {
    clang::SourceLocation potential_new_end (segment.end_stmt->getLocEnd().getLocWithOffset(idx));
    clang::SourceRange range (potential_new_end);
    CharSourceRange CSR = Lexer::makeFileCharRange(
        CharSourceRange::getTokenRange(range), *state->SM, state->lang_opt);
    std::string cursor =
        Lexer::getSourceText(CSR, *state->SM, state->lang_opt).str();
    if (cursor == ";") {
      src_range.setEnd(potential_new_end);
      found = true;
    } else {
      idx++;
    }
  }

  rewriter->RemoveText(src_range);
  rewriter->InsertTextBefore(segment.start_stmt->getLocStart(), StringRef(interface_function_name + "(" + decls_to_string(segment.free_vars, Modifier::Address, false) + ");"));
  rewriter->InsertTextBefore(segment.containing_function_definition->getLocStart(), StringRef("void " + interface_function_name + "(" + decls_to_string(segment.free_vars, Modifier::Deref, true) + ");\n"));
}

std::string header_contents(void) {
  std::string source_code = "#include \"compart_api.h\"\n";/*FIXME hardcoded*/
  source_code += "\n";

  source_code += "#define NO_COMPARTS " + std::to_string(declared_compartments.size()) + "\n";
  source_code += "static struct compart comparts[NO_COMPARTS] = {\n";
  for (std::map<std::string,Compart>::iterator it = declared_compartments.begin(); it != declared_compartments.end(); ++it) {
    const std::pair<std::string,Compart> entry = *it;
    source_code += "  {.name = \"" + entry.second.name +
      "\", .uid = " + entry.second.uid + ", " +
      ".gid = " + entry.second.gid + ", " +
      ".path = \"" + entry.second.chroot + "\"}";
    if (it != declared_compartments.end()) {
      source_code += ",\n";
    } else {
      source_code += "\n";
    }
  }
  source_code += "};\n";
  source_code += "\n";

  return source_code;
}

const std::string C_header_comment = "// Autogenerated file from running Pitchfork\n\n";
const std::string sh_header_comment = "#!/usr/bin/env sh\n# Autogenerated file from running Pitchfork\nset -e\n\n";

struct OutputFile {
  boost::filesystem::path target_file_path;
  std::string target_file_contents;
  bool append;
  std::string header_comment;
};

void add_output_file(std::map<std::string,struct OutputFile> &output_files, boost::filesystem::path target_file_path, std::string target_file_contents, bool append = false, std::string header_comment = C_header_comment)
{
  if (boost::filesystem::exists(target_file_path.string()) && !append) {
    errs().changeColor(raw_ostream::RED); // FIXME apply this to more instances of errs()
    errs() << "Output file already exists: " << target_file_path.string() << "\n";
    errs().resetColor();
    if (!Overwrite) {
      exit(1); // FIXME vary return codes
    }
  }

  std::map<std::string,struct OutputFile>::iterator it;
  it = output_files.find(target_file_path.string());
  if (it != output_files.end() && !append) {
    errs() << "Double output to same file: " << target_file_path.string() << "\n";
    exit(1); // FIXME vary return codes
  } else {
    if (it != output_files.end()) {
      output_files[target_file_path.string()].target_file_contents =
       output_files[target_file_path.string()].target_file_contents + "\n" +
       target_file_contents;
    } else {
      output_files[target_file_path.string()] = {target_file_path, target_file_contents, append, header_comment};
    }
  }
}

void make_output(std::map<std::string,struct OutputFile> &output_files)
{
  for (const std::pair<std::string,struct OutputFile> &output_file : output_files) {
    auto mode = std::ios_base::trunc;
    if (output_file.second.append) {
      mode = std::ios_base::app;
    }

    global_output_files.insert(output_file.first);

    if (OnlyListOutputs) {
      if (!Quiet) {
        if (output_file.second.append) {
          outs() << "Would append: ";
        } else {
          outs() << "Would write: ";
        }
        outs() << output_file.first;
        outs() << "\n";
      }
      continue;
    }

    bool new_file = !boost::filesystem::exists(output_file.second.target_file_path.string());

    std::ofstream output;
    output.open(output_file.second.target_file_path.string(), mode);
    if (new_file) {
      output << output_file.second.header_comment;
    }
    output << output_file.second.target_file_contents;
    output.close();

    if (!Quiet) {
      outs().changeColor(raw_ostream::BLUE); // FIXME apply this to more instances of outs()
      if (output_file.second.append) {
        outs() << "Appended: ";
      } else {
        outs() << "Wrote: ";
      }
      outs().resetColor();
      outs() << output_file.first;
      if (new_file) {
        outs().changeColor(raw_ostream::YELLOW); // FIXME apply this to more instances of outs()
        outs() << " (new)";
        outs().resetColor();
      }
      outs() << "\n";
    }
  }
}

void generate_interface_file(PitchforkState *state, std::string ifx_ext, std::string registered_function_name, Segment segment, boost::filesystem::path file_path, std::string interface_function_name, struct FileRec *file_rec, std::map<std::string,struct OutputFile> &output_files) {

  if (file_rec->interface_file_contents.empty()) {
    std::string source_code = "#include <stdlib.h>\n"; // since we use "NULL" in the synthesised code
    source_code += "#include \"compart_api.h\"\n";/*FIXME hardcoded*/
    file_rec->interface_file_contents = source_code;
    assert(!file_rec->interface_file_contents.empty());
  }

  std::string source_code = "\n// Interface for compartment \"" + segment.compart_name + "\"\n\n";

//struct extension_data ext_add_ten_to_arg(int num)
//int ext_add_ten_from_arg(struct extension_data data)

  std::string marshall_fn_name = interface_function_name + "_marshall";
  std::string demarshall_fn_name = interface_function_name + "_demarshall";

  std::string filename = OutputDirectory + "/" + segment.compart_name + std::to_string(segment.segment_id);
  std::string demarsh_output_file_name = filename + "_output.c";
  std::string demarsh_input_file_name = filename + "_input.c";
  std::string demarsh_generated_script_name = filename + "_script.py";
  std::string demarsh_input_file_contents = "void " + interface_function_name + "(" + decls_to_string(segment.free_vars, Modifier::None, true) + ") {}";

  std::string type_decls;
  for (std::string &s : state->type_decls) {
    type_decls += s + "\n";

  }
  demarsh_input_file_contents = type_decls + demarsh_input_file_contents;

  boost::filesystem::path target_file_path(demarsh_input_file_name);
  add_output_file(output_files, target_file_path, demarsh_input_file_contents);

  std::string cmd = "SYS_PATH=\"" + DemarshallingSysPath + "\" " +
    "OUTPUT_FILE=\"" + demarsh_output_file_name + "\" " +
    "SIGNATURE=\"" + demarsh_input_file_name + "\" " +
    "OUTPUT_SCRIPT=\"" + demarsh_generated_script_name + "\" " +
    "TO_ARG_NAME=\"" + marshall_fn_name + "\" " +
    "FROM_ARG_NAME=\"" + demarshall_fn_name + "\" " +
    PathToDemarshallingScript + "\n\n";
  cmd += "python " + demarsh_generated_script_name + "\n\n ";
  add_output_file(output_files, OutputDirectory + "/" + InterfaceGenerationOutputScript, cmd, true, sh_header_comment);

  std::string arg_var_name = "__arg"; // FIXME const
  std::string result_var_name = "__result"; // FIXME const
  std::string marshal_decl = "struct extension_data " + marshall_fn_name + "(" +
    decls_to_string(segment.free_vars, Modifier::Deref, true) + ")";
  std::string demarshal_decl = "void " + demarshall_fn_name + "(" +
    "struct extension_data " + arg_var_name;
  if ("" != decls_to_string(segment.free_vars, Modifier::Deref, true)) {
    demarshal_decl += ", ";
  }
  demarshal_decl += decls_to_string(segment.free_vars, Modifier::Deref, true) + ")";

  if (segment.free_vars.size() > 0) {
#if 0  
    state->global_header_contents += marshal_decl + ";\n";
    state->global_header_contents += demarshal_decl + ";\n";
#endif

#if 0
    source_code += marshal_decl + " {\n}\n";
    source_code += demarshal_decl + " {\n}\n";
#else
    // The de/marshalling code is generated by a different system, whose output we include here.
    source_code += "#include \"" + demarsh_output_file_name + "\"\n";
#endif

    source_code += "\n";
  }

  std::string reg_func_decl = "struct extension_data " + registered_function_name + "(struct extension_data " + arg_var_name + ")";

  state->global_header_core_contents += reg_func_decl + ";\n";

  source_code += reg_func_decl + " {\n";
  std::string indent = "  "; // FIXME const
  if (segment.free_vars.size() > 0) {
    for (auto &decl : segment.free_vars) {
      source_code += indent + decl_to_string(decl, Modifier::None, true) + ";\n";
    }

    source_code += indent + demarshall_fn_name + "(" + arg_var_name;
    if ("" != decls_to_string(segment.free_vars, Modifier::Address, false)) {
      source_code += ", ";
    }
    source_code += decls_to_string(segment.free_vars, Modifier::Address, false) + ");\n";
  }

  for (const Stmt *stmt : segment.top_level_trace) {
    StringRef prefix = Lexer::getIndentationForLine(stmt->getLocStart(), *state->SM);
    source_code += prefix.str();
    source_code += Lexer::getSourceText(
      CharSourceRange::getTokenRange(stmt->getSourceRange()), *state->SM, state->lang_opt);
    source_code += ";\n";
  }

  if (segment.free_vars.size() > 0) {
    source_code += indent + "return " + marshall_fn_name + "(" +
      decls_to_string(segment.free_vars, Modifier::None, false) + ");\n";
  } else {
    source_code += indent + "return " + arg_var_name + ";\n";
  }

  source_code += "}\n";
  source_code += "\n";

  std::string if_ext_decl = "struct extension_id *" + ifx_ext;
  state->global_header_core_contents += "extern " + if_ext_decl + ";\n";
  source_code += if_ext_decl + " = NULL;\n";
  source_code += "\n";

  std::string interface_function_decl = "void " + interface_function_name + "(" +
    decls_to_string(segment.free_vars, Modifier::Deref, true) + ")";
#if 0  
  state->global_header_contents += interface_function_decl + ";\n";
#endif

  source_code += interface_function_decl + " {\n";
  if (segment.free_vars.size() > 0) {
    source_code += indent + "struct extension_data " + arg_var_name +
      " = " + marshall_fn_name + "(" +
      decls_to_string(segment.free_vars, Modifier::Deref, false) + ");\n";

    source_code += indent + "struct extension_data " + result_var_name +
      " = compart_call_fn(" + ifx_ext + ", " + arg_var_name + ");\n";

    source_code += indent + demarshall_fn_name + "(" + result_var_name;
    if ("" != decls_to_string(segment.free_vars, Modifier::None, false)) {
      source_code += ", ";
    }
    source_code += decls_to_string(segment.free_vars, Modifier::None, false) + ");\n";
  } else {
    source_code += indent + "struct extension_data " + arg_var_name + " = {0};\n"; // FIME wasteful
    source_code += indent + "(void)compart_call_fn(" + ifx_ext + ", " + arg_var_name + ");\n";
  }
  source_code += "}\n";

  state->global_header_contents += "\n";

  file_rec->interface_file_contents += source_code;
}

/*
transfer segment to the interface file -- we #include individual _interface
  that includes all segment instances for that file.
work out context used by segment, generate function templates transfer that too
  add notation for hints, to broaden the context in case it's underapproximated
generate template for de/marshalling in the transformed + interface code

#including global .h declaring functions+types related to all segments+instances

initialisation is put in main()
*/
void transform (PitchforkState *state, std::vector<Segment> segments) {
  Rewriter rewriter(*state->SM, state->lang_opt);

  std::map<std::string,struct FileRec> file_recs;
  std::map<std::string,struct OutputFile> output_files;

  // FIXME move this block to a function?
  state->global_header_file_path = boost::filesystem::path(OutputDirectory) / boost::filesystem::path("pitchfork_ifx.h"/*FIXME const*/);
  state->pitchfork_init_file_path = boost::filesystem::path(OutputDirectory) / boost::filesystem::path("pitchfork_init.c"/*FIXME const*/);

  state->global_header_core_file_path = boost::filesystem::path(OutputDirectory) / boost::filesystem::path("pitchfork_ifx_core.h"/*FIXME const*/);
  state->pitchfork_init_core_file_path = boost::filesystem::path(OutputDirectory) / boost::filesystem::path("pitchfork_init_core.c"/*FIXME const*/);
  add_output_file(output_files, OutputDirectory + "/" + InterfaceGenerationOutputScript, "", true, sh_header_comment); // Initialise the generation of the output script.

  for (Segment &segment : segments) {
    std::map<std::string,FileRec>::iterator it;
    std::string filename = state->SM->getFilename(segment.start_stmt->getLocStart());
    it = file_recs.find(filename);
    if (it == file_recs.end()) {
      init_file_rec(state, file_recs, filename, segment.start_stmt->getLocStart());
    }

    std::string interface_function_name = "__pf_" + segment.compart_name + "_" + std::to_string(segment.segment_id) + "_call";
    std::string ifx_ext = interface_function_name + "_ext";
    std::string registered_function_name = interface_function_name + "_remote";

    update_includes(&rewriter, state, &file_recs[filename]);
    update_segment(&rewriter, state, segment, interface_function_name);

    state->pitchfork_init_core_contents +=
      ifx_ext + " = compart_register_fn(\"" + segment.compart_name + "\", &" +
      registered_function_name + ");\n";

    generate_interface_file(state, ifx_ext, registered_function_name, segment, file_recs[filename].original_file_path, interface_function_name, &file_recs[filename], output_files);
  }

  if (const FunctionDecl* fd = get_main_function(state)) {
    // Even if file (containing main()) contains no segments, we'll need to
    // transform it.
    if (segments.size() == 0) {
      clang::SourceLocation loc = fd->getDefinition()->getLocStart();
      std::string filename = state->SM->getFilename(loc);
      init_file_rec(state, file_recs, filename, loc);
    }

    transform_main(state, fd, &rewriter);

    state->global_header_contents = "#ifndef __PITCHFORK_HEADER\n#define __PITCHFORK_HEADER\n\n" + state->global_header_contents; // FIXME const
    state->global_header_contents += header_contents();
    state->global_header_contents +=
       "#include \"" + state->global_header_core_file_path.string() + "\"\n";
    state->global_header_contents += "#endif // __PITCHFORK_HEADER\n"; // FIXME const
    add_output_file(output_files, state->global_header_file_path, state->global_header_contents);

    add_output_file(output_files, state->pitchfork_init_file_path, state->pitchfork_init_contents);
  }

  add_output_file(output_files, state->global_header_core_file_path, state->global_header_core_contents, true);
  add_output_file(output_files, state->pitchfork_init_core_file_path, state->pitchfork_init_core_contents, true);

  for (const std::pair<std::string,FileRec> &record : file_recs) {
    if (!Quiet) {
      outs().changeColor(raw_ostream::GREEN); // FIXME apply this to more instances of outs()
      outs() << "Output file: " << record.second.target_file_path.string() << "\n";
      outs() << "       from: " << state->SM->getFileEntryForID(record.second.file_id)->getName() << "\n";
      outs().resetColor();
    }

    std::string file_contents;
    llvm::raw_string_ostream output_file(file_contents);
    rewriter.getEditBuffer(record.second.file_id).write(output_file);
    add_output_file(output_files, record.second.target_file_path, output_file.str());

    // In case of file containing main() we'll update the file but won't
    // necessarily have an interface file.
    if (!record.second.interface_file_contents.empty()) {
      add_output_file(output_files, record.second.interface_file_path, record.second.interface_file_contents);
    }
  }

  make_output(output_files);
}

struct PPActions : clang::PPCallbacks {
const SourceManager *_PSM = nullptr;
PitchforkState *state = nullptr;
  PPActions (PitchforkState *state, SourceManager *_PSM) : _PSM(_PSM), state(state) {
    assert(_PSM != nullptr);
    assert(state != nullptr);
  }
  void InclusionDirective (
    SourceLocation HashLoc,
    const Token &IncludeTok,
    StringRef FileName,
    bool IsAngled,
    CharSourceRange FilenameRange,
    const FileEntry *File,
    StringRef SearchPath,
    StringRef RelativePath,
    const clang::Module *Imported) override {

   if (FileName == "pitchfork.h"/*FIXME const*/) {
#if PF_VERBOSE
     outs() << "Include: " << FileName << "\n";
#endif // PF_VERBOSE
     SourceRange PFIR;
     PFIR.setBegin(HashLoc);
     PFIR.setEnd(FilenameRange.getEnd());

    // FIXME inefficient
    bool found = false;
    for (auto &PFIR_ : state->PF_Include_Range) {
      if (PFIR == PFIR_) {
        found = true;
        break;
      }
    }
    if (!found) {
      state->PF_Include_Range.push_back(PFIR);
    }
   } else if (_PSM->isInMainFile(HashLoc)) { // FIXME shouldnt assume we're always working wrt MainFile  
    // FIXME inefficient
    bool found = false;
    std::string filename;
    if (IsAngled) {
      filename = "<" + FileName.str() + ">";
    } else {
      filename = "\"" + FileName.str() + "\"";
    }
    for (auto &filename_ : state->FileIncludes) {
      if (filename == filename_) {
        found = true;
        break;
      }
    }
    if (!found) {
      if (!Quiet) {
        outs().changeColor(raw_ostream::YELLOW); // FIXME apply this to more instances of outs()
        outs() << "#include in " << _PSM->getFilename(HashLoc) << ": " << filename << "\n";
        outs().resetColor();
      }
      state->FileIncludes.push_back(filename);
    }
   }
 }
};

unsigned number_comparts = 0;
std::mutex number_comparts_mtx;

void process(PitchforkState *state) {
#if PF_VERBOSE
  outs() << "StartStmts:" << state->StartStmts.size() << "\n";
  outs() << "EndStmts:" << state->EndStmts.size() << "\n";
#endif // PF_VERBOSE

  std::vector<Segment> segments = gather_segments(state);
  number_comparts_mtx.lock();
  number_comparts += segments.size();
  number_comparts_mtx.unlock();
  check_annotations(state, segments);

  for (Segment &segment : segments) {
    analyse_segment(state, &segment);
  }

/* FIXME not every declared component will be instantiated in each file
  for (std::pair<std::string,Compart> decl_comp : declared_compartments) {
    bool found = false;
    for (Segment &segment : segments) {
      if (decl_comp.first == segment.compart_name) {
        found = true;
        break;
      }
    }
    if (!found && decl_comp.first != MainCompartment) {
      errs() << "Declared compartment '" << decl_comp.first << "' hasn't been instantiated\n";
      exit(1); // FIXME vary return codes    
    }
  }
*/

  for (Segment &segment : segments) {
    bool found = false;
    for (std::pair<std::string,Compart> decl_comp : declared_compartments) {
      if (decl_comp.first == segment.compart_name) {
        found = true;
        break;
      }
    }
    if (!found) {
      errs() << "Instantiated compartment '" << segment.compart_name << "' hasn't been declared\n";
      exit(1); // FIXME vary return codes
    }
  }

#if PF_VERBOSE
  for (Segment &segment : segments) {
    outs() << segment.to_string();
  }
#endif // PF_VERBOSE

  transform(state, segments);
}

void gather_declarations(PitchforkState *state, clang::DeclContext::decl_range decl_range) {
  for (const Decl *decl : decl_range)
  {
    if (isa<clang::TypeDecl>(decl)) {
      SourceLocation fid = decl->getLocation();
      StringRef filename = state->SM->getFilename(fid);
      if (filename.size() == 0) {
        continue;
      }

      if (filename.substr(filename.size() - 2, 2) == ".h") {
        std::string candidate = "#include \"" + std::string(filename) + "\"";
        bool found = false;
        for (std::string &line : state->type_decls) {
          if (line == candidate) {
            found = true;
            break;
          }
        }
        if (!found) {
          state->type_decls.push_back("#include \"" + std::string(filename) + "\"");
        }
        continue;
      }

      state->type_decls.push_back(pitchfork::PFHelper::summarise_sourcecode(state->lang_opt, state->SM, 8000/*FIXME const*/, decl));
    }
  }
}

class PitchforkAction : public clang::ASTFrontendAction {
private:
  PitchforkState state;
  clang::ast_matchers::MatchFinder Finder;
  FunctionEnumerator FE{&state, "FunctionEnumerator"};
public:
  PitchforkAction() {
    Finder.addMatcher(FE.makeMatcher(), &FE);

    pitchfork::ResFunctionFinder *PFStart = new pitchfork::ResFunctionFinder("PFStart", &state.StartStmts);
    Finder.addMatcher(PFStart->makeMatcher("pitchfork_start"), PFStart);
    pitchfork::ResFunctionFinder *PFEnd = new pitchfork::ResFunctionFinder("PFEnd", &state.EndStmts);
    Finder.addMatcher(PFEnd->makeMatcher("pitchfork_end"), PFEnd);
  }

  virtual std::unique_ptr<clang::ASTConsumer>
  CreateASTConsumer(clang::CompilerInstance& ci, clang::StringRef) override {
     state.lang_opt = ci.getLangOpts();
     ci.getPreprocessor().addPPCallbacks(llvm::make_unique<PPActions>(&state, &ci.getSourceManager()));
     return Finder.newASTConsumer();
   }

  virtual void EndSourceFileAction() override {
    state.SM = &getCompilerInstance().getSourceManager();

    gather_declarations(&state, getCompilerInstance().getASTContext().getTranslationUnitDecl()->decls());

    process(&state);
  }
};

class PitchforkAF : public clang::tooling::FrontendActionFactory {
  clang::FrontendAction *create() {
    return new PitchforkAction();
  }
};

std::set<std::string> compartment_files;
std::set<std::string> main_files;
std::mutex compartment_files_mtx;
std::mutex main_files_mtx;

class PitchforkAnalysisAction : public clang::ASTFrontendAction {
private:
  PitchforkState state;
  clang::ast_matchers::MatchFinder Finder;
  FunctionEnumerator FE{&state, "FunctionEnumerator"};
public:
  PitchforkAnalysisAction() {
    Finder.addMatcher(FE.makeMatcher(), &FE);

    pitchfork::ResFunctionFinder *PFStart = new pitchfork::ResFunctionFinder("PFStart", &state.StartStmts);
    Finder.addMatcher(PFStart->makeMatcher("pitchfork_start"), PFStart);
    pitchfork::ResFunctionFinder *PFEnd = new pitchfork::ResFunctionFinder("PFEnd", &state.EndStmts);
    Finder.addMatcher(PFEnd->makeMatcher("pitchfork_end"), PFEnd);
  }

  virtual std::unique_ptr<clang::ASTConsumer>
  CreateASTConsumer(clang::CompilerInstance& ci, clang::StringRef) override {
     return Finder.newASTConsumer();
   }

  virtual void EndSourceFileAction() override {
    state.SM = &getCompilerInstance().getSourceManager();
    for (const FunctionDecl * function_decl :  state.Functions) {
      if ("main" == function_decl->getNameAsString()) {
	main_files_mtx.lock();
        main_files.insert(std::string(state.SM->getFilename(function_decl->getSourceRange().getBegin())));
	main_files_mtx.unlock();
      }
    }

    for (std::vector<const Stmt *> stmts : std::vector<std::vector<const Stmt *>>{state.StartStmts, state.EndStmts}) {
      for (const Stmt * stmt : stmts) {
	compartment_files_mtx.lock();
        compartment_files.insert(std::string(state.SM->getFilename(stmt->getLocStart())));
	compartment_files_mtx.unlock();
      }
    }
  }
};

class PitchforkAnalysisAF : public clang::tooling::FrontendActionFactory {
  clang::FrontendAction *create() {
    return new PitchforkAnalysisAction();
  }
};

int main(int argc, const char **argv) {
  llvm::sys::PrintStackTraceOnErrorSignal(argv[0]);

  cl::SetVersionPrinter(PrintVersion);

/*
Locate start of each segment
From there, locate end of segment (all paths from a compart_start must lead there)
At start of segment, gather context (based on what variables appear in the segment)
Generate template for hand-over (both directions)
Generate template interface file
*/

  using namespace clang::tooling;

  if (3 == argc && !strcmp(argv[1], "-p")) {
    std::string ErrorMessage;

    if (!boost::filesystem::exists(argv[2])) {
      errs() << "File does not exist: " << argv[2] << "\n";
      return 1;
    }

    std::unique_ptr<JSONCompilationDatabase> cdb = JSONCompilationDatabase::loadFromFile(argv[2], ErrorMessage, JSONCommandLineSyntax::AutoDetect);
    for (const std::string& file : cdb->getAllFiles()) {
      outs() << file << " ";
    }
    outs() << "\n";
    return 0;
  }

  CommonOptionsParser op(argc, argv, PitchforkCategory);
  ClangTool Tool(op.getCompilations(), op.getSourcePathList());

  if (FilterInputFiles) {
    // Auto-detect the main file and compartment instances
    int result = Tool.run(new PitchforkAnalysisAF());
    if (main_files.size() == 0) {
      errs() << "No file containing 'main()' has been found\n";
      return 1;
    }
#if 0
    if (main_files.size() > 1) {
      std::string result = "Multiple files containing 'main()' have been found: ";
      for (const std::string& file : main_files) {
        result += file + " ";
      }
      errs() << result << "\n";
      return 1;
    }
#endif
    errs() << "main()-containing file count: " << std::to_string(main_files.size()) << "\n";

    errs() << "segment-containing file count: " << std::to_string(compartment_files.size()) << "\n";
    for (const std::string& file : compartment_files) {
      // In case a file is in both sets, process it in main_files
      if (main_files.find(file) == main_files.end()) {
        outs() << file << " ";
      }
    }
    for (const std::string& file : main_files) {
      outs() << file << " ";
    }
    outs() << "\n";

    return result;
  }

  if (!Parse) {
    if (MainCompartment.getNumOccurrences() > 1) {
      errs() << "Must provide only 1 instance of -" + MainCompartment.ArgStr + "\n";
      exit(1); // FIXME vary return codes
    } else if (MainCompartment.getNumOccurrences() == 0) {
      errs() << "Must provide an instance of -" + MainCompartment.ArgStr + "\n";
      exit(1); // FIXME vary return codes
    }

    if (PathToDemarshallingScript.getNumOccurrences() > 1) {
      errs() << "Must provide only 1 instance of -" + PathToDemarshallingScript.ArgStr + "\n";
      exit(1); // FIXME vary return codes
    } else if (PathToDemarshallingScript.getNumOccurrences() == 0) {
      errs() << "Must provide an instance of -" + PathToDemarshallingScript.ArgStr + "\n";
      exit(1); // FIXME vary return codes
    }

    if (DemarshallingSysPath.getNumOccurrences() > 1) {
      errs() << "Must provide only 1 instance of -" + DemarshallingSysPath.ArgStr + "\n";
      exit(1); // FIXME vary return codes
    } else if (DemarshallingSysPath.getNumOccurrences() == 0) {
      errs() << "Must provide an instance of -" + DemarshallingSysPath.ArgStr + "\n";
      exit(1); // FIXME vary return codes
    }

    if (InterfaceGenerationOutputScript.getNumOccurrences() > 1) {
      errs() << "Must provide only 1 instance of -" + InterfaceGenerationOutputScript.ArgStr + "\n";
      exit(1); // FIXME vary return codes
    } else if (InterfaceGenerationOutputScript.getNumOccurrences() == 0) {
      errs() << "Must provide an instance of -" + InterfaceGenerationOutputScript.ArgStr + "\n";
      exit(1); // FIXME vary return codes
    }

    if (OutputDirectory.getNumOccurrences() > 1) {
      errs() << "Must provide only 1 instance of -" + OutputDirectory.ArgStr + "\n";
      exit(1); // FIXME vary return codes
    } else if (OutputDirectory.getNumOccurrences() == 0) {
      errs() << "Must provide an instance of -" + OutputDirectory.ArgStr + "\n";
      exit(1); // FIXME vary return codes
    }

    parse_compartment_declarations();

#if PF_VERBOSE
    outs() << "declared_compartments.size() == " << declared_compartments.size() << "\n";
#endif // PF_VERBOSE
    if (declared_compartments.size() == 0) {
      errs() << "No compartments have been declared: nothing to do\n";
      exit(1); // FIXME vary return codes
    }
  } else {
    clang::ast_matchers::MatchFinder Finder;
    return Tool.run(newFrontendActionFactory(&Finder).get());
  }

  int result = Tool.run(new PitchforkAF());

  if (OnlyListOutputs) {
    for (const std::string &file_name : global_output_files) {
      outs() << file_name << " ";
    }
  }

  if (0 == number_comparts) {
    errs() << "No segments have been gathered" << "\n";
    exit(1); // FIXME crude
  } else {
    outs() << "Segments found: " << std::to_string(number_comparts) << "\n";
  }

  return result;
}
