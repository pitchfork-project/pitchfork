/*
   Copyright 2021 University of Pennsylvania
   Pitchfork project. Nik Sultana, UPenn, 2019

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
#include "PFCtx.h"
#include "PFHelper.h"

namespace pitchfork {

std::string AnalysisCtxt::TurnKindString(TurnKind tk) {
  std::string explain;
  switch (tk) {
    case AnalysisCtxt::TurnKind::None:
      explain = "<--->";
      break;
    case AnalysisCtxt::TurnKind::Fun:
      explain = "<fun>";
      break;
    case AnalysisCtxt::TurnKind::IfThenElse:
      explain = "<ift>";
      break;
    case AnalysisCtxt::TurnKind::While:
      explain = "<whi>";
      break;
    case AnalysisCtxt::TurnKind::For:
      explain = "<for>";
      break;
    case AnalysisCtxt::TurnKind::Do:
      explain = "<do>";
      break;
    case AnalysisCtxt::TurnKind::Block:
      explain = "<blk>";
      break;
  }
  return explain;
}

AnalysisCtxt::AnalysisCtxt(const Stmt *s) {
  RemainingStmts *stmts = new RemainingStmts();
  stmts->push_back(s);
  Ctxt *ctxt = new Ctxt{nullptr, stmts, false, TurnKind::None};
  Stack *stack = new Stack();
  stack->frames = new std::list<Ctxt *>();
  stack->trace = new std::list<const Stmt *>();
  stack->frames->push_back(ctxt);
  me = new PCtxt();
  me->push_back(stack);
}

AnalysisCtxt::Stack *AnalysisCtxt::clone(Stack *s) {
  Stack *s2 = new Stack();
  s2->frames = new std::list<Ctxt *>();
  s2->trace = new std::list<const Stmt *>(s->trace->size());
  std::copy(s->trace->begin(), s->trace->end(), s2->trace->begin());

  for (auto ctxt : *(s->frames)) {
    RemainingStmts *q = ctxt->rs;
    RemainingStmts *q2 = new RemainingStmts();

    for (auto &e : *q) {
      q2->push_back(e);
    }

    Ctxt *ctxt2 = new Ctxt{ctxt->turn, q2, ctxt->found, ctxt->tk};

    s2->frames->push_back(ctxt2);
  }

  me->push_back(s2);
  return s2;
}

AnalysisCtxt::AnalysisCtxt(AnalysisCtxt &orig) {
  me = new PCtxt();

  for (auto &stack : *orig.me) {
    Stack *s2 = new Stack();
    s2->frames = new std::list<Ctxt *>();
    s2->trace = new std::list<const Stmt *>(stack->trace->size());
    std::copy(stack->trace->begin(), stack->trace->end(), s2->trace->begin());

    for (auto &ctxt : *(stack->frames)) {
      RemainingStmts *q = ctxt->rs;
      RemainingStmts *q2 = new RemainingStmts(q->size());
      std::copy(q->begin(), q->end(), q2->begin());

      Ctxt *ctxt2 = new Ctxt{ctxt->turn, q2, ctxt->found, ctxt->tk};

      s2->frames->push_back(ctxt2);
    }

    me->push_back(s2);
  }
}

std::list<const Stmt *> *AnalysisCtxt::trace_statements(Stack *s) {
  return s->trace;
}

std::string AnalysisCtxt::trace_statements_str(SourceManager *SM, Stack *s) {
  std::string result;
  std::list<const Stmt *> *trace = trace_statements(s);
  result = "Trace (" + std::to_string(trace->size()) + "):\n";
  unsigned count = 0;
  clang::LangOptions LO;
  for (auto &stmt : *trace) {
    clang::SourceLocation begin(stmt->getLocStart());
    result +=
        std::to_string(count) + ":\t" + begin.printToString(*SM) + "  \t| " +
        PFHelper::summarise_sourcecode(LO, SM, CP_MAX_CODELINE_LENGTH, stmt) +
        "\n";
    ++count;
  }
  return result;
}

void AnalysisCtxt::join(AnalysisCtxt *ac) {
  for (PCtxt::iterator it = ac->me->begin(); it != ac->me->end(); ++it) {
    me->push_back(*it);
  }
}

unsigned AnalysisCtxt::filter_subsumed(bool strict) {
  unsigned result = 0;
  PCtxt *me2 = new PCtxt();
  for (Stack *stack1 : *me) {
    bool subsumed = false;

    for (Stack *stack2 : *me) {
      if (stack1 != stack2) {
        if (subsumes(stack2, stack1, strict)) {
          subsumed = true;
          break;
        }
      }
    }

    if (!subsumed) {
      me2->push_back(stack1);
    } else {
      result++;
    }
  }

  me = me2;
  return result;
}

std::string AnalysisCtxt::trace_statements_str(SourceManager *SM) {
  std::string result;
  unsigned count = 0;
  for (auto &stack : *me) {
    result += "Stack " + std::to_string(count) + "/" +
              std::to_string(me->size()) + ":\n";
    result += trace_statements_str(SM, stack);
    ++count;
  }
  return result;
}

std::string AnalysisCtxt::stats() {
  std::string result;
  result += std::to_string(me->size()) + ":";
  for (auto &stack : *me) {
    result += " (" + std::to_string(stack->frames->size()) + ":";
    for (auto &ctxt : *stack->frames) {
      result += " " + std::to_string(ctxt->rs->size());
      if (ctxt->found) {
        result += "!";
      }
    }
    result += ")";
  }
  return result;
}

unsigned AnalysisCtxt::num_stacks() { return me->size(); }

unsigned AnalysisCtxt::num_found() {
  unsigned result = 0;
  for (auto &stack : *me) {
    if (!stack->frames->empty()) {
      // Assuming that "found" mark is always at the top of the stack.
      Ctxt *ctxt = stack->frames->front();
      if (ctxt->found) {
        result += 1;
      }
    }
  }
  return result;
}

float AnalysisCtxt::found_ratio() {
  float num_stacks = float(this->num_stacks());
  if (0 == num_stacks) {
    return -1;
  } else {
    float num_found = float(this->num_found());
    return num_found / num_stacks;
  }
}

bool AnalysisCtxt::stable() {
  // We need to ensure that the AnalysisCtxt is in a "stable" state: i.e.,
  // either a reachability hasn't been attempted (since the beginning or reset),
  // or otherwise it finished in a state were excessive stacks were purged, thus
  // the second disjunct.
  return (0 == this->num_found() && this->initial) ||
         (this->num_found() == this->num_stacks());
}

std::list<const Stmt *> *AnalysisCtxt::found_statements() {
  assert(stable());
  std::list<const Stmt *> *result = new std::list<const Stmt *>();
  for (auto &stack : *me) {
    assert(stack->frames != nullptr && stack->frames->size() > 0 &&
           stack->frames->front()->rs != nullptr &&
           stack->frames->front()->rs->size() > 0);
    result->push_back(stack->frames->front()->rs->front());
  }
  return result;
}

std::string AnalysisCtxt::stats(SourceManager *SM) {
  std::string result;
  result += std::to_string(me->size()) + " stacks:\n";
  for (auto &stack : *me) {
    result += "--(stack) " + std::to_string(stack->frames->size()) + ":\n";
    for (auto &ctxt : *stack->frames) {
      if (ctxt->found) {
        result += "!";
      }
      clang::LangOptions LO;
      std::string turn_s = "";
      if (nullptr != ctxt->turn) {
        turn_s = PFHelper::summarise_sourcecode(
            LO, SM, CP_MAX_CODELINE_LENGTH, ctxt->turn);
      }
      result += " \\--" + TurnKindString(ctxt->tk) + " (" + turn_s +
                ") statements (" + std::to_string(ctxt->rs->size()) + "):\n";
      for (auto &s : *ctxt->rs) {
        assert(nullptr != s);
        clang::SourceLocation begin(s->getLocStart());
        std::string source_code =
            PFHelper::summarise_sourcecode(LO, SM, CP_MAX_CODELINE_LENGTH, s);
        result += "     | " + begin.printToString(*SM) + "  \t| " +
                  source_code + "\n";
      }
    }
  }
  return result;
}

bool AnalysisCtxt::empty() { return me->empty(); }

void AnalysisCtxt::reset() {
  this->initial = true;
  for (auto &stack : *me) {
    for (auto &ctxt : *stack->frames) {
      ctxt->found = false;
    }
  }
}

AnalysisCtxt::Ctxt *AnalysisCtxt::NewLayer(const Stmt *point, const Stmt *s,
                                           TurnKind tk, bool b) {
  RemainingStmts *stmts = new RemainingStmts();
  stmts->push_back(s);
  return new Ctxt{point, stmts, b, tk};
}

AnalysisCtxt::Ctxt *AnalysisCtxt::NewLayer(const Stmt *point,
                                           RemainingStmts *stmts, TurnKind tk,
                                           bool b) {
  return new Ctxt{point, stmts, b, tk};
}

AnalysisCtxt::PCtxt *AnalysisCtxt::filter() {
  PCtxt *me2 = new PCtxt();
  PCtxt *me_false = new PCtxt();

  for (auto &stack : *me) {
    bool outcome = false;
    for (auto &ctxt : *stack->frames) {
      outcome |= ctxt->found;
    }

    if (outcome) {
      me2->push_back(stack);
    } else {
      me_false->push_back(stack);
    }
  }

  me = me2;
  return me_false;
}

bool AnalysisCtxt::equal(Turn *x, Turn *y) { return x == y; }
template <class T>
bool AnalysisCtxt::equalist(T *x, T *y) {
  if (x->size() != y->size()) {
    return false;
  }
  typename T::iterator xi;
  typename T::iterator yi;
  for (xi = x->begin(), yi = y->begin(); xi != x->end() && yi != y->end();
       ++xi, ++yi) {
    if (!equal(*xi, *yi)) {
      return false;
    }
  }
  return true;
}
bool AnalysisCtxt::equal(RemainingStmts *x, RemainingStmts *y) {
  return equalist(x, y);
}
bool AnalysisCtxt::equal(Stack *x, Stack *y, bool strict) {
  // If "strict" then we also care about the paths traversed.
  return equalist(x->frames, y->frames) &&
         (!strict ? true : equalist(x->trace, y->trace));
}
bool AnalysisCtxt::equal(Ctxt *x, Ctxt *y) {
  return equal(x->turn, y->turn) && equal(x->rs, y->rs) &&
         (x->found == y->found) && (x->tk == y->tk);
}

unsigned AnalysisCtxt::deduplicate(bool non_strict_stack_factoring) {
  unsigned result = 0;
  PCtxt *me2 = new PCtxt();

  std::vector<Stack *> v(me->size());

  unsigned idx = 0;
  for (auto &x : *me) {
    bool unique = true;
    for (unsigned i = 0; i < idx; ++i) {
      if (equal(x, v[i], non_strict_stack_factoring)) {
        unique = false;
        result += 1;
        break;
      }
    }
    if (unique) {
      v[idx] = x;
      idx += 1;
    }
  }

  for (unsigned i = 0; i < idx; ++i) {
    me2->push_back(v[i]);
  }

  me = me2;
  return result;
}

bool AnalysisCtxt::subsumes(Ctxt *ctxt1, Ctxt *ctxt2) {
  return (equal(ctxt1->turn, ctxt2->turn) &&
          subsumesShallow(ctxt1->rs, ctxt2->rs) &&
          (ctxt1->found == ctxt2->found) && (ctxt1->tk == ctxt2->tk));
}

template <class T>
bool AnalysisCtxt::subsumesShallow(std::list<T> *list1, std::list<T> *list2) {
  if (list1->size() > list2->size()) {
    return false;
  }

  bool result = true;
  typename std::list<T>::reverse_iterator t1i = list1->rbegin();
  typename std::list<T>::reverse_iterator t2i = list2->rbegin();
  for (; t1i != list1->rend() && t2i != list2->rend(); ++t1i) {
    if (!AnalysisCtxt::equal(*t1i, *t2i)) {
      result = false;
      break;
    }
    ++t2i;
  }
  return result;
}

template <class T>
bool AnalysisCtxt::subsumesDeep(std::list<T> *list1, std::list<T> *list2) {
  bool result = true;
  typename std::list<T>::iterator t1i;
  typename std::list<T>::iterator t2i;
  for (t1i = list1->begin(), t2i = list2->begin();
       t1i != list1->end() && t2i != list2->end(); ++t1i, ++t2i) {
    if (!AnalysisCtxt::subsumes(*t1i, *t2i)) {
      result = false;
      break;
    }
  }
  return result;
}

bool AnalysisCtxt::subsumes(const Stack *stack1, const Stack *stack2,
                            bool strict) {
  return subsumesDeep(stack1->frames, stack2->frames) &&
         (!strict ? true : subsumesShallow(stack1->trace, stack2->trace));
}

unsigned reachable_stmt(clang::LangOptions *lang_opt, SourceManager *SM, const Stmt *Target,
                        AnalysisCtxt &stacks, std::list<const Stmt *> *external_trace,
                        bool (*hook)(clang::LangOptions *lang_opt, clang::SourceManager *_SM, const Stmt *sought_stmt, const Stmt *stmt), bool descend_scopes) {
  stacks.initial = false;

  AnalysisCtxt::PCtxt *OldStacks = stacks.me;
  AnalysisCtxt::PCtxt *NewStacks = new AnalysisCtxt::PCtxt();

  unsigned reaches = 0;

  while (!OldStacks->empty()) {
    auto stack = OldStacks->front();
    OldStacks->pop_front();

    while (!stack->frames->empty()) {
      AnalysisCtxt::Ctxt *Ctxt = stack->frames->front();

      if (Ctxt->found) {
        break;  // Proceed to the next stack if at this one we've found what
                // we're looking for.
      }

      if (Ctxt->rs->empty()) {
        stack->frames->pop_front();
        continue;
      }

      while (!Ctxt->rs->empty()) {
        const Stmt *stmt = Ctxt->rs->front();

        if (hook != nullptr && hook(lang_opt, SM, Target, stmt)) {
          // Quit searching, the target appears to be invalid (e.g., an end to a non-matching start).
          return 0;
        }

        if (stmt == Target) {
          Ctxt->found = true;

#if PF_VERBOSE
          clang::LangOptions LO;
          clang::SourceLocation begin(stmt->getLocStart());
          outs() << reaches << ". Found reachability target at "
                 << begin.printToString(*SM) << ":  "
                 << PFHelper::summarise_sourcecode(
                        LO, SM, CP_MAX_CODELINE_LENGTH, stmt)
                 << "\n";
#endif // PF_VERBOSE

          ++reaches;

          if (!stacks.resume_include_found) {
            Ctxt->rs->pop_front();
          }

          break;  // Will proceed to the next stack.
        }

        if (stacks.trace || stack->do_trace || Ctxt->do_trace) {
          stack->trace->push_back(stmt);
        }

        if (external_trace != nullptr) {
          external_trace->push_back(stmt);
        }

#if CF_VERBOSE
        errs() << ".";
#endif
        Ctxt->rs->pop_front();

        if (isa<CompoundStmt>(stmt)) {
#if CF_VERBOSE
          errs() << "b";
#endif
          if (descend_scopes) {
            const CompoundStmt *block = cast<CompoundStmt>(stmt);
            AnalysisCtxt::RemainingStmts *stmts =
                new AnalysisCtxt::RemainingStmts();
            for (Stmt *s : block->body()) {
              stmts->push_back(s);
            }
            stack->frames->push_front(AnalysisCtxt::NewLayer(
                stmt, stmts, AnalysisCtxt::TurnKind::Block));
          }
          break;
        } else if (isa<IfStmt>(stmt)) {
#if CF_VERBOSE
          errs() << "i";
#endif
          if (descend_scopes) {
            const IfStmt *ifstmt = cast<IfStmt>(stmt);
            const Stmt *cond = ifstmt->getCond();
#if DETAILED_FLOW_TRACKING
            if (nullptr != ifstmt->getElse()) {
              AnalysisCtxt::Stack *s2 = stacks.clone(stack);
              s2->frames->push_front(AnalysisCtxt::NewLayer(
                  cond, ifstmt->getElse(), AnalysisCtxt::TurnKind::IfThenElse));
            }
            assert(nullptr != ifstmt->getThen());
            stack->frames->push_front(AnalysisCtxt::NewLayer(
                cond, ifstmt->getThen(), AnalysisCtxt::TurnKind::IfThenElse));
#else
            // This version of the code doesn't implement precise flow tracking,
	    // it only helps determine whether a statement is reachable.
            if (nullptr != ifstmt->getElse()) {
              stack->frames->push_front(AnalysisCtxt::NewLayer(
                  cond, ifstmt->getElse(), AnalysisCtxt::TurnKind::IfThenElse));
            }
            assert(nullptr != ifstmt->getThen());
            stack->frames->push_front(AnalysisCtxt::NewLayer(
                cond, ifstmt->getThen(), AnalysisCtxt::TurnKind::IfThenElse));
#endif
          }
          break;
        } else if (isa<ForStmt>(stmt) || isa<WhileStmt>(stmt) || isa<DoStmt>(stmt)) {
#if CF_VERBOSE
          errs() << "l";
#endif
          if (descend_scopes) {
            const Stmt *cond = nullptr;
            const Stmt *body = nullptr;
            AnalysisCtxt::TurnKind tk;
            if (isa<ForStmt>(stmt)) {
              const ForStmt *forstmt = cast<ForStmt>(stmt);
              cond = forstmt->getCond();
              body = forstmt->getBody();
              tk = AnalysisCtxt::TurnKind::For;
            } else if (isa<WhileStmt>(stmt)) {
              const WhileStmt *whilestmt = cast<WhileStmt>(stmt);
              cond = whilestmt->getCond();
              body = whilestmt->getBody();
              tk = AnalysisCtxt::TurnKind::While;
            } else if (isa<DoStmt>(stmt)) {
              const DoStmt *dostmt = cast<DoStmt>(stmt);
              cond = dostmt->getCond();
              body = dostmt->getBody();
              tk = AnalysisCtxt::TurnKind::Do;
            } else {
              llvm_unreachable("Unhandled form of loop encountered");
            }
            assert(nullptr != body);
            stack->frames->push_front(AnalysisCtxt::NewLayer(cond, body, tk));
          }
          break;
        } else if (isa<CallExpr>(stmt)) {
#if CF_VERBOSE
          errs() << "c";
#endif
#if FOLLOW_FUNCTION_CALLS
          const CallExpr *ce = cast<CallExpr>(stmt);
          const FunctionDecl *FD = ce->getDirectCallee();
          if (FD->hasBody()) {
            const FunctionDecl *FDef;
            assert(FD->hasBody(FDef));
            stack->frames->push_front(AnalysisCtxt::NewLayer(
                stmt, FDef->getBody(), AnalysisCtxt::TurnKind::Fun));
          }
#endif
          break;
          /*
        } else if (isa<DeclStmt>(stmt)) {
#if CF_VERBOSE
          errs() << "?";
#endif
          const DeclStmt *d = cast<DeclStmt>(stmt);
          d->
          const FunctionDecl *FD = ce->getDirectCallee();
          if (FD->hasBody()) {
            const FunctionDecl *FDef;
            assert(FD->hasBody(FDef));
            stack->frames->push_front(AnalysisCtxt::NewLayer(stmt,
FDef->getBody(), AnalysisCtxt::TurnKind::Fun));
          }
          break;
          */

          /*
            139 :::BinaryOperator
            82 :::DeclStmt
            18 :::DoStmt
            3 :::LabelStmt
            4 :::ParenExpr
            22 :::ReturnStmt
            12 :::UnaryOperator
          */

        } else if (isa<BinaryOperator>(stmt)) {
#if CF_VERBOSE
          errs() << "?";
#endif
          const BinaryOperator *bo = cast<BinaryOperator>(stmt);
          if (bo->isAssignmentOp()) {
            std::list<const CallExpr *> calls;
            std::list<const Expr *> exprs;
            exprs.push_back(bo->getRHS());
            while (!exprs.empty()) {
              const Expr *e = exprs.front();
              exprs.pop_front();
              if (isa<CallExpr>(e)) {
                const CallExpr *ce = cast<CallExpr>(e);
                // errs() << ":::" <<
                // ce->getCalleeDecl()->getAsFunction()->getName()
                // << "\n";

                calls.push_back(ce);
              } else if (isa<BinaryOperator>(e)) {
                exprs.push_back(cast<BinaryOperator>(e)->getLHS());
                exprs.push_back(cast<BinaryOperator>(e)->getRHS());
              }
            }

            AnalysisCtxt::RemainingStmts *stmts =
                new AnalysisCtxt::RemainingStmts();
            for (const Stmt *s : calls) {
              stmts->push_back(s);
            }
            stack->frames->push_front(AnalysisCtxt::NewLayer(
                stmt, stmts, AnalysisCtxt::TurnKind::Block /*FIXME*/));
          }
          break;

        } else {
          if (nullptr != stmt) {
//#if CF_VERBOSE
            errs() << ":::" << stmt->getStmtClassName() << "\n";
//#endif
            // clang::LangOptions LO;
            // outs() << ":  " <<
            // PFHelper::summarise_sourcecode(LO, SM,
            // CP_MAX_CODELINE_LENGTH, stmt) << "\n";
          }
        }
      }
    }

    if (!stack->frames->empty()) {
      NewStacks->push_back(stack);
    }
  }

  stacks.me = NewStacks;
  delete OldStacks;

  return reaches;
}

}  // namespace pitchfork
