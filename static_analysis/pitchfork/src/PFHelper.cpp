/*
   Copyright 2021 University of Pennsylvania
   Pitchfork project. Nik Sultana, UPenn, 2019

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
#include "PFHelper.h"

namespace pitchfork {
using namespace clang;

std::string PFHelper::summarise_sourcecode(clang::LangOptions LO,
                                             SourceManager *SM, unsigned width,
                                             const Stmt *s) {
  std::string source_code = Lexer::getSourceText(
      CharSourceRange::getTokenRange(s->getSourceRange()), *SM, LO);
  if (source_code.length() > width) {
    if (std::string::npos != source_code.find_first_of("\n")) {
      source_code.erase(source_code.find_first_of("\n"));
    }
    if (source_code.length() > width) {
      source_code.erase(width - 3);
    }
    source_code += "...";
  }
  return source_code;
}

std::string PFHelper::summarise_sourcecode(clang::LangOptions LO,
                                             SourceManager *SM, unsigned width,
                                             const Decl *s) {
  std::string source_code = Lexer::getSourceText(
      CharSourceRange::getTokenRange(s->getSourceRange()), *SM, LO);
  if (source_code.length() > width) {
    if (std::string::npos != source_code.find_first_of("\n")) {
      source_code.erase(source_code.find_first_of("\n"));
    }
    if (source_code.length() > width) {
      source_code.erase(width - 3);
    }
    source_code += "...";
  }
  return source_code;
}

const Stmt *PFHelper::locate_stmt(unsigned sought_line_no, SourceManager *SM,
                                    std::queue<const Stmt *> &continuation) {
  while (!continuation.empty()) {
    const Stmt *stmt = continuation.front();
    continuation.pop();

    assert(nullptr != stmt);

    clang::LangOptions LO;
    clang::SourceLocation begin(stmt->getLocStart());
    clang::SourceLocation pre_end(stmt->getLocEnd());
    clang::SourceLocation end(
        clang::Lexer::getLocForEndOfToken(pre_end, 0, *SM, LO));
    FullSourceLoc BeginSourceLoc = FullSourceLoc(begin, *SM);
    FullSourceLoc EndSourceLoc = FullSourceLoc(end, *SM);

    if (BeginSourceLoc.getExpansionLineNumber() == sought_line_no ||
        sought_line_no == EndSourceLoc.getExpansionLineNumber()) {
      outs() << "Determined line " << sought_line_no << " to be this code:\n\t"
             << summarise_sourcecode(LO, SM, CP_MAX_CODELINE_LENGTH, stmt)
             << "\n";
      return stmt;
    }

    if (BeginSourceLoc.getExpansionLineNumber() <= sought_line_no &&
        sought_line_no <= EndSourceLoc.getExpansionLineNumber()) {
#ifdef CF_DEBUG
      outs() << "Considering: "
             << summarise_sourcecode(LO, SM, CP_MAX_CODELINE_LENGTH, stmt)
             << "\n";
#endif

      if (isa<CompoundStmt>(stmt)) {
        const CompoundStmt *block = cast<CompoundStmt>(stmt);
        for (Stmt *s : block->body()) {
          continuation.push(s);
        }
      } else if (isa<IfStmt>(stmt)) {
        errs() << "warning: crossing a conditional at "
               << SM->getFilename(begin) << " (line "
               << begin.printToString(*SM) << "):\n\t"
               << summarise_sourcecode(LO, SM, CP_MAX_CODELINE_LENGTH, stmt)
               << "\n";
        const IfStmt *ifstmt = cast<IfStmt>(stmt);
        continuation.push(ifstmt->getThen());
        if (nullptr != ifstmt->getElse()) {
          continuation.push(ifstmt->getElse());
        }
      } else if (isa<ForStmt>(stmt) || isa<WhileStmt>(stmt)) {
        errs() << "Could not reach line " << sought_line_no
               << " because of loop at " << SM->getFilename(begin) << " (line "
               << begin.printToString(*SM) << "):\n\t"
               << summarise_sourcecode(LO, SM, CP_MAX_CODELINE_LENGTH, stmt)
               << "\n";
        return nullptr;
      }
    }
  }
  return nullptr;
}

const Stmt *PFHelper::locate_stmt(unsigned sought_line_no, SourceManager *SM,
                                    const Stmt *stmt) {
  std::queue<const Stmt *> continuation;
  continuation.push(stmt);
  return locate_stmt(sought_line_no, SM, continuation);
}

} // namespace pitchfork
