cmake_minimum_required(VERSION 3.5.1)

set(LLVM_LINK_COMPONENTS support)

set(CMAKE_CXX_STANDARD 14)

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
#set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra") -- this also shows distracting warnings from Clang's source code
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall")

set(Boost_USE_STATIC_LIBS OFF)
set(Boost_USE_MULTITHREADED ON)
set(Boost_USE_STATIC_RUNTIME OFF)
find_package(Boost 1.58.0 COMPONENTS filesystem)

project(pitchfork)

file(GLOB_RECURSE headers include/*.h)
file(GLOB_RECURSE sources src/*.cpp)

if(Boost_FOUND)

  add_clang_executable(${PROJECT_NAME}
    ${sources}
    )

  target_include_directories(${PROJECT_NAME}
    PRIVATE
    ${Boost_INCLUDE_DIRS}
    include/
    )

  target_link_libraries(${PROJECT_NAME}
    PRIVATE
    ${Boost_LIBRARIES}
    clangToolingCore
    clangTooling
    clangFrontend
    clangAST
    clangASTMatchers
    clangBasic
    clangLex
    clangRewrite
    )

endif()
