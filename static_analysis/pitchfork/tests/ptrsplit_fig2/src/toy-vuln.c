#include "pitchfork.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include "toy.h"

void initkey() {
  for (int i = 0; i < 50; i++) {
    key[i] = 'a';
  }
}

void greeter (char *str) {
  printf(str); printf(", welcome!\n");
}

char * encrypt (char * plaintext, int sz) {
  char *ciphertext = (char * ) (malloc (sz));
  for (int i=0; i<sz; i++) {
    ciphertext[i]=plaintext[i] ^ key[i];
  }
  return ciphertext;
}

int main (int argc, char **argv) {
  char username[64]; // char text[1024]; // FIXME working around blind spot in de/marshaller
  char *text = malloc(sizeof(*text) * 1024);
  char *key_ptr = key;

pitchfork_start("Classified");
  initkey();
pitchfork_end("Classified");

  printf("Enter username: ");
  fgets(username, sizeof(username), stdin);
  greeter(username);

  printf("Enter plaintext: ");
  fgets(text, sizeof(text), stdin);

  char *ciphertext = NULL;
pitchfork_start("Classified");
  ciphertext = encrypt(text, strlen(text));
pitchfork_end("Classified");

  printf("Cipher text: ");
  for (unsigned i=0; i<strlen(text); i++) {
    printf("%x ",ciphertext[i]);
  }
  printf("\n");
  return 0;
}
