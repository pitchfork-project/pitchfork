#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include "signal.h"
#include <stdlib.h>
#include <sys/mman.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <sys/wait.h>

#include "ptrsplit_interface.h"
#include "toy.h"

struct compart comparts[NO_COMPARTS] = {
  {.name = "ptrsplit", .uid = 65534, .gid = 65534, .path = "/var/empty/ptrsplit"},
  {.name = "priv", .uid = 0, .gid = 0, .path = "/var/empty/priv"}
};

struct extension_id * init_key_ext = NULL;
struct extension_id * encrypt_ext = NULL;

/*****************************************
 * MARK: Code for marshalling/unmarshalling
 * (All copied from wget-2017 patch interface.c)
 * ******/

#define MARSHALL_CAT_(a, b) \
        a##b \

#define MARSHALL_CAT(a, b) \
        MARSHALL_CAT_(a, b) \

void marshall_string(char* buf, size_t* buf_index_, char* str) {
  size_t buf_index = *buf_index_;
  if (str) {
    //NULL terminate
    size_t str_length = strlen(str) + 1;
    memcpy(&buf[buf_index], &str_length, sizeof(str_length));
    buf_index += sizeof(str_length);

    memcpy(&buf[buf_index], str, str_length);
    buf_index += str_length;
  } else {
    memset(&buf[buf_index], 0, sizeof(size_t));
    buf_index += sizeof(size_t);
  }
  *buf_index_ = buf_index;
}

void unmarshall_string_(char* buf, size_t* buf_index_, char** str) {
  *str = NULL;
  size_t buf_index = *buf_index_;
  size_t str_length = 0;
  memcpy(&str_length, &buf[buf_index], sizeof(str_length));
  buf_index += sizeof(str_length);
  if (str_length > 0) {
    //consider for NULL terminated
    *str = calloc(str_length, sizeof(char));
    memcpy(*str, &buf[buf_index], str_length);
    buf_index += str_length;
  }
  *buf_index_ = buf_index;
}

#define unmarshall_string(buf, buf_index, str) \
        unmarshall_string_(buf, buf_index, &str)

//only use for primitive data types
#define marshall_prim(buf, buf_index, data) \
  size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data); \
  memcpy(&buf[*(buf_index)], &data, MARSHALL_CAT(marshall, __LINE__)); \
  *(buf_index) += MARSHALL_CAT(marshall, __LINE__); \

#define unmarshall_prim(buf, buf_index, data) \
  size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data); \
  memcpy(&data, &buf[*(buf_index)], MARSHALL_CAT(marshall, __LINE__)); \
  *(buf_index) += MARSHALL_CAT(marshall, __LINE__); \

void marshall_mem(char* buf, size_t* buf_index_, void* str, size_t len) {
  size_t buf_index = *buf_index_;
  memcpy(&buf[buf_index], &len, sizeof(len));
  buf_index += sizeof(len);
  memcpy(&buf[buf_index], str, len);
  buf_index += len;
  *buf_index_ = buf_index;
}

/**************************
 * MARK: Interface used for toy
 * ******/ 

/* This runs on priv, patching `void initkey (int sz)` */
struct extension_data ext_init_key(struct extension_data data) {
  struct extension_data result;

  int sz;
  char* buf = data.buf;
  size_t buf_index = 0;
  unmarshall_prim(buf, &buf_index, sz);

  initkey();

  return result;
}

/* This is called by compartment `toy` */
struct extension_data ext_init_key_to_arg(int sz) {
  struct extension_data data;
  char* buf = data.buf;
	size_t buf_index = 0;

  marshall_prim(buf, &buf_index, sz);

  data.bufc = buf_index;
  return data;
}

/* This runs on priv, patching `encrypt (char * plaintext, int sz)` */
struct extension_data ext_encrypt(struct extension_data data) {
  struct extension_data result;
  char* plaintext;
  int sz;

  char* buf = data.buf;
  size_t buf_index = 0;
  unmarshall_string(buf, &buf_index, plaintext);
  unmarshall_prim(buf, &buf_index, sz);

  encrypt(plaintext, sz);

  char* result_buf = result.buf;
  size_t result_index = 0;

  marshall_mem(result_buf, &result_index, (void*)ciphertext, sz);
  marshall_prim(result_buf, &result_index, sz);

  result.bufc = result_index;
  return result;
}

/* This is called by compartment `toy` */
struct extension_data ext_encrypt_to_arg(char * plaintext, int sz) {
  struct extension_data data;
  char* buf = data.buf;
	size_t buf_index = 0;

  marshall_string(buf, &buf_index, plaintext);
  marshall_prim(buf, &buf_index, sz);

  data.bufc = buf_index;
  return data;
}

void exp_encrpt_from_resp(struct extension_data data) {
  char* result_text;
  int sz;

  char* buf = data.buf;
  size_t buf_index = 0;

  unmarshall_string(buf, &buf_index, result_text);
  unmarshall_prim(buf, &buf_index, sz);

  ciphertext = (char * ) (malloc (sz));
  for (int i=0; i<sz; i++) {
    ciphertext[i]=result_text[i];
  }
}
