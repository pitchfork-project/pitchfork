
void __pf_Classified_1_call_demarshall(struct extension_data data, char * *ciphertext, char * *text);

struct extension_data __pf_Classified_1_call_marshall(char * ciphertext, char * text);
#include <stdlib.h>
#include <string.h>
#include <compart_base.h>
#include <libcompart_serialisation.h>
void __pf_Classified_1_call_demarshall(struct extension_data data, char * *ciphertext, char * *text)
{
  char* buf = data.buf;
  size_t buf_index = 0;
  unmarshall_string(buf, &buf_index, *ciphertext);
  unmarshall_string(buf, &buf_index, *text);
}
struct extension_data __pf_Classified_1_call_marshall(char * ciphertext, char * text)
{
  struct extension_data data;
  char* buf = data.buf;
  size_t buf_index = 0;
  marshall_string(buf, &buf_index, ciphertext);
  marshall_string(buf, &buf_index, text);
  data.bufc = buf_index;
  return data;
}
