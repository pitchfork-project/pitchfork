

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include "toy.h"

void initkey() {
  for (int i = 0; i < 50; i++) {
    key[i] = 'a';
  }
}

void greeter (char *str) {
  printf(str); printf(", welcome!\n");
}

char * encrypt (char * plaintext, int sz) {
  char *ciphertext = (char * ) (malloc (sz));
  for (int i=0; i<sz; i++) {
    ciphertext[i]=plaintext[i] ^ key[i];
  }
  return ciphertext;
}

#include "/home/nik/chopflow/static_analysis/pitchfork/tests/ptrsplit_fig2/pitchforked/pitchfork_ifx.h"
void __pf_Classified_1_call(char * *ciphertext, char * *text);
void __pf_Classified_0_call();
int main (int argc, char **argv) {
  #include "/home/nik/chopflow/static_analysis/pitchfork/tests/ptrsplit_fig2/pitchforked/pitchfork_init.c"
  char username[64]; // char text[1024]; // FIXME working around blind spot in de/marshaller
  char *text = malloc(sizeof(*text) * 1024);
  char *key_ptr = key;

__pf_Classified_0_call();

  printf("Enter username: ");
  fgets(username, sizeof(username), stdin);
  greeter(username);

  printf("Enter plaintext: ");
  fgets(text, sizeof(text), stdin);

  char *ciphertext = NULL;
__pf_Classified_1_call(&ciphertext, &text);

  printf("Cipher text: ");
  for (unsigned i=0; i<strlen(text); i++) {
    printf("%x ",ciphertext[i]);
  }
  printf("\n");
  return 0;
}

#include "/home/nik/chopflow/static_analysis/pitchfork/tests/ptrsplit_fig2/pitchforked/pitchfork_ix_toy-vuln.c"
