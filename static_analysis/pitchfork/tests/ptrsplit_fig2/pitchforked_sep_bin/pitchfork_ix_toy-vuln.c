// Autogenerated file from running Pitchfork

#include <stdlib.h>
#include "compart_api.h"

// Interface for compartment "Classified"

struct extension_data __pf_Classified_0_call_remote(struct extension_data __arg) {
  initkey();
  return __arg;
}

struct extension_id *__pf_Classified_0_call_ext = NULL;

void __pf_Classified_0_call() {
  struct extension_data __arg = {0};
  (void)compart_call_fn(__pf_Classified_0_call_ext, __arg);
}

// Interface for compartment "Classified"

#include "/home/nik/chopflow/static_analysis/pitchfork/tests/ptrsplit_fig2/pitchforked/Classified1_output.c"

struct extension_data __pf_Classified_1_call_remote(struct extension_data __arg) {
  char * ciphertext;
  char * text;
  __pf_Classified_1_call_demarshall(__arg, &ciphertext, &text);
  ciphertext = encrypt(text, strlen(text));
  encrypt(text, strlen(text));
  return __pf_Classified_1_call_marshall(ciphertext, text);
}

struct extension_id *__pf_Classified_1_call_ext = NULL;

void __pf_Classified_1_call(char * *ciphertext, char * *text) {
  struct extension_data __arg = __pf_Classified_1_call_marshall(*ciphertext, *text);
  struct extension_data __result = compart_call_fn(__pf_Classified_1_call_ext, __arg);
  __pf_Classified_1_call_demarshall(__result, ciphertext, text);
}
