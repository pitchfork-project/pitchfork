#!/bin/sh -e

TESTS_SRC_DIR=/home/nik/chopflow/static_analysis/pitchfork/tests/
TESTS_TGT_DIR=${TESTS_SRC_DIR}/build

if [ -e "${TESTS_TGT_DIR}" ]
then
  echo "Build directory (${TESTS_TGT_DIR}) already exists" >&2
  exit 1
fi

mkdir ${TESTS_TGT_DIR}
cd ${TESTS_TGT_DIR}
cmake ..
make
