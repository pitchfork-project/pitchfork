#!/bin/sh -e

PITCHFORK_BIN=/home/nik/pitchfork1/build/bin/pitchfork
TESTS_SRC_DIR=/home/nik/chopflow/static_analysis/pitchfork/tests/
TESTS_TGT_DIR=${TESTS_SRC_DIR}/build

if [ ! -f "${PITCHFORK_BIN}" ]
then
  echo "Pitchfork binary (${PITCHFORK_BIN}) doesn't exist" >&2
  exit 1
fi

if [ ! -e "${TESTS_TGT_DIR}" ]
then
  echo "Build directory (${TESTS_TGT_DIR}) doesn't exist -- run build.sh" >&2
  exit 1
fi

if [ -z $TARGETS ]
then
  TARGETS=`ls ${TESTS_SRC_DIR}/*.c`
fi
echo "TARGETS=${TARGETS}"

for FILE in ${TARGETS}
do
  echo "Running pitchfork on ${FILE}"
  ${PITCHFORK_BIN} -parse -p ${TESTS_TGT_DIR} ${FILE}
  if [ $? -eq 0 ]
  then
    echo "  Parsed: OK"

    PF_ARGS_FILE="${FILE}_pitchfork_args"
    PF_ARGS=
    if [ -e "${PF_ARGS_FILE}" ]
    then
      PF_ARGS=$(cat ${PF_ARGS_FILE})
      echo "  Pitchfork args (${PF_ARGS_FILE}): ${PF_ARGS}"
    else
      echo "  Pitchfork args (${PF_ARGS_FILE}): (file not found)"
    fi

    CMD="${PITCHFORK_BIN} -p ${TESTS_TGT_DIR} ${PF_ARGS} ${FILE}"
    echo "Executing: ${CMD}"
    eval ${CMD}
  else
    echo "  Parsed: FAIL"
  fi
  echo
done
