import os
import sys

LIBCOMPART_PATH_VAR = 'LIBCOMPART_PATH'
PITCHFORK_INCLUDE_PATH_VAR = 'PF_INCLUDE_PATH'

def Settings( **kwargs ):
  opts = ['-Wall', '-Wextra']

  try:
    opts = ['-I', os.environ[LIBCOMPART_PATH_VAR]] + opts
  except KeyError:
    raise Exception("Could not find $" + LIBCOMPART_PATH_VAR + " defined in environment\n")

  try:
    opts = ['-I', os.environ[PITCHFORK_INCLUDE_PATH_VAR]] + opts
  except KeyError:
    raise Exception("Could not find $" + PITCHFORK_INCLUDE_PATH_VAR + " defined in environment\n")

  return {
    'flags': opts,
  }
