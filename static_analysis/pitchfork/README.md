![Pitchfork](pitchfork_icon.png)

**Pitchfork** uses in-program annotations to transform programs to use [libcompart](https://gitlab.com/pitchfork-project/libcompart).
The version of libcompart used by Pitchfork is [packaged with Pitchfork](../../compartmenting/libcompart).
The latest release of libcompart can be found [online](https://gitlab.com/pitchfork-project/libcompart).

## Building
Run `setup.sh` to install dependencies and build. **IMPORTANT** `setup.sh` must be run from inside this directory.

## Usage
Run `./pitchfork -help` to get a list of options supported.

## Tests and Examples
See `tests/`

For each test, the `pitchforked/` directory shows the result of running Pitchfork on that example.

Some tests replicate manually-compartmentalised applications, but automate the analysis and transformation.
The manually-compartmentalised counterparts are in ../../compartmenting.
As with the manually-compartmentalised examples, the Pitchfork versions do not distributed exploit code.
(See the [release README](../../../../../README.md) for a description of excluded files.)

## License
[Apache 2.0](LICENSE)

## Author
[Nik Sultana](http://www.cs.iit.edu/~nsultana1/)
