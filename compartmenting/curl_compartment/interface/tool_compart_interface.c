#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdarg.h>
#include <pthread.h>
#include <stdbool.h>
#include <sys/wait.h>
#include <sys/types.h>

#include "tool_compart_interface.h"
#include "tool_help.h"
#include "tool_hugehelp.h"
#include "tool_cfgable.h"
#include "tool_main.h"
#include "tool_paramhlp.h"
#include "tool_getparam.h"
#include "tool_parsecfg.h"
#include "tool_helpers.h"
#include "tool_operate.h"
#include "tool_libinfo.h"
#include "../include/curl/curl.h"

#include <json-c/json.h>


struct compart comparts[NO_COMPARTS] =
  {
    {.name = "main", .uid = 1000, .gid = 1000, .path = NULL},
    {.name = "operate", .uid = 1000, .gid = 1000, .path = NULL},
    {.name = "help", .uid = 1000, .gid = 1000, .path = NULL},
    {.name = "parseArgs", .uid = 1000, .gid = 1000, .path = NULL},
    {.name = "parseCfg", .uid = 1000, .gid = 1000, .path = NULL},
    {.name = "getArgs", .uid = 1000, .gid = 1000, .path = NULL},
    {.name = "serialTransfers", .uid = 1000, .gid = 1000, .path = NULL}
  };

//Operation Fork Functions
struct extension_id *op_ext = NULL;
struct extension_id * get_opc_ext = NULL;


//-----------------------------------------------Non Serializer Compartments-----------------------------
//Help Comparment Functions
struct extension_id *hlp_ext = NULL;
struct extension_id *man_ext = NULL;
struct extension_id *ver_ext = NULL;
struct extension_id *eng_ext = NULL;

//GetArgs Functions
struct extension_id *get_param_ext = NULL;
struct extension_id *pwd_ext = NULL;
struct extension_id *useragent_ext = NULL;

//-----------------------------------------------Serialized Compartmens-----------------------------

//Parse Config
struct extension_id * parsecfg_ext = NULL;
struct extension_id * parsecfg_sync_op_ext = NULL;

//Parse Args
struct extension_id * parseargs_ext = NULL;
struct extension_id * parseargs_sync_op_ext = NULL;

//Serial Transfer
struct extension_id * serialtransfers_ext = NULL;
struct extension_id * serialtransfers_sync_op_ext = NULL;

struct extension_data blank = {0,{0}};
struct json_object * op_cfg_map = NULL;

/* ----------------------------tool_help compartmentalization-----------------------------------*/
struct extension_data run_help (struct extension_data data) 
{
  tool_help();
  return data;
}
struct extension_data run_big_help ( struct extension_data data)
{
  hugehelp();
  return data;
}
struct extension_data run_ver_info( struct extension_data data)
{
  tool_version_info();
  return data;
}
struct extension_data run_list_engines ( struct extension_data data)
{
 tool_list_engines();
 return data;
} 
/* ----------------------------checkpasswd compartmentalization-----------------------------------*/
struct extension_data package_passwd_args (char * kind, size_t i, bool last, char * passwd) 
{ 
  struct extension_data result = {0,{0}};
  size_t pos=0;

  memcpy(&result.buf, kind, (strlen(kind)+1) * sizeof(char));
  pos += (strlen(kind)+1);

  memcpy(result.buf + pos,&i,sizeof(size_t)); 
  pos += sizeof(size_t);

  memcpy(result.buf + pos, &last ,sizeof(_Bool)); 
  pos += sizeof(_Bool);

  memcpy(result.buf + pos, passwd, (strlen(passwd)+1) * sizeof(char));
  pos += (strlen(passwd)+1);

  result.bufc = pos;

  return result;

}
struct extension_data run_check_passwd(struct extension_data data)
{
  //Unpack
  size_t pos = 0;
  size_t element_size= 0;

  while(data.buf[(pos + element_size)] != '\0') 
  { 
    element_size++;
  }
  char * kind = calloc(1,element_size+1);
  memcpy(kind,data.buf,(element_size +1) * sizeof(char));
  pos += (element_size + 1);

  size_t i = 0;
  memcpy(&i, data.buf + pos ,sizeof(size_t) * sizeof(char));
  pos+= sizeof(size_t);

  bool last = 0;
  memcpy(&last, data.buf + pos,sizeof(_Bool)* sizeof(char));
  pos+= sizeof(_Bool);

  element_size = 0;
  while(data.buf[(pos + element_size)] != '\0') 
  { 
    element_size++;
  }
  char * passwd = calloc(1,element_size+1);
  memcpy(passwd,data.buf + pos,(element_size +1) * sizeof(char));
  pos += (element_size + 1);

  //Run
  CURLcode pass_ok = checkpasswd(kind,i,last,&passwd);
  
  //Return
  pos =0; 
  
  struct extension_data result = {0,{0}};
  memcpy(data.buf,&pass_ok,sizeof(CURLcode));
  pos += sizeof(CURLcode);

  memcpy(result.buf + pos, passwd, (strlen(passwd) + 1));
  pos += (strlen(passwd) + 1);

  result.bufc = pos;

  return result;
}
CURLcode unpack_check_passwd_res (struct extension_data data, char ** config_pass)
{
  CURLcode result = 0; 
  size_t pos =0;
  size_t element_size = 0;

  memcpy(&result,data.buf,sizeof(CURLcode));
  pos+=sizeof(CURLcode);

  while(data.buf[(pos + element_size)] != '\0')
  {
    element_size ++;
  }
  char  * passwd = calloc(1,(element_size +1) * sizeof(char));
  memcpy(passwd,&(data.buf[pos]),element_size+1);
  *config_pass = passwd;

  return result;
}
/* ----------------------------my user agent compartmentalization-----------------------------------*/
struct extension_data run_my_user_agent(struct extension_data data) 
{ 
  char *  result = my_useragent();
  memcpy(&data.buf,result,strlen(result)+1);
  data.bufc = strlen(result)+1;
  return data;
}
char * unpack_my_user_response(struct extension_data data) 
{
  char* result = calloc(1,data.bufc);
  memcpy(result,&data.buf,data.bufc);
  return result;
}

//---------------------------------------Serializer Generated Functions-----------------------------------
void marshall_string(char* buf, size_t* buf_index_, char* str)
{
  size_t buf_index = *buf_index_;

  if(str)
  {
    //NULL terminate
    size_t str_length = strlen(str) + 1;
    memcpy(&buf[buf_index], &str_length, sizeof(str_length));
    buf_index += sizeof(str_length);

    memcpy(&buf[buf_index], str, str_length);
    buf_index += str_length;
  }
  else
  {
    memset(&buf[buf_index], 0, sizeof(size_t));
    buf_index += sizeof(size_t);
  }

  *buf_index_ = buf_index;
}
void unmarshall_string_(char* buf, size_t* buf_index_, char** str)
{
  *str = NULL;
  size_t buf_index = *buf_index_;

  size_t str_length = 0;
  memcpy(&str_length, &buf[buf_index], sizeof(str_length));
  buf_index += sizeof(str_length);

  if(str_length > 0)
  {
    //consider for NULL terminated
    *str = calloc(str_length, sizeof(char));
    memcpy(*str, &buf[buf_index], str_length);
    buf_index += str_length;
  }

  *buf_index_ = buf_index;
}

//---Global Config Marshall / Unmarshall
void marshall_struct_GlobalConfig(char* buf, size_t* buf_index_, struct GlobalConfig* TEST_data)
{
  size_t buf_index = *buf_index_;
  size_t size_of_element_= 0; //Place Holder


  if(TEST_data)
  {
    //Marshall my pointer
    marshall_prim(buf, &buf_index, TEST_data);
    
    //Marshall a size placeholder
    size_t placeholder_index = buf_index;
    marshall_prim(buf, &buf_index, size_of_element_);

    marshall_prim(buf, &buf_index, TEST_data->parallel_connect);
    marshall_prim(buf, &buf_index, TEST_data->errors);
    marshall_prim(buf, &buf_index, TEST_data->isatty);
    marshall_prim(buf, &buf_index, TEST_data->parallel_max);
    marshall_prim(buf, &buf_index, TEST_data->showerror);
    marshall_prim(buf, &buf_index, TEST_data->errors_fopened);
    marshall_string(buf, &buf_index, TEST_data->libcurl);
    marshall_prim(buf, &buf_index, TEST_data->tracetime);
    marshall_prim(buf, &buf_index, TEST_data->noprogress);
    marshall_prim(buf, &buf_index, TEST_data->mute);
    marshall_prim(buf, &buf_index, TEST_data->trace_stream);
    marshall_prim(buf, &buf_index, TEST_data->parallel);
    marshall_string(buf, &buf_index, TEST_data->trace_dump);
    marshall_prim(buf, &buf_index, TEST_data->trace_fopened);
    marshall_prim(buf, &buf_index, TEST_data->progressmode);
    marshall_prim(buf, &buf_index, TEST_data->fail_early);
    marshall_prim(buf, &buf_index, TEST_data->styled_output);
    marshall_prim(buf, &buf_index, TEST_data->first);
    marshall_prim(buf, &buf_index, TEST_data->current);
    marshall_prim(buf, &buf_index, TEST_data->last);
    marshall_prim(buf, &buf_index,TEST_data->errors);
    marshall_prim(buf, &buf_index,TEST_data->trace_stream);
    marshall_prim(buf, &buf_index,TEST_data->tracetype);

    //Marshall the actual size
    marshall_prim(buf,&placeholder_index,buf_index);

  }
  else
  {
    size_t size_of_element_= 0;

  }

  *buf_index_ = buf_index;
}
void _unmarshall_struct_GlobalConfig(char* buf, size_t* buf_index_, struct GlobalConfig** TEST_data)
{
  size_t size_of_element_= 0;
  size_t buf_index = *buf_index_;
  unmarshall_prim(buf, &buf_index, size_of_element_);
  // *TEST_data = NULL;
  if(size_of_element_)
  {
    // *TEST_data = calloc(1, sizeof(**TEST_data));
    struct GlobalConfig* TEST_data_ref = *TEST_data;
    unmarshall_prim(buf, &buf_index, TEST_data_ref->parallel_connect);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->errors);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->isatty);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->parallel_max);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->showerror);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->errors_fopened);
    unmarshall_string(buf, &buf_index, TEST_data_ref->libcurl);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->tracetime);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->noprogress);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->mute);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->trace_stream);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->parallel);
    unmarshall_string(buf, &buf_index, TEST_data_ref->trace_dump);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->trace_fopened);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->progressmode);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->fail_early);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->styled_output);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->first);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->current);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->last);
    unmarshall_prim(buf,&buf_index,TEST_data_ref->errors);
    unmarshall_prim(buf,&buf_index,TEST_data_ref->trace_stream);
    unmarshall_prim(buf,&buf_index,TEST_data_ref->tracetype);
  }
  *buf_index_ = buf_index;
}

#define unmarshall_struct_GlobalConfig(a, b, c) 	_unmarshall_struct_GlobalConfig(a, b, &c)

void ext_global_test_from_arg(struct extension_data data, struct GlobalConfig ** config)
{
  char* buf = data.buf;
  size_t buf_index = 0;

  //Unmarshall the reference to a reference pointer
  struct GlobalConfig * key = NULL;
  unmarshall_prim(buf,&buf_index,key);
  if(*config) //
  { 
    unmarshall_struct_GlobalConfig(buf, &buf_index,*config);
    add_map_key_value(key,*config);
  }  
  else //Check Mapping for the value of ref_ptr
  { 
    *config = (struct GlobalConfig *)get_map_value(key);
    if(!*config) //Thre ref pointer is not found in the map, create a new pointer and add it to the map for the rcvd reference.
    { 
      *config = calloc(1,sizeof(struct GlobalConfig));
      add_map_key_value(key,*config);
    }
    unmarshall_struct_GlobalConfig(buf,&buf_index,*config); 
  }
}

struct extension_data ext_global_test_to_arg(struct GlobalConfig * config)
{
  struct extension_data data;
  char* buf = data.buf;
  size_t buf_index = 0;
  marshall_struct_GlobalConfig(buf, &buf_index, config);
  data.bufc = buf_index;
  return data;
}
struct extension_data ext_global_test_to_resp(int result)
{
  struct extension_data data;
  char* buf= data.buf;
  size_t buf_index = 0;
  marshall_prim(buf, &buf_index, result);
  data.bufc = buf_index;
  return data;
}
int ext_global_test_from_resp(struct extension_data data)
{
  int result;
  char* buf = data.buf;
  size_t buf_index = 0;
  unmarshall_prim(buf, &buf_index, result);
  return result;
}

//---Operational Config Marshall / Unmarshall
#define unmarshall_struct_OperationConfig(a, b, c) 	_unmarshall_struct_OperationConfig(a, b, &c)

void _unmarshall_struct_OperationConfig(char* buf, size_t* buf_index_, struct OperationConfig** TEST_data)
{
  size_t size_of_element_= 0;
  size_t buf_index = *buf_index_;
  unmarshall_prim(buf, &buf_index, size_of_element_);
  //*TEST_data = NULL; 
  if(size_of_element_)
  {
    //*TEST_data = calloc(1, sizeof(**TEST_data));
    struct OperationConfig* TEST_data_ref = *TEST_data;
    unmarshall_string(buf, &buf_index, TEST_data_ref->dns_servers);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->condtime);
    unmarshall_string(buf, &buf_index, TEST_data_ref->service_name);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->post301);
    unmarshall_string(buf, &buf_index, TEST_data_ref->proxy_cipher_list);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->ssl_allow_beast);
    unmarshall_string(buf, &buf_index, TEST_data_ref->tls_authtype);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->autoreferer);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->tftp_blksize);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->tcp_fastopen);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->ftp_ssl_control);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->haproxy_protocol);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->disallow_username_in_url);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->use_resume);
    unmarshall_string(buf, &buf_index, TEST_data_ref->proxy_tls_password);
    unmarshall_string(buf, &buf_index, TEST_data_ref->pubkey);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->ftp_ssl_ccc_mode);
    unmarshall_string(buf, &buf_index, TEST_data_ref->proxy_cert);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->tr_encoding);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->ftp_ssl_reqd);
    unmarshall_string(buf, &buf_index, TEST_data_ref->proxy_cacert);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->ftp_filemethod);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->disable_sessionid);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->post302);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->post303);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->proxyntlm);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->disable_epsv);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->ftp_pret);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->globoff);
    unmarshall_string(buf, &buf_index, TEST_data_ref->proxy_key_passwd);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->ftp_ssl_ccc);
    unmarshall_string(buf, &buf_index, TEST_data_ref->key_type);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->proto_redir_present);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->localport);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->nokeepalive);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->tcp_nodelay);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->sendpersecond);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->proxydigest);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->httpreq);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->proxytunnel);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->timeout);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->use_ascii);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->ftp_create_dirs);
    unmarshall_string(buf, &buf_index, TEST_data_ref->proxy_capath);
    unmarshall_string(buf, &buf_index, TEST_data_ref->egd_file);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->expect100timeout);
    unmarshall_string(buf, &buf_index, TEST_data_ref->preproxy);
    unmarshall_string(buf, &buf_index, TEST_data_ref->dns_ipv4_addr);
    unmarshall_string(buf, &buf_index, TEST_data_ref->proxy_tls_authtype);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->ssh_compression);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->cookiesession);
    unmarshall_string(buf, &buf_index, TEST_data_ref->userpwd);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->gssapi_delegation);
    unmarshall_string(buf, &buf_index, TEST_data_ref->etag_save_file);
    unmarshall_string(buf, &buf_index, TEST_data_ref->noproxy);
    unmarshall_string(buf, &buf_index, TEST_data_ref->headerfile);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->resume_from_current);
    {
      struct State* tmp = NULL;
      _unmarshall_struct_State(buf, &buf_index, &tmp);
      memcpy(&TEST_data_ref->state, tmp, sizeof(*tmp));
      free(tmp);
    }
    unmarshall_prim(buf, &buf_index, TEST_data_ref->ftp_skip_ip);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->postfieldsize);
    unmarshall_string(buf, &buf_index, TEST_data_ref->etag_compare_file);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->ssl_no_revoke);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->ignorecl);
    unmarshall_string(buf, &buf_index, TEST_data_ref->hostpubmd5);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->porttouse);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->followlocation);
    unmarshall_string(buf, &buf_index, TEST_data_ref->oauth_bearer);
    _unmarshall_struct_getout(buf, &buf_index, &(TEST_data_ref->url_list));
        //Hack
    TEST_data_ref->url_get = TEST_data_ref->url_list;
    TEST_data_ref->url_last = TEST_data_ref->url_list;

    unmarshall_string(buf, &buf_index, TEST_data_ref->proxy_pinnedpubkey);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->http09_allowed);
    unmarshall_string(buf, &buf_index, TEST_data_ref->cookie);
    unmarshall_string(buf, &buf_index, TEST_data_ref->proxy);
    unmarshall_string(buf, &buf_index, TEST_data_ref->key);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->happy_eyeballs_timeout_ms);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->retry_delay);
    unmarshall_string(buf, &buf_index, TEST_data_ref->cookiefile);
    unmarshall_string(buf, &buf_index, TEST_data_ref->dns_interface);
    unmarshall_string(buf, &buf_index, TEST_data_ref->proxy_cert_type);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->mail_rcpt_allowfails);
    unmarshall_string(buf, &buf_index, TEST_data_ref->proxy_tls_username);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->netrc);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->socks5_gssapi_nec);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->path_as_is);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->failonerror);
    unmarshall_string(buf, &buf_index, TEST_data_ref->cert);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->proxy_insecure_ok);
    unmarshall_string(buf, &buf_index, TEST_data_ref->proxy_key);
    unmarshall_string(buf, &buf_index, TEST_data_ref->doh_url);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->low_speed_limit);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->proxynegotiate);
    unmarshall_string(buf, &buf_index, TEST_data_ref->netrc_file);
    unmarshall_string(buf, &buf_index, TEST_data_ref->unix_socket_path);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->alivetime);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->crlf);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->connecttimeout);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->encoding);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->create_dirs);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->nobuffer);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->proto_redir);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->native_ca_store);
    unmarshall_string(buf, &buf_index, TEST_data_ref->capath);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->ssl_revoke_best_effort);
    unmarshall_string(buf, &buf_index, TEST_data_ref->tls_username);
    unmarshall_string(buf, &buf_index, TEST_data_ref->proxyuserpwd);
    unmarshall_string(buf, &buf_index, TEST_data_ref->cookiejar);
    unmarshall_string(buf, &buf_index, TEST_data_ref->login_options);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->retry_maxtime);
    unmarshall_string(buf, &buf_index, TEST_data_ref->request_target);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->maxredirs);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->prev);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->next);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->httpversion);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->default_node_flags);
    unmarshall_string(buf, &buf_index, TEST_data_ref->proto_default);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->socks5_auth);
    unmarshall_string(buf, &buf_index, TEST_data_ref->altsvc);
    unmarshall_string(buf, &buf_index, TEST_data_ref->engine);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->abstract_unix_socket);
    unmarshall_string(buf, &buf_index, TEST_data_ref->cipher_list);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->timecond);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->sasl_ir);
    unmarshall_string(buf, &buf_index, TEST_data_ref->postfields);
    unmarshall_string(buf, &buf_index, TEST_data_ref->mail_auth);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->insecure_ok);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->suppress_connect_headers);
    unmarshall_string(buf, &buf_index, TEST_data_ref->customrequest);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->proto_present);
    unmarshall_string(buf, &buf_index, TEST_data_ref->referer);
    unmarshall_string(buf, &buf_index, TEST_data_ref->cacert);
    unmarshall_string(buf, &buf_index, TEST_data_ref->proxy_cipher13_list);
    unmarshall_string(buf, &buf_index, TEST_data_ref->mail_from);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->readbusy);
    unmarshall_string(buf, &buf_index, TEST_data_ref->pinnedpubkey);
    unmarshall_string(buf, &buf_index, TEST_data_ref->proxy_key_type);
    unmarshall_string(buf, &buf_index, TEST_data_ref->ftp_alternative_to_user);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->nonpn);
    unmarshall_string(buf, &buf_index, TEST_data_ref->crlfile);
    unmarshall_string(buf, &buf_index, TEST_data_ref->tls_password);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->use_httpget);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->proxy_ssl_allow_beast);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->remote_time);
    unmarshall_string(buf, &buf_index, TEST_data_ref->sasl_authzid);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->retry_all_errors);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->proxy_ssl_version);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->falsestart);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->max_filesize);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->raw);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->disable_eprt);
    unmarshall_string(buf, &buf_index, TEST_data_ref->writeout);
    unmarshall_string(buf, &buf_index, TEST_data_ref->proxy_service_name);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->ftp_ssl);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->low_speed_time);
    unmarshall_string(buf, &buf_index, TEST_data_ref->ftp_account);
    unmarshall_string(buf, &buf_index, TEST_data_ref->iface);
    unmarshall_string(buf, &buf_index, TEST_data_ref->key_passwd);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->proto);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->xattr);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->req_retry);
    unmarshall_string(buf, &buf_index, TEST_data_ref->ftpport);
    unmarshall_string(buf, &buf_index, TEST_data_ref->range);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->resume_from);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->show_headers);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->content_disposition);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->dirlistonly);
    unmarshall_string(buf, &buf_index, TEST_data_ref->random_file);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->authtype);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->ftp_append);
    unmarshall_string(buf, &buf_index, TEST_data_ref->dns_ipv6_addr);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->netrc_opt);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->proxybasic);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->tftp_no_options);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->unrestricted_auth);
    unmarshall_string(buf, &buf_index, TEST_data_ref->cert_type);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->synthetic_error);
    unmarshall_string(buf, &buf_index, TEST_data_ref->cipher13_list);
    unmarshall_string(buf, &buf_index, TEST_data_ref->proxy_crlfile);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->noalpn);
    unmarshall_string(buf, &buf_index, TEST_data_ref->useragent);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->retry_connrefused);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->proxyanyauth);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->localportrange);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->proxyver);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->recvpersecond);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->verifystatus);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->use_metalink);
    unmarshall_string(buf, &buf_index, TEST_data_ref->krblevel);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->ssl_version);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->ssl_version_max);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->no_body);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->terminal_binary_ok);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->ip_version);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->global);
  }
  *buf_index_ = buf_index;
}
void marshall_struct_OperationConfig(char* buf, size_t* buf_index_, struct OperationConfig* TEST_data)
{
  size_t buf_index = *buf_index_;
  
  //Marshall the pointer to this Operation Cfg
  marshall_prim(buf, &buf_index, TEST_data);

  if(TEST_data)
  {
    size_t size_of_element_= sizeof(*TEST_data);
    marshall_prim(buf, &buf_index, size_of_element_);
    marshall_string(buf, &buf_index, TEST_data->dns_servers);
    marshall_prim(buf, &buf_index, TEST_data->condtime);
    marshall_string(buf, &buf_index, TEST_data->service_name);
    marshall_prim(buf, &buf_index, TEST_data->post301);
    marshall_string(buf, &buf_index, TEST_data->proxy_cipher_list);
    marshall_prim(buf, &buf_index, TEST_data->ssl_allow_beast);
    marshall_string(buf, &buf_index, TEST_data->tls_authtype);
    marshall_prim(buf, &buf_index, TEST_data->autoreferer);
    marshall_prim(buf, &buf_index, TEST_data->tftp_blksize);
    marshall_prim(buf, &buf_index, TEST_data->tcp_fastopen);
    marshall_prim(buf, &buf_index, TEST_data->ftp_ssl_control);
    marshall_prim(buf, &buf_index, TEST_data->haproxy_protocol);
    marshall_prim(buf, &buf_index, TEST_data->disallow_username_in_url);
    marshall_prim(buf, &buf_index, TEST_data->use_resume);
    marshall_string(buf, &buf_index, TEST_data->proxy_tls_password);
    marshall_string(buf, &buf_index, TEST_data->pubkey);
    marshall_prim(buf, &buf_index, TEST_data->ftp_ssl_ccc_mode);
    marshall_string(buf, &buf_index, TEST_data->proxy_cert);
    marshall_prim(buf, &buf_index, TEST_data->tr_encoding);
    marshall_prim(buf, &buf_index, TEST_data->ftp_ssl_reqd);
    marshall_string(buf, &buf_index, TEST_data->proxy_cacert);
    marshall_prim(buf, &buf_index, TEST_data->ftp_filemethod);
    marshall_prim(buf, &buf_index, TEST_data->disable_sessionid);
    marshall_prim(buf, &buf_index, TEST_data->post302);
    marshall_prim(buf, &buf_index, TEST_data->post303);
    marshall_prim(buf, &buf_index, TEST_data->proxyntlm);
    marshall_prim(buf, &buf_index, TEST_data->disable_epsv);
    marshall_prim(buf, &buf_index, TEST_data->ftp_pret);
    marshall_prim(buf, &buf_index, TEST_data->globoff);
    marshall_string(buf, &buf_index, TEST_data->proxy_key_passwd);
    marshall_prim(buf, &buf_index, TEST_data->ftp_ssl_ccc);
    marshall_string(buf, &buf_index, TEST_data->key_type);
    marshall_prim(buf, &buf_index, TEST_data->proto_redir_present);
    marshall_prim(buf, &buf_index, TEST_data->localport);
    marshall_prim(buf, &buf_index, TEST_data->nokeepalive);
    marshall_prim(buf, &buf_index, TEST_data->tcp_nodelay);
    marshall_prim(buf, &buf_index, TEST_data->sendpersecond);
    marshall_prim(buf, &buf_index, TEST_data->proxydigest);
    marshall_prim(buf, &buf_index, TEST_data->httpreq);
    marshall_prim(buf, &buf_index, TEST_data->proxytunnel);
    marshall_prim(buf, &buf_index, TEST_data->timeout);
    marshall_prim(buf, &buf_index, TEST_data->use_ascii);
    marshall_prim(buf, &buf_index, TEST_data->ftp_create_dirs);
    marshall_string(buf, &buf_index, TEST_data->proxy_capath);
    marshall_string(buf, &buf_index, TEST_data->egd_file);
    marshall_prim(buf, &buf_index, TEST_data->expect100timeout);
    marshall_string(buf, &buf_index, TEST_data->preproxy);
    marshall_string(buf, &buf_index, TEST_data->dns_ipv4_addr);
    marshall_string(buf, &buf_index, TEST_data->proxy_tls_authtype);
    marshall_prim(buf, &buf_index, TEST_data->ssh_compression);
    marshall_prim(buf, &buf_index, TEST_data->cookiesession);
    marshall_string(buf, &buf_index, TEST_data->userpwd);
    marshall_prim(buf, &buf_index, TEST_data->gssapi_delegation);
    marshall_string(buf, &buf_index, TEST_data->etag_save_file);
    marshall_string(buf, &buf_index, TEST_data->noproxy);
    marshall_string(buf, &buf_index, TEST_data->headerfile);
    marshall_prim(buf, &buf_index, TEST_data->resume_from_current);
    marshall_struct_State(buf, &buf_index, &TEST_data->state);
    marshall_prim(buf, &buf_index, TEST_data->ftp_skip_ip);
    marshall_prim(buf, &buf_index, TEST_data->postfieldsize);
    marshall_string(buf, &buf_index, TEST_data->etag_compare_file);
    marshall_prim(buf, &buf_index, TEST_data->ssl_no_revoke);
    marshall_prim(buf, &buf_index, TEST_data->ignorecl);
    marshall_string(buf, &buf_index, TEST_data->hostpubmd5);
    marshall_prim(buf, &buf_index, TEST_data->porttouse);
    marshall_prim(buf, &buf_index, TEST_data->followlocation);
    marshall_string(buf, &buf_index, TEST_data->oauth_bearer);
    marshall_struct_getout(buf, &buf_index, TEST_data->url_list);
    marshall_string(buf, &buf_index, TEST_data->proxy_pinnedpubkey);
    marshall_prim(buf, &buf_index, TEST_data->http09_allowed);
    marshall_string(buf, &buf_index, TEST_data->cookie);
    marshall_string(buf, &buf_index, TEST_data->proxy);
    marshall_string(buf, &buf_index, TEST_data->key);
    marshall_prim(buf, &buf_index, TEST_data->happy_eyeballs_timeout_ms);
    marshall_prim(buf, &buf_index, TEST_data->retry_delay);
    marshall_string(buf, &buf_index, TEST_data->cookiefile);
    marshall_string(buf, &buf_index, TEST_data->dns_interface);
    marshall_string(buf, &buf_index, TEST_data->proxy_cert_type);
    marshall_prim(buf, &buf_index, TEST_data->mail_rcpt_allowfails);
    marshall_string(buf, &buf_index, TEST_data->proxy_tls_username);
    marshall_prim(buf, &buf_index, TEST_data->netrc);
    marshall_prim(buf, &buf_index, TEST_data->socks5_gssapi_nec);
    marshall_prim(buf, &buf_index, TEST_data->path_as_is);
    marshall_prim(buf, &buf_index, TEST_data->failonerror);
    marshall_string(buf, &buf_index, TEST_data->cert);
    marshall_prim(buf, &buf_index, TEST_data->proxy_insecure_ok);
    marshall_string(buf, &buf_index, TEST_data->proxy_key);
    marshall_string(buf, &buf_index, TEST_data->doh_url);
    marshall_prim(buf, &buf_index, TEST_data->low_speed_limit);
    marshall_prim(buf, &buf_index, TEST_data->proxynegotiate);
    marshall_string(buf, &buf_index, TEST_data->netrc_file);
    marshall_string(buf, &buf_index, TEST_data->unix_socket_path);
    marshall_prim(buf, &buf_index, TEST_data->alivetime);
    marshall_prim(buf, &buf_index, TEST_data->crlf);
    marshall_prim(buf, &buf_index, TEST_data->connecttimeout);
    marshall_prim(buf, &buf_index, TEST_data->encoding);
    marshall_prim(buf, &buf_index, TEST_data->create_dirs);
    marshall_prim(buf, &buf_index, TEST_data->nobuffer);
    marshall_prim(buf, &buf_index, TEST_data->proto_redir);
    marshall_prim(buf, &buf_index, TEST_data->native_ca_store);
    marshall_string(buf, &buf_index, TEST_data->capath);
    marshall_prim(buf, &buf_index, TEST_data->ssl_revoke_best_effort);
    marshall_string(buf, &buf_index, TEST_data->tls_username);
    marshall_string(buf, &buf_index, TEST_data->proxyuserpwd);
    marshall_string(buf, &buf_index, TEST_data->cookiejar);
    marshall_string(buf, &buf_index, TEST_data->login_options);
    marshall_prim(buf, &buf_index, TEST_data->retry_maxtime);
    marshall_string(buf, &buf_index, TEST_data->request_target);
    marshall_prim(buf, &buf_index, TEST_data->maxredirs);
    marshall_prim(buf, &buf_index, TEST_data->next);
    marshall_prim(buf, &buf_index, TEST_data->prev);
    marshall_prim(buf, &buf_index, TEST_data->httpversion);
    marshall_prim(buf, &buf_index, TEST_data->default_node_flags);
    marshall_string(buf, &buf_index, TEST_data->proto_default);
    marshall_prim(buf, &buf_index, TEST_data->socks5_auth);
    marshall_string(buf, &buf_index, TEST_data->altsvc);
    marshall_string(buf, &buf_index, TEST_data->engine);
    marshall_prim(buf, &buf_index, TEST_data->abstract_unix_socket);
    marshall_string(buf, &buf_index, TEST_data->cipher_list);
    marshall_prim(buf, &buf_index, TEST_data->timecond);
    marshall_prim(buf, &buf_index, TEST_data->sasl_ir);
    marshall_string(buf, &buf_index, TEST_data->postfields);
    marshall_string(buf, &buf_index, TEST_data->mail_auth);
    marshall_prim(buf, &buf_index, TEST_data->insecure_ok);
    marshall_prim(buf, &buf_index, TEST_data->suppress_connect_headers);
    marshall_string(buf, &buf_index, TEST_data->customrequest);
    marshall_prim(buf, &buf_index, TEST_data->proto_present);
    marshall_string(buf, &buf_index, TEST_data->referer);
    marshall_string(buf, &buf_index, TEST_data->cacert);
    marshall_string(buf, &buf_index, TEST_data->proxy_cipher13_list);
    marshall_string(buf, &buf_index, TEST_data->mail_from);
    marshall_prim(buf, &buf_index, TEST_data->readbusy);
    marshall_string(buf, &buf_index, TEST_data->pinnedpubkey);
    marshall_string(buf, &buf_index, TEST_data->proxy_key_type);
    marshall_string(buf, &buf_index, TEST_data->ftp_alternative_to_user);
    marshall_prim(buf, &buf_index, TEST_data->nonpn);
    marshall_string(buf, &buf_index, TEST_data->crlfile);
    marshall_string(buf, &buf_index, TEST_data->tls_password);
    marshall_prim(buf, &buf_index, TEST_data->use_httpget);
    marshall_prim(buf, &buf_index, TEST_data->proxy_ssl_allow_beast);
    marshall_prim(buf, &buf_index, TEST_data->remote_time);
    marshall_string(buf, &buf_index, TEST_data->sasl_authzid);
    marshall_prim(buf, &buf_index, TEST_data->retry_all_errors);
    marshall_prim(buf, &buf_index, TEST_data->proxy_ssl_version);
    marshall_prim(buf, &buf_index, TEST_data->falsestart);
    marshall_prim(buf, &buf_index, TEST_data->max_filesize);
    marshall_prim(buf, &buf_index, TEST_data->raw);
    marshall_prim(buf, &buf_index, TEST_data->disable_eprt);
    marshall_string(buf, &buf_index, TEST_data->writeout);
    marshall_string(buf, &buf_index, TEST_data->proxy_service_name);
    marshall_prim(buf, &buf_index, TEST_data->ftp_ssl);
    marshall_prim(buf, &buf_index, TEST_data->low_speed_time);
    marshall_string(buf, &buf_index, TEST_data->ftp_account);
    marshall_string(buf, &buf_index, TEST_data->iface);
    marshall_string(buf, &buf_index, TEST_data->key_passwd);
    marshall_prim(buf, &buf_index, TEST_data->proto);
    marshall_prim(buf, &buf_index, TEST_data->xattr);
    marshall_prim(buf, &buf_index, TEST_data->req_retry);
    marshall_string(buf, &buf_index, TEST_data->ftpport);
    marshall_string(buf, &buf_index, TEST_data->range);
    marshall_prim(buf, &buf_index, TEST_data->resume_from);
    marshall_prim(buf, &buf_index, TEST_data->show_headers);
    marshall_prim(buf, &buf_index, TEST_data->content_disposition);
    marshall_prim(buf, &buf_index, TEST_data->dirlistonly);
    marshall_string(buf, &buf_index, TEST_data->random_file);
    marshall_prim(buf, &buf_index, TEST_data->authtype);
    marshall_prim(buf, &buf_index, TEST_data->ftp_append);
    marshall_string(buf, &buf_index, TEST_data->dns_ipv6_addr);
    marshall_prim(buf, &buf_index, TEST_data->netrc_opt);
    marshall_prim(buf, &buf_index, TEST_data->proxybasic);
    marshall_prim(buf, &buf_index, TEST_data->tftp_no_options);
    marshall_prim(buf, &buf_index, TEST_data->unrestricted_auth);
    marshall_string(buf, &buf_index, TEST_data->cert_type);
    marshall_prim(buf, &buf_index, TEST_data->synthetic_error);
    marshall_string(buf, &buf_index, TEST_data->cipher13_list);
    marshall_string(buf, &buf_index, TEST_data->proxy_crlfile);
    marshall_prim(buf, &buf_index, TEST_data->noalpn);
    marshall_string(buf, &buf_index, TEST_data->useragent);
    marshall_prim(buf, &buf_index, TEST_data->retry_connrefused);
    marshall_prim(buf, &buf_index, TEST_data->proxyanyauth);
    marshall_prim(buf, &buf_index, TEST_data->localportrange);
    marshall_prim(buf, &buf_index, TEST_data->proxyver);
    marshall_prim(buf, &buf_index, TEST_data->recvpersecond);
    marshall_prim(buf, &buf_index, TEST_data->verifystatus);
    marshall_prim(buf, &buf_index, TEST_data->use_metalink);
    marshall_string(buf, &buf_index, TEST_data->krblevel);
    marshall_prim(buf, &buf_index, TEST_data->ssl_version);
    marshall_prim(buf, &buf_index, TEST_data->ssl_version_max);
    marshall_prim(buf, &buf_index, TEST_data->no_body);
    marshall_prim(buf, &buf_index, TEST_data->terminal_binary_ok);
    marshall_prim(buf, &buf_index, TEST_data->ip_version);
    marshall_prim(buf, &buf_index, TEST_data->global);
  }
  else
  {
    size_t size_of_element_= 0;
    marshall_prim(buf, &buf_index, size_of_element_);
  }
  *buf_index_ = buf_index;
}

#define unmarshall_struct_getout(a, b, c) 	_unmarshall_struct_getout(a, b, &c)

void marshall_struct_getout(char* buf, size_t* buf_index_, struct getout* TEST_data)
{
  size_t buf_index = *buf_index_;
  if(TEST_data)
  {
    size_t size_of_element_= sizeof(*TEST_data);
    marshall_prim(buf, &buf_index, size_of_element_);
    marshall_string(buf, &buf_index, TEST_data->url);
    marshall_string(buf, &buf_index, TEST_data->outfile);
    marshall_prim(buf, &buf_index, TEST_data->flags);
    marshall_string(buf, &buf_index, TEST_data->infile);
    struct getout* node = TEST_data->next; 
    size_t linked_list_length = 0;
    struct getout* counter_node = node; 
    while(counter_node != NULL) 
    { 
    	counter_node = counter_node->next; 
    	linked_list_length++; 
    } 
    marshall_prim(buf, &buf_index, linked_list_length); 
    while(node != NULL) 
    { 
    	marshall_size(buf, &buf_index, node, 1); 
    	node = node->next; 
    } 
  }
  else
  {
    size_t size_of_element_= 0;
    marshall_prim(buf, &buf_index, size_of_element_);
  }
  *buf_index_ = buf_index;
}
void _unmarshall_struct_getout(char* buf, size_t* buf_index_, struct getout** TEST_data)
{
  size_t size_of_element_= 0;
  size_t buf_index = *buf_index_;
  unmarshall_prim(buf, &buf_index, size_of_element_);
  *TEST_data = NULL;
  if(size_of_element_)
  {
    *TEST_data = calloc(1, sizeof(**TEST_data));
    struct getout* TEST_data_ref = *TEST_data;
    unmarshall_string(buf, &buf_index, TEST_data_ref->url);
    unmarshall_string(buf, &buf_index, TEST_data_ref->outfile);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->flags);
    unmarshall_string(buf, &buf_index, TEST_data_ref->infile);
    struct getout* node = TEST_data_ref; 
    size_t linked_list_length = 0;
    unmarshall_prim(buf, &buf_index, linked_list_length); 
    for(size_t i = 0; i < linked_list_length; i++) 
    { 
    	struct getout* n_node = calloc(sizeof(struct getout), 1); 
    	unmarshall_size(buf, &buf_index, n_node, 1); 
    	node->next = n_node; 
    	n_node->next = NULL; 
    	node = n_node; 
    } 
  }
  *buf_index_ = buf_index;
}
void marshall_struct_URLGlob(char* buf, size_t* buf_index_, struct URLGlob* TEST_data)
{
  size_t buf_index = *buf_index_;
  if(TEST_data)
  {
    size_t size_of_element_= sizeof(*TEST_data);
    marshall_prim(buf, &buf_index, size_of_element_);
    marshall_prim(buf, &buf_index, TEST_data->pattern);
    marshall_prim(buf, &buf_index, TEST_data->pos);
    marshall_prim(buf, &buf_index, TEST_data->urllen);
    marshall_prim(buf, &buf_index, TEST_data->error);
    marshall_prim(buf, &buf_index, TEST_data->beenhere);
    marshall_string(buf, &buf_index, TEST_data->glob_buffer);
    marshall_prim(buf, &buf_index, TEST_data->size);
  }
  else
  {
    size_t size_of_element_= 0;
    marshall_prim(buf, &buf_index, size_of_element_);
  }
  *buf_index_ = buf_index;
}
void marshall_struct_State(char* buf, size_t* buf_index_, struct State* TEST_data)
{
  size_t buf_index = *buf_index_;
  if(TEST_data)
  {
    size_t size_of_element_= sizeof(*TEST_data);
    marshall_prim(buf, &buf_index, size_of_element_);
    marshall_prim(buf, &buf_index, TEST_data->urlnum);
    marshall_struct_getout(buf, &buf_index, TEST_data->urlnode);
    marshall_string(buf, &buf_index, TEST_data->uploadfile);
    marshall_struct_URLGlob(buf, &buf_index, TEST_data->inglob);
    marshall_prim(buf, &buf_index, TEST_data->up);
    marshall_struct_URLGlob(buf, &buf_index, TEST_data->urls);
    marshall_string(buf, &buf_index, TEST_data->outfiles);
    marshall_prim(buf, &buf_index, TEST_data->li);
    marshall_prim(buf, &buf_index, TEST_data->infilenum);
    marshall_string(buf, &buf_index, TEST_data->httpgetfields);
  }
  else
  {
    size_t size_of_element_= 0;
    marshall_prim(buf, &buf_index, size_of_element_);
  }
  *buf_index_ = buf_index;
}
void _unmarshall_struct_URLGlob(char* buf, size_t* buf_index_, struct URLGlob** TEST_data)
{
  size_t size_of_element_= 0;
  size_t buf_index = *buf_index_;
  unmarshall_prim(buf, &buf_index, size_of_element_);
  *TEST_data = NULL;
  if(size_of_element_)
  {
    *TEST_data = calloc(1, sizeof(**TEST_data));
    struct URLGlob* TEST_data_ref = *TEST_data;
    unmarshall_prim(buf, &buf_index, TEST_data_ref->pattern);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->pos);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->urllen);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->error);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->beenhere);
    unmarshall_string(buf, &buf_index, TEST_data_ref->glob_buffer);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->size);
  }
  *buf_index_ = buf_index;
}

#define unmarshall_struct_URLGlob(a, b, c) 	_unmarshall_struct_URLGlob(a, b, &c)

void _unmarshall_struct_State(char* buf, size_t* buf_index_, struct State** TEST_data)
{
  size_t size_of_element_= 0;
  size_t buf_index = *buf_index_;
  unmarshall_prim(buf, &buf_index, size_of_element_);
  *TEST_data = NULL;
  if(size_of_element_)
  {
    *TEST_data = calloc(1, sizeof(**TEST_data));
    struct State* TEST_data_ref = *TEST_data;
    unmarshall_prim(buf, &buf_index, TEST_data_ref->urlnum);
    unmarshall_struct_getout(buf, &buf_index, TEST_data_ref->urlnode);
    unmarshall_string(buf, &buf_index, TEST_data_ref->uploadfile);
    unmarshall_struct_URLGlob(buf, &buf_index, TEST_data_ref->inglob);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->up);
    unmarshall_struct_URLGlob(buf, &buf_index, TEST_data_ref->urls);
    unmarshall_string(buf, &buf_index, TEST_data_ref->outfiles);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->li);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->infilenum);
    unmarshall_string(buf, &buf_index, TEST_data_ref->httpgetfields);
  }
  *buf_index_ = buf_index;
}

#define unmarshall_struct_State(a, b, c) 	_unmarshall_struct_State(a, b, &c)

int ext_test_opcfg_from_resp(struct extension_data data)
{
  int result;
  char* buf = data.buf;
  size_t buf_index = 0;
  unmarshall_prim(buf, &buf_index, result);
  return result;
}
struct extension_data ext_test_opcfg_to_resp(int result)
{
  struct extension_data data;
  char* buf= data.buf;
  size_t buf_index = 0;
  marshall_prim(buf, &buf_index, result);
  data.bufc = buf_index;
  return data;
}
struct extension_data ext_test_opcfg_to_arg(struct OperationConfig *s)
{
  struct extension_data data;
  char* buf = data.buf;
  size_t buf_index = 0;
  marshall_struct_OperationConfig(buf, &buf_index, s);
  data.bufc = buf_index;
  return data;
}
void ext_test_opcfg_from_arg(struct extension_data data, struct OperationConfig **opConfig)
{
 char* buf = data.buf;
  size_t buf_index = 0;

  //Unmarshall the reference to a reference pointer
  struct OperationConfig * key = NULL;
  unmarshall_prim(buf,&buf_index,key);
  if(*opConfig) //
  { 
    unmarshall_struct_OperationConfig(buf, &buf_index,*opConfig);
    add_map_key_value(key,*opConfig);
  }  
  else //Check Mapping for the value of ref_ptr
  { 
    *opConfig = (struct OperationConfig *)get_map_value(key);
    if(!*opConfig) //Thre ref pointer is not found in the map, create a new pointer and add it to the map for the rcvd reference.
    { 
      *opConfig = calloc(1,sizeof(struct OperationConfig));
      add_map_key_value(key,*opConfig);
    }
    unmarshall_struct_OperationConfig(buf,&buf_index,*opConfig); 
  }
}


/* ----------------------------Operation Compartment Forking-----------------------------------*/
struct extension_data op_fork(struct extension_data data)
{ 
  int pid=fork(); 
  if(!pid) 
  {
    size_t buff_index = 0; 
    int argc = 0;
    unmarshall_prim(data.buf,&buff_index,argc);
    char * argv[(const int)argc]; 
    for(int i =0; i < argc; i++)
      unmarshall_string(data.buf,&buff_index,argv[i]);
    execv(argv[0],argv);
  }
  else
  {
    int status;
    waitpid(pid, &status, 0);
    struct extension_data result = {0,{0}};
    result.bufc = sizeof(status);
    memcpy(&result.buf,&status,sizeof(status));
    return result;
  }
}
struct extension_data pack_op_fork(int argc, char ** argv)
{
  struct extension_data result = {0,{0}};
  //Add Split and NULL arguments for the exec
  argc +=2; 
  marshall_prim(result.buf,&result.bufc,argc);

  char split_string[] = "SPLIT";
  argv[argc-2] = split_string;
  argv[argc-1] = (char *) NULL;
  for(int i =0; i<argc; i++)
  {
    marshall_string(result.buf,&result.bufc,argv[i]);
  }
  return result;
}
CURLcode op_fork_return(struct extension_data data)
{
  CURLcode c_code = 0;
  memcpy(&c_code,&data.buf[GLOBAL_SIZE_NON_PAD],sizeof(CURLcode));
  return c_code;
}

//---------------------------------------------------------Operational Functions-------------------------------------------
//Parse CFG
struct extension_data ext_parse_cfg(struct extension_data data)
{
  //un marshall the Global Config
  struct GlobalConfig * ref_config = NULL;
  ext_global_test_from_arg(data,&ref_config);

  //Synch global config and operation config pointers using the mapping
  sync_parent_refs(ref_config);

  //unmarshall the filename parameter
  
  size_t buff_index = GLOBAL_SIZE_NON_PAD;
  char * filename;
  unmarshall_string(data.buf,&buff_index,filename);

  //Run the Parse Config Operations
  parseconfig(filename,ref_config);

  //repackage global config with param and return
  struct extension_data result = ext_global_test_to_arg(ref_config);
  return result;
}
struct extension_data pack_ext_parse_cfg(struct GlobalConfig * global, char * filename)
{

  struct extension_data result = ext_global_test_to_arg(global);
  marshall_string(result.buf,&result.bufc,filename);
  return result;
}

//Parse Args
struct extension_data ext_parse_args(struct extension_data data)
{ 
  //un marshall the Global Config
  struct GlobalConfig * global = NULL;
  ext_global_test_from_arg(data,&global);

  //Synch global config and operation config pointers using the mapping
  sync_parent_refs(global);
  
  size_t buff_index = GLOBAL_SIZE_NON_PAD;

  int argc = 0;
  unmarshall_prim(data.buf,&buff_index,argc);
  char ** argv = calloc(argc,sizeof(char *));
  for(int i =0; i<argc; i++)
    unmarshall_string(data.buf,&buff_index,argv[i]);
  
  ParameterError pa_return = parse_args(global,argc,argv);

  struct extension_data result = ext_global_test_to_arg(global);
  marshall_prim(result.buf,&result.bufc,pa_return);

  return result;

} 
struct extension_data pack_ext_parse_args(struct GlobalConfig * global, int argc, char ** argv)
{
  //pack global config
  struct extension_data result = ext_global_test_to_arg(global);

  //Pack remaining arguments
  marshall_prim(result.buf,&result.bufc,argc);
  for(int i =0; i < argc; i ++)
    marshall_string(result.buf,&result.bufc,argv[i]);
  return result;
}
ParameterError unpack_ext_parse_args_resp(struct extension_data data, struct GlobalConfig ** global)
{
  ext_global_test_from_arg(data,global);
  size_t buffer_index = GLOBAL_SIZE_NON_PAD;
  
  ParameterError return_pe = 0;
  unmarshall_prim(data.buf,&buffer_index,return_pe);
  return return_pe;
}

//Serial Transfers
struct extension_data ext_serial_transfers(struct extension_data data)
{
  //Required Inits for the compartment
  curl_global_init(CURL_GLOBAL_DEFAULT);
  get_libcurl_info();

  //unpack the global struct and synccurl_share_setopt
  struct GlobalConfig * global = NULL;
  ext_global_test_from_arg(data,&global);

  //Synch global config and operation config pointers using the mapping
  sync_parent_refs(global);

  CURLcode action_result = serial_transfers(global,NULL);
  struct extension_data result = ext_global_test_to_arg(global);
  marshall_prim(result.buf,&result.bufc,action_result);
  return result;
}
struct extension_data pack_serial_transfers(struct GlobalConfig * global)
{
 struct extension_data result = ext_global_test_to_arg(global);
 return result;
}
CURLcode unpack_serial_transfers(struct extension_data data,struct GlobalConfig ** global)
{
  ext_global_test_from_arg(data,global);
  size_t buffer_pos = GLOBAL_SIZE_NON_PAD;
  CURLcode result = 0;
  unmarshall_prim(data.buf,&buffer_pos,result);
  return result;
}

//-----------------------------------------------Operation Config + Global Config Synchroniztion------------------------------------
//Send Child Objects (ONLY CALLED IN MAIN)
void sync_child_structure_caller(struct GlobalConfig * gc, struct extension_id * sync_eid ,int op_code) 
{ 
  //Initialize a JSON Map if one does not exist
  if(!op_cfg_map)
    op_cfg_map = json_object_new_object();

  switch (op_code)
  {
    case FORCE_COMPART_SYNC:;//Push All Childrent to the Compartment
      struct OperationConfig * node = gc->first;
      while(node)
      {
        struct extension_data child_node_ext = {0,{0}};
        marshall_struct_OperationConfig(child_node_ext.buf,&child_node_ext.bufc,node);
        child_node_ext.bufc = op_code; //op_code HACK
        compart_call_fn(sync_eid,child_node_ext);

        //Free this node - We will be creating new children from REQUEST_COMPART_SYNC
        struct OperationConfig * next_node = node->next;
        //free(node);
        node=next_node;
      }
      break;
    case REQUEST_COMPART_SYNC:;//Request All Children from the Comparment

      //Build the child data structures with recursion
      struct extension_data request = {0,{0}};
      marshall_prim(request.buf,&request.bufc,gc->first);
      request.bufc = op_code; //op_code HACK
      recursive_build(request,sync_eid);
      //Sync the parent object references
      sync_parent_refs(gc);
      
      break;
    default:
      break;
  }
}

//Function to Handle various child Structure requests (COMPARTMENT CALL ONLY)
struct extension_data  sync_child_structure_callee(struct extension_data data)
{
  int sync_flag = data.bufc;
  
  struct extension_data result = {0,{0}}; 
  struct OperationConfig * req_ptr = NULL;
  //Case 1: Data is a request for a certain struct.(data will be a pointer with nothing else.)
  //Case 2: Data is a Push of a certain Struct. (data will be the reference pointer and the serialize data structure)
  if(!op_cfg_map)
    op_cfg_map = json_object_new_object();  
  switch(sync_flag)
  {
    case FORCE_COMPART_SYNC:;
      req_ptr = recursive_build(data,NULL);
    break;

    case REQUEST_COMPART_SYNC:;
      size_t buff_index = 0;
      unmarshall_prim(data.buf,&buff_index,req_ptr);
      marshall_struct_OperationConfig(result.buf,&result.bufc,req_ptr);
    break;
    default:
    break;
  }
  return result;
}

struct OperationConfig * recursive_build(struct extension_data key_data, struct extension_id * sync_eid)
{  
  int op_code = key_data.bufc;
  if(sync_eid)
    key_data = compart_call_fn(sync_eid,key_data);

  //Unpack and Create OperationConfig Appropriatley
  struct OperationConfig * op_cfg_ref = NULL;
  ext_test_opcfg_from_arg(key_data,&op_cfg_ref); 

  //Create and/or Reassign the Global Reference
  struct extension_data global_ref = {0,{0}};
  marshall_prim(global_ref.buf,&global_ref.bufc,op_cfg_ref->global); 
  op_cfg_ref->global = NULL;
  ext_global_test_from_arg(global_ref,&op_cfg_ref->global);
  
  //Check for peer ptr entry, recurse as neccessary
  if(op_cfg_ref->next)
  {
    struct extension_data next_resp = {0,{0}};
    marshall_prim(next_resp.buf,&next_resp.bufc,op_cfg_ref->next);
    next_resp.bufc = op_code;//op_code HACK
    op_cfg_ref->next = recursive_build(next_resp,sync_eid);
  }
  
  if(op_cfg_ref->prev)
  {
    struct extension_data prev_resp = {0,{0}};
    marshall_prim(prev_resp.buf,&prev_resp.bufc,op_cfg_ref->prev);
    prev_resp.bufc = op_code;//op_code HACK
    op_cfg_ref->prev = recursive_build(prev_resp,sync_eid);
  }
  
  return op_cfg_ref;
}

void sync_parent_refs(struct GlobalConfig * gc)
{
  gc->first = (struct OperationConfig *)get_map_value(gc->first);
  gc->current = (struct OperationConfig *)get_map_value(gc->current);
  gc->last = (struct OperationConfig *)get_map_value(gc->last);
}

//JSON Config Mapping Helpers
void * get_map_value(void * ref_ptr)
{
  uint64_t value;
  json_object * object;
  char key[32] = {0};
  sprintf(key,"%p",ref_ptr);
  object = json_object_object_get(op_cfg_map,key);
  value = json_object_get_int64(object);
  if(value)
  {
    return (void *)value;
  } else { 
    return NULL;
  }
}
void add_map_key_value(void * key_ptr, void * value_ptr)
{
  json_object * object;
  char key[32] = {0};
  sprintf(key,"%p",key_ptr);
  json_object_object_add(op_cfg_map,key,json_object_new_int64((uint64_t)value_ptr));
} 
