#include "../lib/libcompart/compart_base.h"
#include "../lib/libcompart/combin.h"
#include "tool_getparam.h"
#include "tool_cfgable.h"
#include "tool_operate.h"
#include <json-c/json.h>

#define NO_COMPARTS 7
#define FORCE_COMPART_SYNC 0
#define REQUEST_COMPART_SYNC 1

#define GLOBAL_SIZE_NON_PAD 118
#define OP_CFG_SIZE_NON_PAD
//#define MAX_ARG_SIZE 1024

extern struct compart comparts[NO_COMPARTS];

//tool_help
extern struct extension_id * hlp_ext;
extern struct extension_id * man_ext;
extern struct extension_id * ver_ext;
extern struct extension_id * eng_ext;
extern struct extension_id * op_ext;

//tool_paramhlp
extern struct extension_id * pwd_ext;
extern struct extension_id * useragent_ext;
extern struct extension_id * get_param_ext;

//tool_parsecfg
extern struct extension_id * parsecfg_ext;
extern struct extension_id * parsecfg_sync_op_ext;

//tool_parseargs
extern struct extension_id * parseargs_ext;
extern struct extension_id * parseargs_sync_op_ext;

//serial_transfers
extern struct extension_id * serialtransfers_ext;
extern struct extension_id * serialtransfers_sync_op_ext;

struct extension_data blank;
struct json_object * op_cfg_map; 

struct extension_data pack_integer (int pack_int);
int unpack_integer(struct extension_data data);

/*tool_help */
struct extension_data run_help ( struct extension_data data);
struct extension_data run_big_help ( struct extension_data data);
struct extension_data run_ver_info( struct extension_data data);
struct extension_data run_list_engines ( struct extension_data data);

/*operate*/
struct extension_data operate_ext(struct extension_data data);
struct extension_data pack_op_fork(int argc, char ** argv);
struct extension_data package_op_return(CURLcode proc_return);
struct extension_data op_fork(struct extension_data data);

CURLcode op_fork_return(struct extension_data data);
struct extension_data package_passwd_args (char * kind, size_t i, bool last, char * passwd) ;
struct extension_data run_check_passwd(struct extension_data);
CURLcode unpack_check_passwd_res (struct extension_data data, char ** config_pass);


struct extension_data run_my_user_agent(struct extension_data data); 
char *unpack_my_user_response(struct extension_data data);


//------------------------------------------OPERATION FUNCTIONS---------------------------------------------------

void _unmarshall_struct_OperationConfig(char* buf, size_t* buf_index_, struct OperationConfig** TEST_data);

void marshall_struct_OperationConfig(char* buf, size_t* buf_index_, struct OperationConfig* TEST_data);

void _unmarshall_struct_getout(char* buf, size_t* buf_index_, struct getout** TEST_data);

struct extension_data ext_test_opcfg(struct extension_data data);

void marshall_struct_getout(char* buf, size_t* buf_index_, struct getout* TEST_data);

int ext_test_opcfg_from_resp(struct extension_data data);

struct extension_data ext_test_opcfg_to_resp(int result);

void marshall_struct_URLGlob(char* buf, size_t* buf_index_, struct URLGlob* TEST_data);

void marshall_struct_State(char* buf, size_t* buf_index_, struct State* TEST_data);

struct extension_data ext_test_opcfg_to_arg(struct OperationConfig *s);

void _unmarshall_struct_URLGlob(char* buf, size_t* buf_index_, struct URLGlob** TEST_data);

void ext_test_opcfg_from_arg(struct extension_data data, struct OperationConfig **opConfig);

void _unmarshall_struct_State(char* buf, size_t* buf_index_, struct State** TEST_data);

//------------------------------------------GLOBAL CONFIG FUNCTIONS---------------------------------------------------

void ext_global_test_from_arg(struct extension_data data, struct GlobalConfig ** config);

struct extension_data ext_global_test_to_arg(struct GlobalConfig * config);

void marshall_struct_GlobalConfig(char* buf, size_t* buf_index_, struct GlobalConfig* TEST_data);

struct extension_data ext_global_test(struct extension_data data);

struct extension_data ext_global_test_to_resp(int result);

int ext_global_test_from_resp(struct extension_data data);

void _unmarshall_struct_GlobalConfig(char* buf, size_t* buf_index_, struct GlobalConfig** TEST_data);

#define unmarshall_struct_GlobalConfig(a, b, c) 	_unmarshall_struct_GlobalConfig(a, b, &c)

//------------------------------------------BOILERPLATE FUNCTIONS---------------------------------------------------
#ifndef _LIBCOMPART_SERIALISATION__
#define _LIBCOMPART_SERIALISATION__

void unmarshall_string_(char* buf, size_t* buf_index_, char** str);
void marshall_string(char* buf, size_t* buf_index_, char* str);

#define MARSHALL_CAT_(a, b)   a##b

#define MARSHALL_CAT(a, b)   MARSHALL_CAT_(a, b)

#define unmarshall_string(buf, buf_index, str)   unmarshall_string_(buf, buf_index, &str)

//only use for primitive data types
#define marshall_prim(buf, buf_index, data)   size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data); memcpy(&buf[*(buf_index)], &data, MARSHALL_CAT(marshall, __LINE__)); *(buf_index) += MARSHALL_CAT(marshall, __LINE__);

#define unmarshall_prim(buf, buf_index, data)   size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data);   memcpy(&data, &buf[*(buf_index)], MARSHALL_CAT(marshall, __LINE__));   *(buf_index) += MARSHALL_CAT(marshall, __LINE__);

#define marshall_size(buf, buf_index, data, size)   size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data[0]) * size;   memcpy(&buf[*(buf_index)], data, MARSHALL_CAT(marshall, __LINE__));   *(buf_index) += MARSHALL_CAT(marshall, __LINE__);

#define unmarshall_size(buf, buf_index, data, size)    size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data[0]) * size;   data = calloc(size, sizeof(data[0]));   memcpy(data, &buf[*(buf_index)], MARSHALL_CAT(marshall, __LINE__));   *(buf_index) += MARSHALL_CAT(marshall, __LINE__);

#endif // _LIBCOMPART_SERIALISATION__

struct extension_data ext_get_param(struct extension_data data);

struct extension_data ext_parse_cfg(struct extension_data data);
struct extension_data pack_ext_parse_cfg(struct GlobalConfig * gc, char * filename);

struct extension_data ext_parse_args(struct extension_data);
struct extension_data pack_ext_parse_args(struct GlobalConfig * global, int argc, char ** argv);
ParameterError unpack_ext_parse_args_resp(struct extension_data data, struct GlobalConfig ** global);

struct extension_data ext_serial_transfers(struct extension_data data);
struct extension_data pack_serial_transfers(struct GlobalConfig * global);
CURLcode unpack_serial_transfers(struct extension_data data,struct GlobalConfig ** global);

void sync_child_structure_caller(struct GlobalConfig * gc, struct extension_id * sync_eid ,int op_code);
struct extension_data  sync_child_structure_callee(struct extension_data data);
struct OperationConfig * recursive_build(struct extension_data key_data, struct extension_id * sync_eid);
void sync_parent_refs(struct GlobalConfig * gc);
void * get_map_value(void * ref_ptr);
void add_map_key_value(void * key_ptr, void * value_ptr);