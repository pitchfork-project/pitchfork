#!/bin/bash
set -e

#mkdirs
echo "Cloning: Curl"
git clone https://github.com/curl/curl.git
cd curl
git checkout b8d7857a12467fd2502997fcf215fed46e6070e8
./buildconf
./configure
cd ..

echo "Patching: libCompart"
patch ../libcompart/compart.c < compart.patch
patch ../libcompart/compart_api.h < compart_api.patch
make -C ../libcompart

#src
echo "Patching: curl"
patch -uN -d curl/src/ < src.patch

echo "** Patching Complete **\n. "
echo "Please make sure you have json-c installed prior to a make. (sudo apt-get install libjson-c-dev)\n"
