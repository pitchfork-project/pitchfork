# TO RUN

`sudo rm /etc/cf_ex_some_root_file` (clean up)

`make`

`sudo chown root:root beep`

`sudo chown root:root beep_compartment`

`sudo chmod u+s beep`

`sudo chmod u+s beep_compartment`

To try exploit on original beep:

`./exploit.sh BEEP_PATH ./beep`

To try exploit on compartmentalized beep:

`./exploit.sh BEEP_PATH ./beep_compartment`

A successful exploit will modify the file `/etc/cf_ex_some_root_file` from "123456" to some other strings.


# Description of beep
Beep is a 372 line C program has been exploited in Holey Beep (CVE-2018-0492) [LINK https://holeybeep.ninja/]. This is well known class of vulnerabilities known as "time of check to time of use"(TOCTTOU). More details on the exploit can be found here [https://sigint.sh/#/holeybeep]

The exploit uses a global variable, console_type, in beep that is used to determine if a file is a device file or not(BEEP_TYPE_EVDEV or BEEP_TYPE_CONSOLE). The attacker can exploit a race condition in play_beep() by having the console_type still be set to BEEP_TYPE_EVDEV (device file) from a previous call to play_beep(), but change the file descriptor point to another file by changing the symlink. The symlink is passed as a file parameter to beep.

Using compartmentalization, the beep process is split into two components: a privileged component, which handles requests such as write, read, ioctl, and an unprivileged component, which runs beep and communicates with the privileged component. The library calls to read, write, ioctl are replaced with p_read, p_write, p_ioctl, which marshals the arguments into an RPC call to the privileged component where the privilege component checks the arguments, executes the actual command and returns an appropriate return value.

The compartmentalization prevents the attacker from overwriting a file by adding a check in the privileged component to only allow writing to device files.

# Reproduction
The exploit was reproduced on Debian 8.11 and Ubuntu 16.04.5.
For the latter we had to make a small patch ([beep.c_patch_ubuntu_16.04.5_LTS]()) because of different naming of ttys.
Compartmentalised version of beep was only tested on Debian -- for Ubuntu we need to adapt our [beep.patch]() to work with [beep.c_patch_ubuntu_16.04.5_LTS]().
