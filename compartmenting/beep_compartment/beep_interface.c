#include <fcntl.h>
#include <getopt.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <linux/kd.h>
#include <linux/input.h>

#include "beep_interface.h"
#include "compart_base.h"

struct compart comparts[NO_COMPARTS] =
  {{.name = "beep", .uid = 65534, .gid = 65534, .path = "/dev/"},
   {.name = "privileged", .uid = 0, .gid = 0, .path = "/"}};

struct extension_id *ioctl_eviocgsnd_ext = NULL;
struct extension_id *ioctl_kiocsound_ext = NULL;
struct extension_id *write_ext = NULL;
struct extension_id *open_ext = NULL;

struct extension_data ext_ioctl_eviocgsnd_to_arg(int fd)
{
  struct extension_data result;
  result.bufc = sizeof(fd);
  memcpy(result.buf, &fd, sizeof(fd));
  return result;
}

int ext_ioctl_eviocgsnd_from_arg(struct extension_data data)
{
  int result;
  memcpy(&result, data.buf, sizeof(result));
  return result;
}

struct extension_data ext_ioctl_eviocgsnd_to_resp(int fd)
{
  return ext_ioctl_eviocgsnd_to_arg(fd);
}

int ext_ioctl_eviocgsnd_from_resp(struct extension_data data)
{
  int result = ext_ioctl_eviocgsnd_from_arg(data);
  return result;
}

struct extension_data ext_ioctl_eviocgsnd(struct extension_data data)
{
    int fd = ext_ioctl_eviocgsnd_from_arg(data);
    int result = ioctl(fd, EVIOCGSND(0));
    return ext_ioctl_eviocgsnd_to_resp(result);
}

struct extension_data ext_ioctl_kiocsound_to_arg(int console_fd, int period)
{
  struct extension_data result = {0};
  result.bufc = sizeof(console_fd) + sizeof(period);
  memcpy(result.buf, &console_fd, sizeof(console_fd));
  memcpy(result.buf + sizeof(console_fd), &period, sizeof(period));
  return result;
}

void ext_ioctl_kiocsound_from_arg(struct extension_data data, int *fake_fd, int *ioctl_arg)
{
  memcpy(fake_fd, data.buf, sizeof(*fake_fd));
  memcpy(ioctl_arg, data.buf + sizeof(*fake_fd), sizeof(*ioctl_arg));
}

struct extension_data ext_ioctl_kiocsound(struct extension_data data)
{
    int fd;
    int ioctl_arg;
    ext_ioctl_kiocsound_from_arg(data, &fd, &ioctl_arg);
    int result = ioctl(fd, KIOCSOUND, ioctl_arg);
    return ext_ioctl_eviocgsnd_to_resp(result);
}

struct extension_data ext_write_to_arg(int fd, const void *buf, size_t count)
{
  struct extension_data result = {0};
  result.bufc = sizeof(fd) + sizeof(count) + strlen(buf)+1;
  memcpy(result.buf, &fd, sizeof(fd));
  memcpy(result.buf + sizeof(fd), &count, sizeof(count));
  memcpy(result.buf + sizeof(fd) + sizeof(count), buf, strlen(buf) + 1);
  return result;
}

void ext_write_from_arg(struct extension_data data, int *fd, char **buf, size_t *count)
{
  memcpy(fd, data.buf, sizeof(*fd));
  memcpy(count, data.buf + sizeof(*fd), sizeof(*count));
  int buf_size = strlen(data.buf + sizeof(*fd) +  sizeof(*count)) + 1;
  *buf = malloc(buf_size);
  memcpy(*buf, data.buf + sizeof(*fd) +  sizeof(*count), buf_size);
}

struct extension_data ext_write(struct extension_data data)
{
    int fd;
    char *buf;
    size_t count;
    ext_write_from_arg(data, &fd, &buf, &count);
    int result = write(fd, buf, count);
    free(buf);
    return ext_ioctl_eviocgsnd_to_resp(result);
}

struct extension_data ext_open_to_arg(const void *pathname, int flags)
{
  struct extension_data result = {0};
  result.bufc = sizeof(flags) + strlen(pathname)+1;
  memcpy(result.buf, &flags, sizeof(flags));
  memcpy(result.buf + sizeof(flags), pathname, strlen(pathname) + 1);
  return result;
}

void ext_open_from_arg(struct extension_data data, char **pathname, int *flags)
{
  memcpy(flags, data.buf, sizeof(*flags));
  int pathname_size = strlen(data.buf + sizeof(*flags)) + 1;
  *pathname = malloc(pathname_size);
  memcpy(*pathname, data.buf + sizeof(*flags), pathname_size);
}

struct extension_data ext_open(struct extension_data data)
{
    char *pathname;
    int flags;
    ext_open_from_arg(data, &pathname, &flags);
    int result = open(pathname, flags);
    free(pathname);
    return ext_ioctl_eviocgsnd_to_resp(result);
}
