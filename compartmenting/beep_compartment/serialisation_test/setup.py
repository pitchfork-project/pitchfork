import sys
import os

sys.path.insert(0, '../../libcompart_serializer/')

import function_serializer
import struct_serializer

# Takes as input only the file_name
# generates file_name.c and file_name.h
# appends to the file if already present (useful for multiple functions)
output_file = sys.argv[1]

boilerplate = struct_serializer.c_boiler_plate

header_boilerplate = ''' 
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <linux/kd.h>
#include <linux/input.h>
#include "../../libcompart/compart_api.h"
#include "../../libcompart/compart_base.h"

#define NO_COMPARTS 2
extern struct compart comparts[NO_COMPARTS];

extern struct extension_id *ioctl_eviocgsnd_ext;
extern struct extension_id *ioctl_kiocsound_ext;
extern struct extension_id *write_ext;
extern struct extension_id *open_ext;

'''


interface_addition = '''
	struct compart comparts[NO_COMPARTS] =
	{ { .name = "beep", .uid = 65534, .gid = 65534, .path = "/dev/"},
   	{.name = "privileged", .uid = 0, .gid = 0, .path = "/" } };

   	struct extension_id *ioctl_eviocgsnd_ext = NULL;
struct extension_id *ioctl_kiocsound_ext = NULL;
struct extension_id *write_ext = NULL;
struct extension_id *open_ext = NULL;

   '''


include_header_file = "#include \"" + output_file + ".h\" \n"

header, interface_generated_code = function_serializer.RunSerializer(['function.h'])


# write the interface code to a file
# create a new file OR append to the existing file

if os.path.exists(output_file + '.c'):
    mode = 'a' # append if already exists
else:
    mode = 'w' # make a new file if not

with open(output_file+'.c', mode) as f:
	if mode == 'w':
		f.write(include_header_file)
		f.write(boilerplate)
		f.write(interface_addition)
	f.write(interface_generated_code)

with open(output_file+'.h', mode) as f:
	if mode == 'w':
		f.write(header_boilerplate)
	f.write(header)



