# NOTE
This test produces compilable output and the generated serialisation code seems
sensible, but the combined system (i.e., generated code + `beep_compartment`)
has not been executed or tested yet. That's tested in the automated workflow.

# TODO
Currently `make beep_interface.o` produces the following warning:
```
beep_interface.c: In function ‘ext_open’:
beep_interface.c:230:18: warning: implicit declaration of function ‘open’ [-Wimplicit-function-declaration]
   return_value = open(filename, flags);
                  ^
```

# Running the test

Run this command 4 times, each time enter different function: `python -B setup.py beep_interface`
Just press enter when asked for any inpu -- we will go with the default option for each pointer.

Now open to beep_interface.c and make the following changes to ext_* functions:

First, in the `ext_ioctl_kiocsound` function replace
```
  int return_value;
  return_value = ioctl_kiocsound(fd, request, ioctl_arg);
```
with
```
  int return_value = ioctl(fd, KIOCSOUND, ioctl_arg);
```

Second, in the `ext_ioctl_eviocgsnd` function replace
```
  int return_value;
  return_value = ioctl_eviocgsnd(fd, request);
```
with
```
  int return_value = ioctl(fd, EVIOCGSND(0));
```
An example of the resulting `beep_interface.c` can be found in [beep_interface.c_EXAMPLE](beep_interface.c_EXAMPLE).

Finally, execute the following commands:
```
make beep_compartment.c
make beep_interface.o
```
Then continue by following the steps to [run the explot](../README.md).
