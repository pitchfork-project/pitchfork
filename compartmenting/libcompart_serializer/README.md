## Libcompart Serializer

## What is this?
This is a tool that generates C code to serialize data to and from two different endpoints (threads, processes, machines). Generation is mostly automatic -- no need to manually construct datatypes in template languages such as google protobuf. Feed the tool a function and it will split out the generated marshalling function for you. In cases of marshalling pointers, the tool will ask the user how to perform the generation (since C is not type safe and we do not know the total size of data being pointed by the pointer)

### Setup for Ubuntu
+ `./ubuntu_3.8_install.sh`
  + Setup requirements
    + Uses python 2.7
    + Assumes default clang installation is 3.8
    + Installs a specific version of libclang for 3.8
    + Assumes the location of libclang library is the following:
	+ clang.cindex.Config.set_library_file('/usr/lib/x86_64-linux-gnu/libclang-3.8.so.1')
        + Change lines that contain this if so

### Potential Errors
+ clang.cindex.LibclangError: libclang.so
  + Try creating a link as mentioned at the end of this [discussion](https://github.com/mapbox/cncc/issues/6)
  + After trying this, the error changed. It was resolved by manually downloading clang version 3.8
    + sudo pip install https://pypi.python.org/packages/source/c/clang/clang-3.8.tar.gz
    + Found at the end of this [discussion](https://github.com/JBakamovic/yavide/issues/78)
    + Manually install the `python-clang-6.0` package on Ubuntu also appears to solving this issue, by using `sudo apt-get install python-clang-6.0` and `sudo ln -s libclang-6.0.so.1 libclang-3.8.so.1`. But we recommend using `pip` to install, as it would work better with newer version or different operating systems.
+ Errors for package manager ```apt``` at Ubuntu indicating no package of ```llvm-toolchain``` found, but it doesn't looks like impacting building and running the serializer based on current experiments as long as ```clang-3.8``` is correctly installed. The detailed message is the following:
  ```
  Reading package lists... Done
  Building dependency tree       
  Reading state information... Done
  E: Unable to locate package llvm-toolchain
  ```

## Example

This example is provided in the `example` folder

Let's say we want to marshall this function in `example/add_functions.h`:

`
int add_ten(int val)
{
    return val + 10;
}
`

Run `setup.py add_functions.h marshall_add_functions.h` -- This tells the tool to take in `add_functions.h` and to write the generated code to `marshall_add_functions.h`

Then, you will be prompted with a function to serialize. Type in `add_ten`

`
Select a function to serialize
add_ten
add_one
Enter function:
`

Then the output will show the following if generated correctly:

`
Parsing structs...
function is of type SerialFunction
Generating: int add_ten(int val)
ext_add_ten_from_arg
ext_add_ten_from_resp
ext_add_ten_to_arg
ext_add_ten
ext_add_ten_to_resp
`
Now, you can include `marshall_add_functions.h` and use the above methods to marshall data in the function. Usage of these functions and more complex data structures are shown in `tests`.

## Testing
+ Go to [tests](tests) to see how the testing framework works and examples

## Files

### struct_serializer.py
+ Parses each struct in all .c/.h files passed into command line
+ Asks for input struct to parse
+ Generates marshall... calls based on input struct to parse
  + For any pointer that is passed, the user must specify how the data is copied
    + For example, a char pointer can be copied using strcpy, or act as a generic buffer

### function_serializer.py
+ Has a dependency on struct_serializer
+ Parses each function in all .c/.h files passed to command line
+ Asks for input function to parse
+ Generates necessary C code for marshalling data in the function in the parent->child and child->parent communication
+ Make sure that you add both the .h and .c as arguments or else the program may not pick up on functions declared in just the .c file

## Can handle
+ arrays and struct arrays
+ multilevel pointers
+ structs and data structures
  + data structures includes: singular/circular/doubly linked list, binary tree

# Limitations
+ Cannot handle recursive data structures well.
+ Special pointers such as FILE cannot be handled yet.
+ ~~Complex data structures are experimental (linked list, etc), which are located in `experimental`~~ Data structures like linked list is handled, but more complex strucutres such as mime, maps still require manual code.
+ Has internal memory leak in some cases ~~(marshalling structs and pointers)~~: although memory leaks when dealing with recognized data structures mentioned above are fixed throught commt `bac8cd1e`, having the serializer dealing with much more complex structures to a level where manual work is needed, wil still leads to memory leaks.
+ Doesn't reliably avoid name conflicts -- this is why the renames were doing in commit `8500b238` to change `data` into `TEST_data`.

# Future work
+ Consider handling complex data structures effectively (variants of single linked list, double linked list, maps, vector, etc)
+ Testing on more real world examples
+ Instead of manually asking for how to generate code for structs/pointers, automatically do so. Maybe have a runtime component to analyze?
