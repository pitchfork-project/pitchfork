// RUN: cat input.txt | python -B setup.py binary_tree_interface.h
// RUN: %CC %C_FLAGS -I../header %s -o %t
// RUN: echo "" > %t.log
// RUN: { sudo PITCHFORK_LOG=%t.log %t || true; } | %FileCheck %s

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

#include "binary_tree.h"
#include "compart_api.h"
#include "binary_tree_interface.h"

int main()
{
  	compart_init(NO_COMPARTS, comparts, default_config);

	struct extension_id* struct_ext = compart_register_fn("other compartment", &ext_IsInvalidBinaryTree);

  	compart_start("struct compartment");

	struct Node* head = AllocateNode(5);
	struct Node* head_left = AppendNodeToLeft(head, 4);
	struct Node* head_right = AppendNodeToRight(head, 3);
	AppendNodeToLeft(head_left, 2);
	AppendNodeToRight(head_right, 1);

	struct extension_data arg = ext_IsInvalidBinaryTree_to_arg(head);
	int result = ext_IsInvalidBinaryTree_from_resp(compart_call_fn(struct_ext, arg));
	if(result > 0)
	{
		printf("VULNERABLE, GOT A RESULT OF %d\n", result);
	}
	else
	{
		printf("NOT VULNERABLE\n");
// CHECK: NOT VULNERABLE
	}

	fflush(stdout);
	FreeBinaryTree(head);
	ext_IsInvalidBinaryTree_from_resp(compart_call_fn(struct_ext, arg));
	return 0;
}
