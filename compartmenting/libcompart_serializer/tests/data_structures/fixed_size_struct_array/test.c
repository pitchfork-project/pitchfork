// RUN: cat input.txt | python -B setup.py struct_array_interface.h
// RUN: %CC %C_FLAGS -I../header %s -o %t
// RUN: echo "" > %t.log
// RUN: { sudo PITCHFORK_LOG="%t.log" %t || true; } | %FileCheck %s

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

#include "compart_api.h"
#include "struct_array_interface.h"

int main()
{
    // printf("the size is: %ld\n", sizeof(struct OperationConfig));
    // return 0;

    compart_init(NO_COMPARTS, comparts, default_config);

    struct extension_id *struct_ext = compart_register_fn("other compartment", &ext_test_state);

    compart_start("struct compartment");
    struct State *s = calloc(1, sizeof(struct State));
    s->up = 1500;
    // s->urlnode = calloc(1, sizeof(struct getout));
    // s->urlnode->flags = 1;
    s->inglob = calloc(1, sizeof(struct URLGlob));
    s->inglob->pattern[0].content.Set.size = 100;
    s->urls = calloc(1, sizeof(struct URLGlob));
    s->urls->pattern[1].content.CharRange.step = 3;

    // struct OperationConfig *s = calloc(1, sizeof(struct OperationConfig));
    // s->proxyver = 100;

    // s->state.up = 1500;
    // s->state.urlnode = calloc(1, sizeof(struct getout));
    // s->state.urlnode->flags = 1;

    // s->state.inglob = calloc(1, sizeof(struct URLGlob));
    // s->state.inglob->pattern[0].type = UPTSet;
    // s->state.inglob->pattern[0].content.Set.size = 100;

    // s->state.urls = calloc(1, sizeof(struct URLGlob));
    // s->state.urls->pattern[1].type = UPTCharRange;
    // s->state.urls->pattern[1].content.CharRange.step = 3;

    // s->httpreq = HTTPREQ_SIMPLEPOST;

    struct extension_data arg = ext_test_state_to_arg(s);
    int result = ext_test_state_from_resp(compart_call_fn(struct_ext, arg));
    if (result > 0)
    {
        printf("VULNERABLE, GOT A RESULT OF %d\n", result);
    }
    else
    {
        printf("NOT VULNERABLE\n");
        // CHECK: NOT VULNERABLE
    }

    fflush(stdout);
    return 0;
}
