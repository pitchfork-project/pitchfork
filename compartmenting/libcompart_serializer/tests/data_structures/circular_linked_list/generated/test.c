// RUN: cat input.txt | python -B setup.py circular_linked_list_interface.h
// RUN: %CC %C_FLAGS -I../header %s -o %t
// RUN: echo "" > %t.log
// RUN: { sudo PITCHFORK_LOG=%t.log %t || true; } | %FileCheck %s

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

#include "circular_linked_list.h"
#include "compart_api.h"
#include "circular_linked_list_interface.h"

int main()
{
  	compart_init(NO_COMPARTS, comparts, default_config);

	struct extension_id* struct_ext = compart_register_fn("other compartment", &ext_IsInvalidLinkedList);

  	compart_start("struct compartment");

	struct Node* head = AllocateNode(3);
	AppendNodeToBack(head, 2);
	AppendNodeToBack(head, 1);

	struct extension_data arg = ext_IsInvalidLinkedList_to_arg(head);
	int result = ext_IsInvalidLinkedList_from_resp(compart_call_fn(struct_ext, arg));
	if(result > 0)
	{
		printf("VULNERABLE, GOT A RESULT OF %d\n", result);
	}
	else
	{
		printf("NOT VULNERABLE\n");
// CHECK: NOT VULNERABLE
	}

	fflush(stdout);
	FreeLinkedList(head);
	ext_IsInvalidLinkedList_from_resp(compart_call_fn(struct_ext, arg));
	return 0;
}
