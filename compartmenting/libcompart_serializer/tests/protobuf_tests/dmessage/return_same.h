#pragma once

#include "dmessage.pb-c.h"

const char* some_message = "MESSAGE";

#define SUCCESS 1
#define NO_SUCCESS 0

int return_same(struct _DMessage* dmessage)
{
    if(!strcmp(dmessage->d, some_message))
    {
        return SUCCESS;
    }
    return NO_SUCCESS;
}
