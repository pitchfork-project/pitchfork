// RUN: protoc-c --proto_path=%S --c_out=. amessage.proto 
// RUN: mv %S/amessage.pb-c.c %S/amessage.pb-c.cc

// RUN: printf "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n" | python -B setup.py amessage_interface.h
// RUN: sed -i '/data_ref->data_ref/d' amessage_interface.h
// RUN: %CC %C_FLAGS -I%S %s %S/amessage.pb-c.cc -lprotobuf-c -o %t
// RUN: echo "" > %t.log
// RUN: { sudo PITCHFORK_LOG=%t.log %t || true; } | %FileCheck %s

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

#include "compart_api.h"
#include "amessage_interface.h"

int main()
{
  	
    compart_init(NO_COMPARTS, comparts, default_config);
  	return_same_ext = compart_register_fn("other compartment", &ext_return_same);

  	compart_start("hello compartment");

    AMessage msg = AMESSAGE__INIT;
    msg.a = MESSAGE_A;
    msg.has_b = MESSAGE_HAS_B;
    msg.b = MESSAGE_B;

	struct extension_data arg = ext_return_same_to_arg(&msg);
	int success = ext_return_same_from_resp(compart_call_fn(return_same_ext, arg));

	printf("success: %d\n", success);
// CHECK: success: 1

	fflush(stdout);
	ext_return_same_from_resp(compart_call_fn(return_same_ext, arg));
	return 0;
}

