# pointer to struct tests

The tests are defined in `header/structs.h`

You can run `python setup.py` in source to generate a certain testcase

# TODO

For StructWithPrimitiveStruct:
The current code covers the cases when we have pointers to struct.
And so it involves passing double pointers at many places. It does so by prefixing it with &
But when we have a direct struct under a struct, that will not work.
So we need to change the code so that it add following lines around `unmarshall_struct_PrimitiveStruct` function:

`struct PrimitiveStruct* pp = &(*data)->primitive_struct;` <br />
`unmarshall_struct_PrimitiveStruct(buf, &buf_index, pp);` <br />
`(*data)->primitive_struct = *pp;` <br />

Right now, this fix is needed in the `_unmarshall_struct_StructWithPrimitiveStruct` function.
