// RUN: printf "return_same\n\n\n" | python -B setup.py tpl_interface.h
// RUN: %CC %C_FLAGS -I../add_function %s -o %t
// RUN: echo "" > %t.log
// RUN: { sudo PITCHFORK_LOG=%t.log %t || true; } | %FileCheck %s

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

#include "compart_api.h"
#include "return_same.h"
#include "tpl_interface.h"

int main()
{
  	compart_init(NO_COMPARTS, comparts, default_config);
  	return_same_ext = compart_register_fn("other compartment", &ext_return_same);

  	compart_start("hello compartment");

    struct S s;
    s.i = S_I; s.s = S_S; s.c = S_C; s.t = S_T; s.j = S_J; s.u = S_U;

	struct extension_data arg = ext_return_same_to_arg(&s);
	int okay = ext_return_same_from_resp(compart_call_fn(return_same_ext, arg));

	printf("okay: %d\n", okay);
// CHECK: okay: 1

	fflush(stdout);
	ext_return_same_from_resp(compart_call_fn(return_same_ext, arg));
	return 0;
}

