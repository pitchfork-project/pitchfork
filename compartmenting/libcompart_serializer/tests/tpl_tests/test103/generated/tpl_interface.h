
#include <string.h>
#include <stdlib.h>

#define NO_COMPARTS 2

static struct compart comparts[NO_COMPARTS] =
  {{.name = "hello compartment", .uid = 65534, .gid = 65534, .path = "/dev/"},
   {.name = "other compartment", .uid = 65534, .gid = 65534, .path = "/dev/"}};

struct extension_id *return_same_ext = NULL;

void marshall_struct_S(char* buf, size_t* buf_index_, struct S* TEST_data);

struct extension_data ext_return_same_to_resp(int result);

int ext_return_same_from_resp(struct extension_data data);

void _unmarshall_struct_S(char* buf, size_t* buf_index_, struct S** TEST_data);

struct extension_data ext_return_same_to_arg(struct S* s);

void ext_return_same_from_arg(struct extension_data data, struct S* *s);

struct extension_data ext_return_same(struct extension_data data);

#define unmarshall_struct_S(a, b, c) 	_unmarshall_struct_S(a, b, &c)

#ifndef _LIBCOMPART_SERIALISATION__
#define _LIBCOMPART_SERIALISATION__

void unmarshall_string_(char* buf, size_t* buf_index_, char** str);
void marshall_string(char* buf, size_t* buf_index_, char* str);

#define MARSHALL_CAT_(a, b)   a##b

#define MARSHALL_CAT(a, b)   MARSHALL_CAT_(a, b)

#define unmarshall_string(buf, buf_index, str)   unmarshall_string_(buf, buf_index, &str)

//only use for primitive data types
#define marshall_prim(buf, buf_index, data)   size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data); memcpy(&buf[*(buf_index)], &data, MARSHALL_CAT(marshall, __LINE__)); *(buf_index) += MARSHALL_CAT(marshall, __LINE__);

#define unmarshall_prim(buf, buf_index, data)   size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data);   memcpy(&data, &buf[*(buf_index)], MARSHALL_CAT(marshall, __LINE__));   *(buf_index) += MARSHALL_CAT(marshall, __LINE__);

#define marshall_size(buf, buf_index, data, size)   size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data[0]) * size;   memcpy(&buf[*(buf_index)], data, MARSHALL_CAT(marshall, __LINE__));   *(buf_index) += MARSHALL_CAT(marshall, __LINE__);

#define unmarshall_size(buf, buf_index, data, size)   size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data[0]) * size;   data = calloc(size, sizeof(data[0]));   memcpy(data, &buf[*(buf_index)], MARSHALL_CAT(marshall, __LINE__));   *(buf_index) += MARSHALL_CAT(marshall, __LINE__);

#endif // _LIBCOMPART_SERIALISATION__


//marshall string to [size][str]
void marshall_string(char* buf, size_t* buf_index_, char* str)
{
  size_t buf_index = *buf_index_;

  if(str)
  {
    //NULL terminate
    size_t str_length = strlen(str) + 1;
    memcpy(&buf[buf_index], &str_length, sizeof(str_length));
    buf_index += sizeof(str_length);

    memcpy(&buf[buf_index], str, str_length);
    buf_index += str_length;
  }
  else
  {
    memset(&buf[buf_index], 0, sizeof(size_t));
    buf_index += sizeof(size_t);
  }

  *buf_index_ = buf_index;
}

void unmarshall_string_(char* buf, size_t* buf_index_, char** str)
{
  *str = NULL;
  size_t buf_index = *buf_index_;

  size_t str_length = 0;
  memcpy(&str_length, &buf[buf_index], sizeof(str_length));
  buf_index += sizeof(str_length);

  if(str_length > 0)
  {
    //consider for NULL terminated
    *str = calloc(str_length, sizeof(char));
    memcpy(*str, &buf[buf_index], str_length);
    buf_index += str_length;
  }

  *buf_index_ = buf_index;
}
void marshall_struct_S(char* buf, size_t* buf_index_, struct S* TEST_data)
{
  size_t buf_index = *buf_index_;
  if(TEST_data)
  {
    size_t size_of_element_= sizeof(*TEST_data);
    marshall_prim(buf, &buf_index, size_of_element_);
    marshall_prim(buf, &buf_index, TEST_data->c);
    marshall_prim(buf, &buf_index, TEST_data->i);
    marshall_prim(buf, &buf_index, TEST_data->j);
    marshall_string(buf, &buf_index, TEST_data->s);
    marshall_prim(buf, &buf_index, TEST_data->u);
    marshall_string(buf, &buf_index, TEST_data->t);
  }
  else
  {
    size_t size_of_element_= 0;
    marshall_prim(buf, &buf_index, size_of_element_);
  }
  *buf_index_ = buf_index;
}
void _unmarshall_struct_S(char* buf, size_t* buf_index_, struct S** TEST_data)
{
  size_t size_of_element_= 0;
  size_t buf_index = *buf_index_;
  unmarshall_prim(buf, &buf_index, size_of_element_);
  *TEST_data = NULL;
  if(size_of_element_)
  {
    *TEST_data = calloc(1, sizeof(**TEST_data));
    struct S* TEST_data_ref = *TEST_data;
    unmarshall_prim(buf, &buf_index, TEST_data_ref->c);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->i);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->j);
    unmarshall_string(buf, &buf_index, TEST_data_ref->s);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->u);
    unmarshall_string(buf, &buf_index, TEST_data_ref->t);
  }
  *buf_index_ = buf_index;
}

#define unmarshall_struct_S(a, b, c) 	_unmarshall_struct_S(a, b, &c)

struct extension_data ext_return_same_to_resp(int result)
{
  struct extension_data data;
  char* buf= data.buf;
  size_t buf_index = 0;
  marshall_prim(buf, &buf_index, result);
  data.bufc = buf_index;
  return data;
}
int ext_return_same_from_resp(struct extension_data data)
{
  int result;
  char* buf = data.buf;
  size_t buf_index = 0;
  unmarshall_prim(buf, &buf_index, result);
  return result;
}
struct extension_data ext_return_same_to_arg(struct S* s)
{
  struct extension_data data;
  char* buf = data.buf;
  size_t buf_index = 0;
  marshall_struct_S(buf, &buf_index, s);
  data.bufc = buf_index;
  return data;
}
void ext_return_same_from_arg(struct extension_data data, struct S* *s)
{
  char* buf = data.buf;
  size_t buf_index = 0;
  unmarshall_struct_S(buf, &buf_index, *s);
}
struct extension_data ext_return_same(struct extension_data data)
{
  struct S* s;
  ext_return_same_from_arg(data, &s);
  int return_value;
  return_value = return_same(s);


if(s->i == S_I && !strcmp(s->s, S_S) && s->c == S_C && !strcmp(s->t, S_T) && s->j == S_J && s->u == S_U)
{
    return_value = 1;
}
else
{
    return_value = 0;
}


  struct extension_data result = ext_return_same_to_resp( return_value );
  return result;
}
