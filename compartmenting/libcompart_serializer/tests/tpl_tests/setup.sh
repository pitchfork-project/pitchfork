#!/bin/bash

wget https://github.com/troydhanson/tpl/archive/master.zip
unzip master.zip
rm master.zip

cd tpl-master
./bootstrap
./configure
make
sudo make install
cd ..
rm -rf tpl-master

