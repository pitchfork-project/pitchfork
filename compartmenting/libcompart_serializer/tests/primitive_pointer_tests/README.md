# add function

This example program shows how you can use marshaller to generate the interface for patching a program

In `unpatched/hello_vulnerable.c`

```c
#include <stdio.h>

int add_ten(int val)
{
	return val + 10;
}

int main()
{
	int old_val = 0;
	int new_val = add_ten(old_val);

	printf("OLD VAL: %d, NEW VAL: %d\n", old_val, new_val);
	
	//possible vulnerable code
	if(new_val >= 10)
	{
		printf("NEW VAL IS VULNERABLE\n");
	}
	else
	{
		printf("NEW VAL IS NOT VULNERABLE\n");
	}

	return 0;
}
```

This simple example is simple case where if the value is more than or equal to ten, the program prints out a message about it being vulnerable. (This sort of code occurs in real software as well)

The goal is to patch the code, so that the printf statement doesn't run. A way to patch this is to patch the add_ten() function.

In patched version of the program, add_ten() will be called and the resulting value will be checked if it is greater than ten.

The patched version exists in `patched/` folder.

MAKE SURE TO RUN THE PATCHED VERSION AS SUDO

The output of patched should be like so:
```
<11> starting other compartment
<11> starting hello compartment
started other compartment
OLD VALUE: 0, NEW VALUE: 9
NEW VAL IS NOT VULNERABLE
<9> child is done
```

The steps for patching the program:

There is a script that performs these steps. Follow this setup and read what the script does to have a better understanding

The script exists in `setup_marshall_hello_vulnerable/setup.py`

Run using `python setup.py`

Type in `add_ten` to generate the code for serializing add_ten

`hello_vulnerable_interface.c` is generated

`make`

`sudo ./hello_vulnerable`

Description of what happens manually:

This will generate code to marshall add_ten()
+ `python function_serializer.py add_function/add_function.h`
  + `add_ten`
+ Now take the output, which for me is

```
Unable to parse getc in /usr/include/stdio.h
Unable to parse _IO_putc in /usr/include/libio.h
Unable to parse putchar_unlocked in /usr/include/stdio.h
Unable to parse ferror_unlocked in /usr/include/stdio.h
Unable to parse fflush in /usr/include/stdio.h
Unable to parse fputc in /usr/include/stdio.h
Unable to parse __underflow in /usr/include/libio.h
Unable to parse fwrite in /usr/include/stdio.h
Unable to parse _IO_ftrylockfile in /usr/include/libio.h
Unable to parse fputs in /usr/include/stdio.h
Unable to parse tmpnam in /usr/include/stdio.h
Unable to parse feof_unlocked in /usr/include/stdio.h
Unable to parse getline in /usr/include/stdio.h
Unable to parse sprintf in /usr/include/stdio.h
Unable to parse fclose in /usr/include/stdio.h
Unable to parse _IO_sgetn in /usr/include/libio.h
Unable to parse fileno in /usr/include/stdio.h
Unable to parse perror in /usr/include/stdio.h
Unable to parse renameat in /usr/include/stdio.h
Unable to parse getdelim in /usr/include/stdio.h
Unable to parse remove in /usr/include/stdio.h
Unable to parse _IO_vfprintf in /usr/include/libio.h
Unable to parse freopen in /usr/include/stdio.h
Unable to parse fread_unlocked in /usr/include/stdio.h
Unable to parse fputc_unlocked in /usr/include/stdio.h
Unable to parse tempnam in /usr/include/stdio.h
Unable to parse tmpfile in /usr/include/stdio.h
Unable to parse fgetc in /usr/include/stdio.h
Unable to parse pclose in /usr/include/stdio.h
Unable to parse fileno_unlocked in /usr/include/stdio.h
Unable to parse fgets in /usr/include/stdio.h
Unable to parse ctermid in /usr/include/stdio.h
Unable to parse _IO_feof in /usr/include/libio.h
Unable to parse getchar_unlocked in /usr/include/stdio.h
Unable to parse fgetc_unlocked in /usr/include/stdio.h
Unable to parse fgetpos in /usr/include/stdio.h
Unable to parse fsetpos in /usr/include/stdio.h
Unable to parse ftell in /usr/include/stdio.h
Unable to parse _IO_padn in /usr/include/libio.h
Unable to parse vdprintf in /usr/include/stdio.h
Unable to parse __getdelim in /usr/include/stdio.h
Unable to parse __overflow in /usr/include/libio.h
Unable to parse ferror in /usr/include/stdio.h
Unable to parse open_memstream in /usr/include/stdio.h
Unable to parse tmpnam_r in /usr/include/stdio.h
Unable to parse fseeko in /usr/include/stdio.h
Unable to parse __uflow in /usr/include/libio.h
Unable to parse popen in /usr/include/stdio.h
Unable to parse _IO_ferror in /usr/include/libio.h
Unable to parse _IO_flockfile in /usr/include/libio.h
Unable to parse fflush_unlocked in /usr/include/stdio.h
Unable to parse _IO_seekoff in /usr/include/libio.h
Unable to parse printf in /usr/include/stdio.h
Unable to parse fmemopen in /usr/include/stdio.h
Unable to parse fopen in /usr/include/stdio.h
Unable to parse getchar in /usr/include/stdio.h
Unable to parse putw in /usr/include/stdio.h
Unable to parse puts in /usr/include/stdio.h
Unable to parse fdopen in /usr/include/stdio.h
Unable to parse clearerr_unlocked in /usr/include/stdio.h
Unable to parse scanf in /usr/include/stdio.h
Unable to parse putc in /usr/include/stdio.h
Unable to parse dprintf in /usr/include/stdio.h
Unable to parse setbuffer in /usr/include/stdio.h
Unable to parse _IO_funlockfile in /usr/include/libio.h
Unable to parse putchar in /usr/include/stdio.h
Unable to parse _IO_getc in /usr/include/libio.h
Unable to parse fscanf in /usr/include/stdio.h
Unable to parse ftrylockfile in /usr/include/stdio.h
Unable to parse getc_unlocked in /usr/include/stdio.h
Unable to parse _IO_vfscanf in /usr/include/libio.h
Unable to parse ungetc in /usr/include/stdio.h
Unable to parse rename in /usr/include/stdio.h
Unable to parse _IO_peekc_locked in /usr/include/libio.h
Unable to parse sscanf in /usr/include/stdio.h
Unable to parse fread in /usr/include/stdio.h
Unable to parse ftello in /usr/include/stdio.h
Unable to parse fwrite_unlocked in /usr/include/stdio.h
Unable to parse fprintf in /usr/include/stdio.h
Unable to parse _IO_seekpos in /usr/include/libio.h
Unable to parse feof in /usr/include/stdio.h
Unable to parse clearerr in /usr/include/stdio.h
Unable to parse fseek in /usr/include/stdio.h
Unable to parse rewind in /usr/include/stdio.h
Unable to parse putc_unlocked in /usr/include/stdio.h
Unable to parse getw in /usr/include/stdio.h
Unable to parse flockfile in /usr/include/stdio.h
Unable to parse _IO_free_backup_area in /usr/include/libio.h
Unable to parse setbuf in /usr/include/stdio.h
Unable to parse funlockfile in /usr/include/stdio.h
Unable to parse setlinebuf in /usr/include/stdio.h
Unable to parse setvbuf in /usr/include/stdio.h
[ 'main',
  int main() ,
  <SourceRange start <SourceLocation file 'hello_world.c', line 8, column 1>, end <SourceLocation file 'hello_world.c', line 12, column 2>>]
[ 'add_ten',
  int add_ten(int val) ,
  <SourceRange start <SourceLocation file 'hello_world.c', line 3, column 1>, end <SourceLocation file 'hello_world.c', line 6, column 2>>]
Select a function to serialize
main
add_ten
Enter function: Translation unit: hello_world.c

Parsing structs...
WARNING could not find struct reference in parsing members: 
WARNING could not find struct reference in parsing members: __off64_t
WARNING could not find struct reference in parsing members: __off_t
Generating: int add_ten(int val) 

#define MARSHALL_CAT_(a, b)   a##b 
#define MARSHALL_CAT(a, b)   MARSHALL_CAT_(a, b) 
//marshall string to [size][str]
void marshall_string(char* buf, size_t* buf_index_, char* str)
{
  size_t buf_index = *buf_index_;

  if(str)
  {
    //NULL terminate
    size_t str_length = strlen(str) + 1;
    memcpy(&buf[buf_index], &str_length, sizeof(str_length));
    buf_index += sizeof(str_length);

    memcpy(&buf[buf_index], str, str_length);
    buf_index += str_length;
  }
  else
  {
    memset(&buf[buf_index], 0, sizeof(size_t));
    buf_index += sizeof(size_t);
  }

  *buf_index_ = buf_index;
}

void unmarshall_string_(char* buf, size_t* buf_index_, char** str)
{
  *str = NULL;
  size_t buf_index = *buf_index_;

  size_t str_length = 0;
  memcpy(&str_length, &buf[buf_index], sizeof(str_length));
  buf_index += sizeof(str_length);

  if(str_length > 0)
  {
    //consider for NULL terminated
    *str = calloc(str_length, sizeof(char));
    memcpy(*str, &buf[buf_index], str_length);
    buf_index += str_length;
  }

  *buf_index_ = buf_index;
}

#define unmarshall_string(buf, buf_index, str)   unmarshall_string_(buf, &buf_index, &str)

//only use for primitive data types
#define marshall_prim(buf, buf_index, data)   size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data); memcpy(&buf[*(buf_index)], &data, MARSHALL_CAT(marshall, __LINE__)); *(buf_index) += MARSHALL_CAT(marshall, __LINE__); 
#define unmarshall_prim(buf, buf_index, data)   size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data);   memcpy(&data, &buf[*(buf_index)], MARSHALL_CAT(marshall, __LINE__));   *(buf_index) += MARSHALL_CAT(marshall, __LINE__); 
#define marshall_size(buf, buf_index, data, size)   memcpy(&buf[*(buf_index)], &data, sizeof(data) * size);   *(buf_index) += MARSHALL_CAT(marshall, __LINE__); 
#define unmarshall_size(buf, buf_index, data, size)   memcpy(&data, &buf[*(buf_index)], sizeof(data) * size);   *(buf_index) += MARSHALL_CAT(marshall, __LINE__); 

void ext_add_ten_from_arg(struct extension_data data, int *val);
int ext_add_ten_from_resp(struct extension_data data);
struct extension_data ext_add_ten_to_arg(int val);
struct extension_data ext_add_ten(struct extension_data data);
struct extension_data ext_add_ten_to_resp(int result);
void ext_add_ten_from_arg(struct extension_data data, int *val)
{
  
  char* buf = data.buf;
  size_t buf_index = 0;
  
  unmarshall_prim(buf, &buf_index, *val);
}

int ext_add_ten_from_resp(struct extension_data data)
{
  
  int result;
  
  char* buf = data.buf;
  size_t buf_index = 0;
  
  unmarshall_prim(buf, &buf_index, result);
  return result;
  
}

struct extension_data ext_add_ten_to_arg(int val)
{
  
  struct extension_data data;
  char* buf = data.buf;
  size_t buf_index = 0;
  
  marshall_prim(buf, &buf_index, val);
  
  data.bufc = buf_index;
  
  return data;
  
}

struct extension_data ext_add_ten(struct extension_data data)
{
  int val;
  ext_add_ten_from_arg(data, &val);
  struct extension_data result = ext_add_ten_to_resp(0);
  return result;
}

struct extension_data ext_add_ten_to_resp(int result)
{
  
  struct extension_data data;
  char* buf= data.buf;
  size_t buf_index = 0;
  
  marshall_prim(buf, &buf_index, result);
  
  data.bufc = buf_index;
  return data;
  
}

```

+ Copy everything into `hello_vulnerable_interface.c`
+ Add the boiler plate into `hello_vulnerable_interface.c`
+ Adjust the ext_add_ten(...) function to call add_ten() and fix the result
+ Adjust `hello_vulnerable.c` and fix it up (libcompart code)
+ MAKE SURE TO RUN AS SUDO




