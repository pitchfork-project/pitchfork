// RUN: printf "add_ten\n1\n1\n1\n" | python -B setup.py pointer_3x_to_int_interface.h
// RUN: %CC %C_FLAGS -I../add_function %s -o %t
// RUN: echo "" > %t.log
// RUN: { sudo PITCHFORK_LOG=%t.log %t || true; } | %FileCheck %s

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

#include "add_ten.h"
#include "compart_api.h"
#include "pointer_3x_to_int_interface.h"

int main()
{
  	compart_init(NO_COMPARTS, comparts, default_config);
  	add_ten_ext = compart_register_fn("other compartment", &ext_add_ten);

  	compart_start("hello compartment");

	int old_val = 5;
	int* old_val2 = &old_val;
	int** old_val3 = &old_val2;

	struct extension_data arg = ext_add_ten_to_arg(&old_val3);
	int new_val = ext_add_ten_from_resp(compart_call_fn(add_ten_ext, arg));

	printf("OLD VALUE: %d, NEW VALUE: %d\n", old_val, new_val);

	//possible vulnerable code
	if(new_val >= 10)
	{
		printf("NEW VAL IS VULNERABLE\n");
	}
	else
	{
		printf("NEW VAL IS NOT VULNERABLE\n");
// CHECK: NEW VAL IS NOT VULNERABLE
	}

	fflush(stdout);
	ext_add_ten_from_resp(compart_call_fn(add_ten_ext, arg));
	return 0;
}

