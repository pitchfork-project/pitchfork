// RUN: printf "TestStructWithString\n$\n" | python -B setup.py struct_interface.h
// RUN: %CC %C_FLAGS -I../headers %s -o %t
// RUN: echo "" > %t.log
// RUN: { sudo PITCHFORK_LOG="%t.log" valgrind --leak-check=full %t 2>&1 || true; } | %FileCheck %s
// ADD TURN RN to RUN TO SEE RESULTS TO A FILE
// RN: sudo PITCHFORK_LOG="%t.log" valgrind --leak-check=full %t &> %t.valgrind

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

#include "structs.h"
#include "compart_api.h"
#include "struct_interface.h"

int main()
{
	struct StructWithString* struct_with_string = calloc(sizeof(*struct_with_string), 1);
	struct_with_string->integer = INITIAL_NUMBER;
	struct_with_string->string = malloc(strlen(INITIAL_STRING)+1);
	strcpy(struct_with_string->string, INITIAL_STRING);

	struct extension_data arg = ext_TestStructWithString_to_arg(struct_with_string);
	int result = ext_TestStructWithString_from_resp(ext_TestStructWithString(arg));
	if(result > 0)
	{
		printf("VULNERABLE, GOT A RESULT OF %d\n", result);
	}
	else
	{
		printf("NOT VULNERABLE\n");
// CHECK: NOT VULNERABLE
// CHECK-NOT: definitely lost
// CHECK-NOT: LEAK SUMMARY
	}

	fflush(stdout);
	free(struct_with_string->string);
	free(struct_with_string);
	return 0;
}
