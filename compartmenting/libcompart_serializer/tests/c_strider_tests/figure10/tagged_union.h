#pragma once

struct tagged_union
{
    int tag;
    union
    {
        int x;
        char c;
    } u; /* selected by tag */
};

