#pragma once

#include "tagged_union.h"

#define TAGGED_UNION_TAG 123

#define SUCCESS 1
#define NO_SUCCESS 0

int return_same(struct tagged_union* tu)
{
    if(tu->tag == TAGGED_UNION_TAG)
    {
        return SUCCESS;
    }
	return NO_SUCCESS;
}

