#!/bin/bash

TAR_FILE='redis-2.0.2.tar.gz'
wget https://storage.googleapis.com/google-code-archive-downloads/v2/code.google.com/redis/redis-2.0.2.tar.gz
tar xvf ${TAR_FILE}
rm -rf ${TAR_FILE}
