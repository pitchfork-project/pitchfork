import sys

sys.path.insert(0, '../../../')

import function_serializer

output_file = sys.argv[1]

def InsertLines(lines, start, new_lines):
	new_lines = new_lines.split('\n')
	for i, new_line in enumerate(new_lines):
		lines.insert(start + i, new_lines[i])

interface_header_generated_code, interface_generated_code = function_serializer.RunSerializer(['redis.c'], {'exclude_to_resp' : False, 'exclude_from_resp': False, 'exclude_ext_name': False, 'to_arg_name': None, 'from_arg_name': None, 'include_boilerplate': True})

interface_libcompart_code = '''
#include <string.h>
#include <stdlib.h>

struct extension_data {
    size_t bufc;
    char buf[512];
};

#include "ae.h"
extern void beforeSleep(struct aeEventLoop *eventLoop);

'''
#add the libcompart code
interface = interface_libcompart_code + interface_header_generated_code + interface_generated_code

lines = interface.split('\n')

# remove invalid generated code in ext_beforeSleep
'''
for idx, line in enumerate(lines):
    if 'void return_value;' in line:
        lines[idx] = ''
    elif 'struct extension_data result = ext_beforeSleep_to_resp( return_value );' in line:
        lines[idx] = ''
        if 'return result;' in lines[idx+1]:
            lines[idx + 1] = 'return data;'
    elif 'beforeSleep(eventLoop);' in line:
        lines[idx] = 'beforeSleep(eventLoop);'

    # TODO this to fix in marshaller (always outputs struct_aeEventLoop even if the struct is typedef'd)
    if 'marshall_struct_aeEventLoop(buf, &buf_index, eventLoop);' in line:
        lines[idx] = 'marshall_aeEventLoop(buf, &buf_index, eventLoop);'
    elif 'unmarshall_struct_aeEventLoop(buf, &buf_index, *eventLoop);' in line:
        lines[idx] = 'unmarshall_aeEventLoop(buf, &buf_index, *eventLoop);'

    # TODO fix this issue with having extra ref
    if 'unmarshall_prim(buf, &buf_index, data_ref->apidata_ref);' in line:
        lines[idx] = 'unmarshall_prim(buf, &buf_index, data_ref->apidata);'

    # TODO fix this issue with array buffer declaration
    if 'aeFiredEvent [10240]* tmp = NULL;' in line:
        lines[idx] = 'aeFiredEvent* tmp[10240] = NULL;'
    if 'aeFileEvent [10240]* tmp = NULL;' in line:
        lines[idx] = 'aeFileEvent* tmp[10240] = NULL;'
'''

interface_code = '\n'.join(lines)

#write the interface code

with open(output_file, 'w') as f:
	f.write(interface_code)
y
