# Installation

Setup regular version of redis

`./redis-2.0.2-setup.sh`

## Errors

/home/henry/chopchop/compartmenting/libcompart_serializer/tests/c_strider_tests/redis-2.0.2/redis.c:9272: undefined reference to `log`

### Fix, change the redis-server output to have -lm
```
redis-server: $(OBJ)
    $(CC) -o $(PRGNAME) $(CCOPT) $(DEBUG) $(OBJ) -lm
```

Other script installs c strider to system

This will install new software onto the system. Check `c-strider-setup.sh` for more details 

`sudo ./c-strider-setup.sh`

# Expected output for figure4 and figure10
```
henry@henry-vm:~/chopchop/compartmenting/libcompart_serializer/tests/c_strider_tests$ lit . -v
lit: /home/henry/chopchop/compartmenting/libcompart_serializer/tests/lit.site.cfg:74: note: Repository root is /home/henry/chopchop/compartmenting
lit: /home/henry/chopchop/compartmenting/libcompart_serializer/tests/lit.site.cfg:83: note: Using build type Debug
rm -f compart.o libcompart.so compost.o compart_general.o
cc -Wall -O0 -g -DINCLUDE_PID -I/home/henry/chopchop/compartmenting/libcompart  -DPITCHFORK_DBGSTDOUT -DINCLUDE_PID  -c compost.c
PF_FLAGS=-DPITCHFORK_DBGSTDOUT -DINCLUDE_PID
cc -Wall -O0 -g -DINCLUDE_PID -I/home/henry/chopchop/compartmenting/libcompart  -DPITCHFORK_DBGSTDOUT -DINCLUDE_PID  -c compart.c -o compart_general.o
ld -relocatable compart_general.o compost.o -o compart.o
-- Testing: 2 tests, single process --
PASS: Testing libcompart_serializer :: c_strider_tests/figure10/test.c (1 of 2)
PASS: Testing libcompart_serializer :: c_strider_tests/figure4/test.c (2 of 2)
Testing Time: 0.23s
  Expected Passes    : 2
```
