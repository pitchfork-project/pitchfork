#pragma once
#include "dict.h"

#define SUCCESS 1
#define NO_SUCCESS 0

static unsigned int dictIntHash(const void* key)
{
    int i = (int*)key;
    return (unsigned int)i;
}

static int dictIntKeyCompare(void *privdata, const void *key1, const void *key2)
{
    const int i1 = (const int)key1;
    const int i2 = (const int)key2;
    return i1 == i2;
}

static void dictIntKeyDestructor(void *privdata, void *val)
{
}

/* Sets type and expires */
static dictType intDictType = {
    dictIntHash,            /* hash function */
    NULL,                      /* key dup */
    NULL,                      /* val dup */
    dictIntKeyCompare,      /* key compare */
    dictIntKeyDestructor,      /* key destructor */
    NULL                       /* val destructor */
};

dict* init_dict()
{
    dict* d = dictCreate(&intDictType, NULL);

    int val1 = 1;
    int val2 = 2;
    int val3 = 3;
    int val4 = 4;

    dictAdd(d, (void*)val1, NULL);
    dictAdd(d, (void*)val2, NULL);
    dictAdd(d, (void*)val3, NULL);
    dictAdd(d, (void*)val4, NULL);

    return d;
}

int test_dict(dict* d)
{
    dictIterator *di = dictGetIterator(d);
    dictEntry *de;

    int expected = 1;
    while((de = dictNext(di)) != NULL)
    {
        int key = (int)dictGetEntryKey(de);
        if(key != expected)
        {
            dictReleaseIterator(di);
            return NO_SUCCESS;
        }
        expected++;
    }

    dictReleaseIterator(di);

    return SUCCESS;
}

int return_same(dict* d)
{
    //return test_dict(d);
    return SUCCESS;
}

