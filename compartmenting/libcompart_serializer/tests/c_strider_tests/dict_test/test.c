// RUN: cat input.txt | python -B setup.py dict_interface.h
// RUN: %CC %C_FLAGS -x c -I../header %s dict.c2 zmalloc.c2 -o %t
// RUN: echo "" > %t.log
// RUN: { sudo PITCHFORK_LOG="%t.log" %t || true; } | %FileCheck %s

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

#include "compart_api.h"
#include "return_same.h"
#include "dict_interface.h"

int main()
{
    compart_init(NO_COMPARTS, comparts, default_config);

    return_same_ext = compart_register_fn("other compartment", &ext_return_same);

    compart_start("struct compartment");
    
    dict* d = init_dict();

    fprintf(stderr, "SIZE %d\n", dictSize(d));

    struct extension_data arg = ext_return_same_to_arg(d);
    int success = ext_return_same_from_resp(compart_call_fn(return_same_ext, arg));

    printf("success: %d\n", success);
// CHECK: success: 1

    fflush(stdout);
    ext_return_same_from_resp(compart_call_fn(return_same_ext, arg));

    dictRelease(d);

    return 0;
}
