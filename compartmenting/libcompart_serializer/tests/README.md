# Tests

# Setup
+ First thing is to go to libcompart and compile libcompart!
  + Errors may occur if libcompart is not compiled
  + **The tests also includes large data strucutres, which means changing the size of `EXT_ARG_SIZE` of libcompart necessary!!!** Making the changes to libcompart below is strongly recommended, a buffer size too small will cause the stack smashing problem when running some tests.
    ```
    compart_api.h:14 EXT_ARG_SIZE 4096  // enlarge size from 512->4096
    compart.c:783 my_config = defualt_config  // load default config
    ```
+ Make sure lit is installed (from pip)
  + `pip install lit`
  + typing lit in command line 'lit' should produce "usage: lit [-h] [--version] [-j N] [--config-prefix NAME] [-D NAME=VAL] [-q]"
+ Make sure FileCheck is installed
  + This repository already contain the binary of the FileCheck, so you don't necessarily have to install it, as there's an error going on with `pip` and `filecheck` `(no correct version)`.
  + `pip install filecheck==0.0.13`

# Dependencies
+ libcompart (libcompart directory, this repo)
+ lit (python pip)
+ FileCheck (this directory)

# Running tests
+ try run `lit .` or `lit . -v` (verbose output with why the test failed) to run all the tests automatically.

+ All tests run from a certain directory (relative) to import files. For example, the `setup.py` in each directory contains `sys.path.insert(0, '../../../')` to import files in some location. However, this does not affect running `lit` from any other directory.

+ Under each directory of specific test, there may or may not have a `input.txt` file (depend on the test), which serves a purpose of providing inputs for the serializer automatically, such as indicating which function are we serializing as well as the data structures/custom code input. Please do not change these files unless you know what you are doing.

+ The logic of tests is to run the commented lines in the beginning of each `test.c` file as a script, and there'll be examination by `FileCheck` such as `// CHECK: NOT VULNERABLE`, which will check if the output is the desired one. Automatic compilation and logging is also implemented in the same position. You could develop your own test following the similar rule of `lit` and `FileCheck`.

# Each testing directory provides a more detailed explanation of what is being tested

## hello_vulnerable
+ hello_vulnerable
  + This is the most basic form of marshalling for libcompart

## primitive_pointer_tests
+ Multilevel pointer tests (single, double, triple)
    + pointer_to_int
        + single pointer level
    + pointer_2x_to_int
        + double pointer level
    + pointer_3x_to_int

## struct_tests
+ Tests for structs (nested, typedef, struct to pointers, pointers to struct)
    + PrimitiveStruct
        + struct only contains int
    + StructWithPointerToPrimitveStruct
        + struct pointer to primitive struct
    + StructWithString
        + struct with string inside it
    + TypedefPrimitiveStruct
        + primitive struct with typedef
    + StructWithPrimitiveStruct
        + struct with enclosed primitive struct
    + StructWithString_len
        + struct with string array
    + fixed_size_struct_array
        + testing the fixed size struct array drawn from the `OperationConfig` of `CURL`

## internal_memory_tests

~~Tests from `internal_memory_tests` fail due to checks in memory allocations with generated code.~~
  The tests includes memory leak detection for all the data structure mentioned. And it is all passing so far.
  + primitive structs / with strings
  + structs with pointer / multi-level pointers
  + data structures: singular_linked_list (other structures such as doubly linked list could be run in `/data_structures` via valgrind)


## protobuf_test / tpl_tests / c_strider_tests
Tests from `protobuf_tests`, `tpl_tests` and `c_strider_tests` may fail because the tests depend on a third party library. This can be fixed by following the corresponding `README.md`, which includes a setup section in each directory.

The setup code may fail for multiple reasons: out of disk space, setup script not being robust, etc.

# Debugging
+ In each folder, there should be an `Output` folder containing the script that was ran
+ Run the script commands manually and figure out where it went wrong
  + The `Output` folder should contains the following
    + The generated executable
    + The `*.script* file
      + This script file contains the commands that were ran for the test case


## How does testing work
+ lit is a testing framework used in llvm
+ It parses commands found in the source file and executes them
+ [hello_vulnerable example](hello_vulnerable/unpatched/hello_vulnerable.c)
```c
// RUN: gcc -g -DINCLUDE_PID -I../add_function %s -o %t
// RUN: %t | %FileCheck %s
...
skip
...
	//possible vulnerable code
	if(new_val >= 10)
	{
		printf("NEW VAL IS VULNERABLE\n");
// CHECK: NEW VAL IS VULNERABLE
```
+ In the example above, the commented out line `// RUN: gcc -g -DINCLUDE_PID -I../add_function %s -o %t` tells lit to execute this command when parsing this c for testing
  + `%t` is a macro for temporary files
  + `%s` is this file, so for example, it's the absolute path to hello_vulnerable.c
+ The other line that is important is `%t | %FileCheck %s`
  + This means to execute the compiled temporary file and pipe stdout to filecheck (where filecheck takes in the source file `hello_vulnerable.c`
    + This adds another line that we can use where we can check for the output of the program
    + On the line `// CHECK: NEW VAL IS VULNERABLE`, `FileCheck` parses the output of the program and expects on that line that we output `NEW VAL IS VULNERABLE`
+ [lit documentation](https://llvm.org/docs/CommandGuide/lit.html)
+ [FileCheck documentation](https://llvm.org/docs/CommandGuide/FileCheck.html)


