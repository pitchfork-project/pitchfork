#PROTOTYPE - Henry Zhu 6/6/19

import function_definition
from function_definition import Function
from struct_c_serialization_boilerplate import c_boiler_plate

import struct_helper
from struct_helper import PointerRefType

import clang.cindex
import argparse
import re
import sys

DEBUG = True


def log(s):
    if DEBUG:
        print s

def PANIC(s):
    print(s)
    sys.exit(-1)

#if a struct has a member that contains another struct, generate functions for it
def GenerateMarshallStructReferenceFunction(structs, member, generated_structs,
                                            extra_functions, p_path,
                                            p_sub_path):
    struct_ref = member.GetStructReference(structs)
    if struct_ref:
        print 'Found a member reference to another struct ' + member.GetName(
        ) + ' --> ' + struct_ref.GetName()
        print struct_ref.GetMembers().keys()
        GenerateMarshallStructFunction(structs, struct_ref, generated_structs,
                                       extra_functions, p_path, p_sub_path)


# main component that generates the serialization of the struct
def GenerateMarshallStructFunction(structs, struct, generated_structs,
                                   extra_functions, p_path, p_sub_path):
    struct_name = struct.GetName()
    struct_type = struct.GetType()
    struct_type_no_space = struct.GetTypeNoSpace()
    struct_node = struct.GetNode()
    definition = struct_node.get_definition()
    members = struct.GetMembers()

    #check if struct already exists and its marshalling code is generated
    if struct_name in generated_structs:
        return

    marshall_function = struct_helper.BeginMarshallFunctionTemplate(struct)
    unmarshall_function = ''

    for member_name, member in members.items():
        full_member_name = 'TEST_data->' + member_name

        mtype = member.GetType()
        log("mtype: " + mtype)

        # TODO: This is a temporary fix, assuming that the struct array with fixed size is a primitive
        if re.search(r'\[-?[1-9]\d*\]', mtype):
            marshall_call = 'marshall_prim(buf, &buf_index, %s);' % full_member_name
            marshall_function.AddLine(marshall_call)
            continue

        pointer_level = member.GetPointerLevel()
        log("PointerLevel: " + str(pointer_level) + "\n")
        struct_reference = member.GetStructReference(structs)

        #Check if the reference to another struct is self referencing (ie, linked list)
        struct_base_type = struct.GetBaseType()
        struct_reference_base_type = None
        if struct_reference:
            struct_reference_base_type = struct_reference.GetBaseType()
        if struct_reference and struct_base_type != struct_reference_base_type and (pointer_level == 1 or pointer_level == 0):
            # we only consider pointer when dealing with free path
            if pointer_level > 0:
                member_name_str = str(member_name)
                p_sub_path.append(member_name_str)
            GenerateMarshallStructReferenceFunction(structs, member,
                                                    generated_structs,
                                                    extra_functions, p_path,
                                                    p_sub_path)
            if pointer_level > 0:
                buf = p_sub_path.pop()
            # TODO
            # This is a temporary fix. This only works when struct contains another struct or a pointer to it.
            # Double pointer to struct might not work!
            if pointer_level == 0:
                full_member_name = '&' + full_member_name
            marshall_call = 'marshall_%s(buf, &buf_index, %s);' % (
                struct_reference.GetTypeNoSpace(), full_member_name)
            log("Marshall Call: " + marshall_call + "\n")

        #char* (can represent NULL terminated string or unspecified buffer size)
        elif pointer_level > 0:
            base_type = member.GetBaseType()
            marshall_struct_name = full_member_name
            marshall_struct_type = member.GetType()
            for i in range(pointer_level - 1):
                current_pointer_level = abs(i - pointer_level + 1)
                marshall_index = 'marshall_index%d' % (current_pointer_level)

                pointer_ref = struct_helper.GetMemberSizeInput(
                    struct, member, marshall_struct_type)
                pointer_data = pointer_ref.GetData()
                pointer_ref_type = pointer_ref.GetType()

                #how many times are we iterating
                marshall_value = 0
                if pointer_ref_type == PointerRefType.INTEGER:
                    marshall_value = str(pointer_data)
                elif pointer_ref_type == PointerRefType.STRING:
                    marshall_value = full_member_name
                elif pointer_ref_type == PointerRefType.STRUCT_MEMBER_REFERENCE:
                    marshall_value = full_member_name
                elif pointer_ref_type == PointerRefType.ARGUMENT:
                    pointer_arg_type = pointer_data[0]
                    pointer_arg_name = pointer_data[1]
                    pointer_arg_type_name = pointer_arg_type + ' ' + pointer_arg_name
                    marshall_function.AddArgument(pointer_arg_type_name)
                    marshall_value = pointer_arg_name
                elif pointer_ref_type == PointerRefType.PRIMITIVE:
                    marshall_value = full_member_name
                elif pointer_ref_type == PointerRefType.CUSTOM:
                    custom_code = pointer_data[0]
                    elements = pointer_data[1]
                    marshall_value = custom_code

                #TODO maybe use memcpy instead?
                marshall_function.AddLine(
                    'for (size_t %s = 0; %s < %s; %s++)' %
                    (marshall_index, marshall_index, marshall_value,
                     marshall_index))
                marshall_function.AddLine('{')

                #generate a datatype to support pointer

                #struct A **
                marshall_struct_type = base_type + ' ' + '*' * current_pointer_level

                #a2
                marshall_new_struct_name = member_name + '%d' % (
                    current_pointer_level)

                #struct A ** a2 = a3[marshall_index3]
                marshall_statement = marshall_struct_type + ' ' + marshall_new_struct_name + ' = ' + marshall_struct_name + '[' + marshall_index + '];'

                marshall_function.AddLine(marshall_statement)

                #update struct name
                marshall_struct_name = marshall_new_struct_name

            #last call to fetch most inner loop or if only single pointer
            pointer_ref = struct_helper.GetMemberSizeInput(
                struct, member, member_name, p_path, p_sub_path,
                marshall_struct_type)
            pointer_data = pointer_ref.GetData()
            pointer_ref_type = pointer_ref.GetType()
            if pointer_ref_type == PointerRefType.INTEGER:
                marshall_call = 'marshall_size(buf, &buf_index, %s, %d);' % (
                    marshall_struct_name, pointer_data)
            elif pointer_ref_type == PointerRefType.STRING:
                marshall_call = 'marshall_string(buf, &buf_index, %s);' % marshall_struct_name
            elif pointer_ref_type == PointerRefType.STRUCT_MEMBER_REFERENCE:
                marshall_call = 'marshall_size(buf, &buf_index, %s, TEST_data->%s);' % (
                    marshall_struct_name, pointer_data)
            elif pointer_ref_type == PointerRefType.ARGUMENT:
                pointer_arg_type = pointer_data[0]
                pointer_arg_name = pointer_data[1]
                pointer_arg_type_name = pointer_arg_type + ' ' + pointer_arg_name
                marshall_function.AddArgument(pointer_arg_type_name)
                marshall_call = 'marshall_size(buf, &buf_index, %s, %s);' % (
                    marshall_struct_name, pointer_arg_name)
            elif pointer_ref_type == PointerRefType.PRIMITIVE:
                marshall_call = 'marshall_prim(buf, &buf_index, %s);' % (
                    marshall_struct_name)
            elif pointer_ref_type == PointerRefType.CUSTOM:
                custom_code = pointer_data[0]
                elements = pointer_data[1]
                marshall_call = 'marshall_size(buf, &buf_index, %s, %d);' % (
                    marshall_struct_name, elements)
                marshall_call_start = 'marshall_size(buf, &buf_index, '
                marshall_call_end = ', %d);' % (elements)
                #perform code changes
                custom_code = re.sub(
                    r'%MARSHALL\((.*)\)%',
                    marshall_call_start + r'\1' + marshall_call_end,
                    custom_code)

                lines = []
                
                #extract extra functions
                is_extra_function = False
                start_extra_function = False
                extra_function = None
                
                #extract UN/MARSHALL_ONLY as a function
                start_marshall = False
                start_unmarshall = False

                for line in custom_code.split('\n'):
                    if line == '%BEGIN_FUNCTION%':
                        start_extra_function = True
                    elif line == '%END_FUNCTION%':
                        start_extra_function = False
                        is_extra_function = False
                        extra_functions.append(extra_function)
                        extra_function = None
                    elif start_extra_function:
                        start_extra_function = False
                        is_extra_function = True
                        extra_function = Function(line)
                    elif is_extra_function:
                        extra_function.AddLine(line)
                    
                    elif line == '%BEGIN_MARSHALL_ONLY%':
                        start_marshall = True
                    elif line == '%END_MARSHALL_ONLY%':
                        start_marshall = False
                    elif start_marshall:
                        line = line + ' //MARSHALL_ONLY'
                        lines.append(line)
                    
                    elif line == '%BEGIN_UNMARSHALL_ONLY%':
                        start_unmarshall = True
                    elif line == '%END_UNMARSHALL_ONLY%':
                        start_unmarshall = False
                    elif start_unmarshall:
                        line = line + ' //UNMARSHALL_ONLY'
                        lines.append(line)
                    else:
                        lines.append(line)

                #add each line to function
                for line in lines:
                    #comment code out
                    if '//UNMARSHALL_ONLY' in line:
                        line = r'//' + line
                    marshall_function.AddLine(line)

                marshall_call = ''

            marshall_function.AddLine(marshall_call)

            for i in range(pointer_level - 1):
                marshall_function.AddLine('}')

            marshall_call = ''

        #void* (unspecified members buffer)
        elif mtype == 'void *':
            pass
        #FILE*, int (representing something that cannot be transferred over (file descriptors) unless the compartment is initialized immediately, so fork will copy file descriptors)
        elif mtype == 'FILE *':
            #TODO
            pass

        #TODO multilevel pointers

        #assume everything else is a primitive
        else:
            marshall_call = 'marshall_prim(buf, &buf_index, %s);' % full_member_name

        #add marshall_call to marshall_...(...)
        marshall_function.AddLine(marshall_call)

    p_path.append(p_sub_path[:])
    struct_helper.EndMarshallFunctionTemplate(marshall_function)
    marshall_function.SetStructArgument(struct)
    
    generated_structs[struct_type] = marshall_function

# generates all the necessary code
def GenerateC(structs, struct):
    node = struct.GetNode()
    definition = node.get_definition()

    #pp = pprint.PrettyPrinter(indent=4)
    #pp.pprint(struct.members)

    generated_structs = {}
    extra_functions = []
    p_path = []
    p_sub_path = []
    GenerateMarshallStructFunction(structs, struct, generated_structs,
                                   extra_functions, p_path, p_sub_path)

    log('Generated_Structs:')
    log(generated_structs)

    #create unmarshall functions
    for name, f in generated_structs.items():
        unmarshall_name = 'un' + name
        prototype = f.GetPrototype(
        )  #e.g. void marshall_TypedefPrimitiveStruct(char* buf, size_t* buf_index_, TypedefPrimitiveStruct* data)

        #change prototype
        #find data, get one space before it and insert another pointer
        data_index = prototype.find('TEST_data')
        data_index = data_index - 1
        prototype = prototype[:data_index] + '*' + prototype[
            data_index:]  #e.g. void marshall_TypedefPrimitiveStruct(char* buf, size_t* buf_index_, TypedefPrimitiveStruct** data)

        #change marshall to unmarshall
        start_function_name = prototype.find('marshall')
        end_function_name = prototype.find('(')
        prototype_function_name = prototype[
            start_function_name:end_function_name]
        prototype = re.sub(
            'marshall', '_unmarshall', prototype
        )  # e.g. void _unmarshall_TypedefPrimitiveStruct(char* buf, size_t* buf_index_, TypedefPrimitiveStruct** data)

        #add this to get buffer size
        pre_function = '''
size_t size_of_element_= 0;

size_t buf_index = *buf_index_;
unmarshall_prim(buf, &buf_index, size_of_element_);

*TEST_data = NULL;
if(size_of_element_)
{
	*TEST_data = calloc(1, sizeof(**TEST_data));
'''

        #add a struct reference to prefuncton
        #now data shouldn't be a **
        #instead data is a single pointer to make sure marshall and unmarshall calls are basically the same in arguments, just replace marshall with unmarshall)
        args = f.GetArgs()
        struct_arg = args[
            -1]  # Get last parameter (Assume is the struct to marshall/unmarshall)
        struct_arg = struct_arg.replace('TEST_data', '')
        struct_arg += 'TEST_data_ref = *TEST_data;'
        pre_function += struct_arg

        end_function = '''

*buf_index_ = buf_index;
'''

        #change body
        new_body = []

        brackets_count = 0

        requires_pointer_allocation = False #for pointers, we need to check locations to allocate memory
        requires_pointer_allocation_line_delay_counter = 0
        requires_pointer_allocation_line_delay = 3
        allocation_size_string = ''

        pointer_variable_alias = None
        pointer_variable_alias_set_back_string = None

        start_copying = False
        body = f.GetBody()
        for i, line in enumerate(body):
            #ignore size_of_element_because it is part of original in making data buffer size
            if start_copying and 'size_of_element_' not in line.string:
                def MakeRegexChanges(line):
                    #change any marshall calls to unmarshall
                    new_line = re.sub('marshall', 'unmarshall', line)
                    new_line = re.sub('TEST_data', 'TEST_data_ref', new_line)
                    new_line = re.sub('%TEST_DATA_PTR%', 'TEST_data', new_line)
                    return new_line
                                
                new_line = MakeRegexChanges(line.string)

                #for (size_t unmarshall_index1 = 0; unmarshall_index1 < 1; unmarshall_index1++)
                #if there is a for (size_t unmarshall_index ..., then this means that this a pointer loop
                if new_line.find('for (size_t unmarshall_index') != -1:
                    #grab the index loop, this will be the size of the allocation
                    #for (size_t unmarshall_index1 = 0; unmarshall_index1 < 1; unmarshall_index1++)
                    #the above case, it's "1"
                    found_less_than = new_line.find('<')
                    if found_less_than == -1:
                        PANIC('expected < in \"' + new_line + '\"')
                    found_less_than += 2
                    found_semicolon = new_line.find(';', found_less_than)
                    if found_semicolon == -1:
                        PANIC('expected ; in \"' + new_line + '\"')

                    #append this new allocation string before the for loop
                    allocation_size_string = new_line[found_less_than:found_semicolon]

                    #two lines down we encounter the deferencing of the pointer
                    #robj * mbargv1 = TEST_data_ref->mbargv[unmarshall_index1];
                    #check if we grab the variable by checking for pointer and unmarshall_index
                    pointer_deference_line = MakeRegexChanges(body[i + 2].string)
                    if '*' not in pointer_deference_line and '[unmarshall_index' not in pointer_deference_line:
                        PANIC('pointer deference line was not expected in \"' + pointer_deference_line + '\"')

                    #grab the variable name
                    found_equals = pointer_deference_line.find('=')
                    if found_equals == -1:
                        PANIC('expected = in \"' + pointer_deference_line + '\"')
                    found_equals += 2
                    found_left_bracket = pointer_deference_line.find('[', found_equals)
                    if found_left_bracket == -1:
                        PANIC('expected [ in \"' + pointer_deference_line + '\"')

                    pointer_variable = pointer_deference_line[found_equals:found_left_bracket]
                    allocation_string = pointer_variable + ' = calloc(' + allocation_size_string + ', sizeof(*' + pointer_variable + '));'
                    new_body.append(allocation_string)
                    requires_pointer_allocation = True

                if requires_pointer_allocation:
                    requires_pointer_allocation_line_delay_counter += 1

                #we reach the beginning of the pointer unmarshalling, grab the variable and allocate that memory
                if requires_pointer_allocation and requires_pointer_allocation_line_delay_counter == requires_pointer_allocation_line_delay:
                    #robj * mbargv1 = TEST_data_ref->mbargv[unmarshall_index1];
                    #grab the indexed variable after =
                    found_equals = new_line.find('=')
                    if found_equals == -1:
                        PANIC('expected = in \"' + new_line + '\"')
                    found_equals += 2
                    found_semicolon = new_line.find(';', found_equals)
                    if found_semicolon == -1:
                        PANIC('expected ; in \"' + new_line + '\"')

                    pointer_variable = new_line[found_equals:found_semicolon]
                    allocation_string = pointer_variable + ' = calloc(1, sizeof(*' + pointer_variable + '));'
                    new_body.append(allocation_string)
                    
                    #robj * mbargv1 = TEST_data_ref->mbargv[unmarshall_index1];
                    #convert this to
                    #TEST_data_ref->mbargv[unmarshall_index1] = mbargv1;
                    variable_end = found_equals - 3
                    variable_start = new_line.rfind(' ', 0, variable_end - 1) + 1
                    if variable_start == -1:
                        PANIC('expected space in \"' + new_line + '\"')
                    pointer_variable_alias = new_line[variable_start:variable_end]
                    pointer_variable_alias_set_back_string = pointer_variable + ' = ' + pointer_variable_alias + ';'

                    requires_pointer_allocation = False
                    requires_pointer_allocation_line_delay_counter = 0

                if pointer_variable_alias is not None and pointer_variable_alias in new_line and 'unmarshall_prim' in new_line:
                    #   unmarshall_prim(buf, &buf_index, blockingkeys1);
                    # to this
                    #   unmarshall_prim(buf, &buf_index, *blockingkeys1);
                    new_line = new_line.replace(pointer_variable_alias, '*' + pointer_variable_alias)

                    # and add this line
                    #   unmarshall_prim(buf, &buf_index, blockingkeys1);
                    #   robj * blockingkeys1 = TEST_data_ref->blockingkeys[unmarshall_index1]; <--- NEW
                    new_body.append(new_line)
                    new_line = pointer_variable_alias_set_back_string



                # TODO
                # this checks for & in unmarshall
                # this is because if you do &some_struct, unmarshall_struct... will expand to &&some_struct
                # we need a temporary variable

                # where struct is type struct
                # unmarshall_struct_PrimitiveStruct(buf, &buf_index, &data_ref->struct);
                # but umarshall_struct... requires struct* and doing this will compile into _unmarshall_struct(buf, &buf_index, &&data_ref->struct)
                # so we need a temporary

                '''
                {
                        struct PrimitiveStruct* tmp = NULL;
                        unmarshall_struct_PrimitiveStruct(buf, &buf_index, tmp);
                        memcpy(&data_ref->primitive_struct, tmp, sizeof(*tmp));
                }
                '''

                address_of_unmarshall_type = new_line.rfind('&')
                last_comma = new_line.rfind(',')
                # make sure & is the last argument (meaning we have & for struct)
                if address_of_unmarshall_type > last_comma and last_comma != -1:
                    new_body.append('{')
                    new_line_end = new_line.rfind(')')
                    original_assignment = new_line[
                        address_of_unmarshall_type:
                        new_line_end]  # &data_ref->primitive_struct

                    members = struct.GetMembers()
                    for struct_member_name, struct_member in members.items():
                        if struct_member_name in new_line:
                            struct_member_type = struct_member.GetType(
                            )  # struct PrimitiveStruct*
                            struct_member_spelling = struct_member.GetName(
                            )  # primitive_struct

                            temporary_variable_assignment = struct_member_type + '* tmp = NULL;'  # struct PrimitiveStruct* tmp = NULL;

                            if '[' in struct_member_type and ']' in struct_member_type:
                                left = struct_member_type.find("[")
                                right = struct_member_type.find("]") + 1
                                array_size = struct_member_type[left:right]
                                struct_member_type_no_array = struct_member_type[:
                                                                                 left]
                                temporary_variable_assignment = struct_member_type_no_array + '(*tmp)' + array_size + ' = NULL;'  # struct PrimitiveStruct (*tmp)[array_size] = NULL;

                            new_body.append(temporary_variable_assignment)
                            break

                    # unmarshall_struct_PrimitiveStruct(buf, &buf_index, &data_ref->primitive_struct); into unmarshall_struct_PrimitiveStruct(buf, &buf_index, tmp);
                    new_line = new_line.replace(original_assignment, 'tmp')
                    new_body.append(new_line)

                    #memcpy(&data_ref->primitive_struct, tmp, sizeof(*tmp));
                    memcpy_line = 'memcpy(' + original_assignment + ', tmp, sizeof(*tmp));'
                    new_body.append(memcpy_line)
                    # adding a free line to deal with memory leak
                    clean_up_line = 'free(tmp);'
                    new_body.append(clean_up_line)
                    new_body.append('}')
                else:
                    new_body.append(new_line)
            if line.string == '{':
                #find this
                #if(c)
                #{
                start_copying = True
            elif line.string == '}':
                #this is end of if statement
                break

        unmarshall_f = Function(prototype)
        unmarshall_f.AddCLines(pre_function)

        for line in new_body:
            if r'//MARSHALL_ONLY' in line:
                continue
            if r'//UNMARSHALL_ONLY' in line:
                line = line.replace('//', '', 1)  #uncomment line
            unmarshall_f.AddLine(line)
        unmarshall_f.AddCLines(end_function)

        #generate a, b, c... arguments
        arguments = prototype.count(',') + 1
        dummy_arg = 'a'
        dummy_arguments = [chr(ord(dummy_arg) + i) for i in range(arguments)]
        dummy_arguments_string = ', '.join(dummy_arguments)

        #generate macro for marshall arguments
        dummy_arguments[-1] = '&' + dummy_arguments[-1]
        dummy_arguments_addr_string = ', '.join(dummy_arguments)

        unmarshall_f.extra = '''
#define %s(%s) \
	%s(%s)\n
''' % ('un' + prototype_function_name, dummy_arguments_string,
        '_un' + prototype_function_name, dummy_arguments_addr_string)

        generated_structs[unmarshall_name] = unmarshall_f

    #remove comments
    for name, generated_struct in generated_structs.items():
        generated_struct.RemoveComments()

    #TODO fix
    #this isn't a generated struct, more of helper function
    for extra_function in extra_functions:
        generated_structs[extra_function.GetPrototype()] = extra_function

    # pointer_analyser(structs, struct, p_path, p_sub_path)

    return generated_structs, p_path


def SerializeStructs(files):
    index = clang.cindex.Index.create()
    structs = {}

    #for each file, parse the file and generate a translation unit and find the structs
    for file_ in files:
        tu = index.parse(file_)
        print 'Translation unit:', tu.spelling
        new_structs = struct_helper.GetStructs(tu)
        struct_helper.ParseStructs(new_structs)

        #always prefer the parsed struct that has a bigger number of elements
        #TODO note that this isn't always correct? What if there is another struct that has the different fields and user wants to select it
        #let the user select from an array of options, I guess?
        for struct_name, struct in new_structs.items():
            old_struct = structs.get(struct_name)
            if old_struct:
                old_num_members = len(old_struct.GetMembers().keys())
                new_num_members = len(struct.GetMembers().keys())

                #remove new struct if the old struct is more updated
                if old_num_members > new_num_members:
                    new_structs.pop(struct_name)

        #update the mapping of structs
        structs.update(new_structs)

        #print new_structs.keys()
        #print [[member for member in struct.GetMembers().keys()] for struct in new_structs.values()]

        #print structs.keys()
        #print [[member for member in struct.GetMembers().keys()] for struct in structs.values()]

    return structs


#TODO also pick out which function to transform

#TODO find default lib path for other os/distros
#default for ubuntu
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('files', nargs='+')
    args = parser.parse_args()

    structs = SerializeStructs(args.files)
    struct = struct_helper.GetStructInput(structs)
    generated_structs = GenerateC(structs, struct)

    #add boiler plate
    generated_structs['boiler_plate'] = Function(c_boiler_plate)

    for name, f in generated_structs.items():
        print "\n" + f.Generate()
