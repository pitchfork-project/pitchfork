#ifndef _LIBCOMPART_SERIALISATION__
#define _LIBCOMPART_SERIALISATION__

void unmarshall_string_(char* buf, size_t* buf_index_, char** str);
void marshall_string(char* buf, size_t* buf_index_, char* str);

#define MARSHALL_CAT_(a, b) \
  a##b

#define MARSHALL_CAT(a, b) \
  MARSHALL_CAT_(a, b)

#define unmarshall_string(buf, buf_index, str) \
  unmarshall_string_(buf, buf_index, &str)

//only use for primitive data types
#define marshall_prim(buf, buf_index, data) \
  size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data); \
memcpy(&buf[*(buf_index)], &data, MARSHALL_CAT(marshall, __LINE__)); \
*(buf_index) += MARSHALL_CAT(marshall, __LINE__);

#define unmarshall_prim(buf, buf_index, data) \
  size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data); \
  memcpy(&data, &buf[*(buf_index)], MARSHALL_CAT(marshall, __LINE__)); \
  *(buf_index) += MARSHALL_CAT(marshall, __LINE__);

#define marshall_size(buf, buf_index, data, size) \
  size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data[0]) * size; \
  memcpy(&buf[*(buf_index)], data, MARSHALL_CAT(marshall, __LINE__)); \
  *(buf_index) += MARSHALL_CAT(marshall, __LINE__);

#define unmarshall_size(buf, buf_index, data, size) \
  size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data[0]) * size; \
  data = calloc(size, sizeof(data[0])); \
  memcpy(data, &buf[*(buf_index)], MARSHALL_CAT(marshall, __LINE__)); \
  *(buf_index) += MARSHALL_CAT(marshall, __LINE__);

#endif // _LIBCOMPART_SERIALISATION__
