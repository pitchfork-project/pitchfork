import sys
import clang.cindex
import pprint
import re
import argparse
import os

import struct_helper
import struct_serializer

#PROTOTYPE - Henry Zhu 6/6/19

DEBUG = False


def log(s):
    if DEBUG:
        print s


# basically a function to be serialized, (same as struct_serializer sort of?)
# holds prototype + body as text and then the final text can be generated
class SerialFunction():
    def __init__(self, node, lines=[]):
        self.node = node
        self.lines = lines

    def GetNode(self):
        return self.node

    def SetFileLines(self, lines):
        self.lines = lines

    def GetName(self):
        node = self.GetNode()
        name = node.spelling
        return name

    def GetReturnType(self):
        node = self.GetNode()
        return_type = node.result_type.spelling
        return return_type

    def GetArgsAsString(self):
        declaration = self.GetEntireDeclaration()
        start = declaration.find('(')
        end = declaration.find(')')

        assert (start != -1)
        assert (end != -1)
        assert (start < end)
        return declaration[start + 1:end]

    # a source range is start line column to end line column
    def GetTextInSourceRange(self, source_range):
        assert (isinstance(source_range, clang.cindex.SourceRange))

        start = source_range.start
        end = source_range.end

        start_line = start.line - 1
        end_line = end.line - 1

        start_col = start.column - 1
        end_col = end.column - 1

        lines = self.lines

        #read first line and column
        start = lines[start_line][start_col:]

        #read last line and column
        end = lines[end_line][:end_col]

        #read everything between
        middle = []
        for i in range(start_line + 1, end_line):
            line = lines[i]

            # for .c files, need to stop at end of function declaration
            if line.find('{', 0) != -1:
                end = ''
                break
            middle.append(line)

        middle = ''.join(middle)

        text = start + middle + end

        text = re.sub('\s+', ' ', text)
        text = re.sub('\n', ' ', text)
        text = re.sub(';', '', text)

        return text

    def GetEntireDeclaration(self):
        node = self.GetNode()
        return self.GetTextInSourceRange(node.extent)

    def __repr__(self):
        return self.GetEntireDeclaration()


def IsFunctionNode(node):
    if not node:
        return False

    if node.kind == clang.cindex.CursorKind.FUNCTION_DECL:
        return True

    return False


#grabs all the functions in the children
def GetFunctions(parent_node):
    functions = {}

    for node in parent_node.get_children():
        if IsFunctionNode(node):
            functions[node.spelling] = SerialFunction(node)

    return functions


# basically reads in the files and finds all the functions in the files
def SerializeFunctions(files, config):
    index = clang.cindex.Index.create()
    functions = {}

    #each parsed file has its own lines, will use this to read raw source/column
    cache_file_lines = {}

    #for each file, parse the file and generate a translation unit and find the functions
    tu = None
    for file_ in files:
        tu = index.parse(file_, None)
        new_functions = GetFunctions(tu.cursor)
        for function_name, function in new_functions.items():
            node = function.GetNode()
            extent = node.extent

            #make sure start and end files are the same
            assert (extent.start.file.name == extent.end.file.name)

            filename = extent.start.file.name
            file_lines = cache_file_lines.get(filename)
            if not file_lines:
                #make sure the files read are the ones I parsed
                if filename in files:
                    with open(filename, 'r+') as f:
                        file_lines = f.readlines()
                        cache_file_lines[filename] = file_lines
                else:
                    print 'Unable to parse %s in %s' % (function_name,
                                                        filename)
                    new_functions.pop(function_name)
                    continue

            function.SetFileLines(file_lines)

        functions.update(new_functions)

    return functions


def PrintFunctions(functions):
    for name, _ in functions.items():
        print name


# prompt user to input a function to be selected from a list of functions
def SelectFunction(functions):
    if len(functions) == 0:
        raise ValueError("There are no functions to choose from")
    elif len(functions) == 1:
        selected = functions.keys()[0]
        print("Serialising " + selected)
        return functions[selected]

    print 'Select a function to serialize'
    PrintFunctions(functions)

    function = None
    while not function:
        function_name = raw_input('Enter function: ')
        function = functions.get(function_name)

    return function


# walks up the node tree to the root
def GetBaseNode(node):
    children = list(node.get_children())
    if len(children) > 0:
        return GetBaseNode(children[0])
    return node


def GetSizeInputForPointer(pointer_type, name):
    print 'Enter \'$\' to assume NULL-terminated string'
    print 'Enter a number like \'3\' for fixed size array'
    print 'Enter a member like \'=\' to copy the pointer as a primitive'
    print 'Press enter for default (either string copy if char *, else copy as primitive)'
    print 'Pointer:', pointer_type, name

    result = None
    while not result:
        result = raw_input('Enter: \n')
        integer = struct_helper.ConvertToInt(result)
        if integer:
            if integer > 0:
                return struct_helper.PointerRef(
                    struct_helper.PointerRefType.INTEGER, integer)
            else:
                print 'Integer cannot be negative', integer
                return None
        if pointer_type == 'char *' and (result == '' or result == '$'):
            return struct_helper.PointerRef(
                struct_helper.PointerRefType.STRING, result)
        if result == '' or result == "=":
            return struct_helper.PointerRef(
                struct_helper.PointerRefType.PRIMITIVE, None)
        result = None

    return result


def GeneratePrimitivePointerMarshallCode(arg_type, name, marshall_function,
                                         unmarshall_function):
    pointer_level = arg_type.count('*')
    if pointer_level == 0:
        return

    base_type = arg_type.replace('*', '')

    current_pointer_type = arg_type
    marshall_name = name

    #last call to fetch most inner loop or if only single pointer
    pointer_ref = GetSizeInputForPointer(current_pointer_type, name)
    pointer_data = pointer_ref.GetData()
    pointer_ref_type = pointer_ref.GetType()
    if pointer_ref_type == struct_helper.PointerRefType.INTEGER:
        marshall_call = 'marshall_size(buf, &buf_index, %s, %d);' % (
            marshall_name, pointer_data)
        unmarshall_call = 'unmarshall_size(buf, &buf_index, *%s, %d);' % (
            marshall_name, pointer_data)
    elif pointer_ref_type == struct_helper.PointerRefType.STRING:
        marshall_call = 'marshall_string(buf, &buf_index, %s);' % marshall_name
        unmarshall_call = 'unmarshall_string(buf, &buf_index, *%s);' % marshall_name
    elif pointer_ref_type == struct_helper.PointerRefType.PRIMITIVE:
        marshall_call = 'marshall_prim(buf, &buf_index, %s);' % (marshall_name)
        unmarshall_call = 'unmarshall_prim(buf, &buf_index, *%s);' % (
            marshall_name)

    marshall_function.AddLine(marshall_call)
    unmarshall_function.AddLine(unmarshall_call)

    for i in range(pointer_level - 1):
        current_pointer_level = abs(i - pointer_level + 1)

        marshall_index = 'marshall_index%d' % (current_pointer_level)

        current_pointer_type = base_type + '*' * current_pointer_level
        pointer_ref = GetSizeInputForPointer(current_pointer_type, name)
        pointer_data = pointer_ref.GetData()
        pointer_ref_type = pointer_ref.GetType()

        #how many times are we iterating
        marshall_value = 0
        if pointer_ref_type == struct_helper.PointerRefType.INTEGER:
            marshall_value = str(pointer_data)
        elif pointer_ref_type == struct_helper.PointerRefType.STRING:
            marshall_value = full_member_name
        elif pointer_ref_type == struct_helper.PointerRefType.PRIMITIVE:
            marshall_value = full_member_name

        #TODO maybe use memcpy instead?
        marshall_function.AddLine(
            'for (size_t %s = 0; %s < %s; %s++)' %
            (marshall_index, marshall_index, marshall_value, marshall_index))
        marshall_function.AddLine('{')

        unmarshall_function.AddLine(
            'for (int %s; %s < %s; %s++)' %
            (marshall_index, marshall_index, marshall_value, marshall_index))
        unmarshall_function.AddLine('{')

        #generate a datatype to support pointer

        #struct A **
        marshall_type = base_type + ' ' + '*' * current_pointer_level

        #a2
        marshall_new_name = name + '%d' % (current_pointer_level)

        #struct A ** a2 = a3[marshall_index3]
        marshall_statement = marshall_type + ' ' + marshall_new_name + ' = ' + marshall_name + '[' + marshall_index + '];'
        unmarshall_statement = marshall_type + '* ' + marshall_new_name + ' = ' + marshall_name + '[' + marshall_index + '];'

        marshall_function.AddLine(marshall_statement)
        unmarshall_function.AddLine(unmarshall_statement)

        #update struct name
        marshall_name = marshall_new_name

        if pointer_ref_type == struct_helper.PointerRefType.INTEGER:
            marshall_call = 'marshall_size(buf, &buf_index, %s, %d);' % (
                marshall_name, pointer_data)
            unmarshall_call = 'unmarshall_size(buf, &buf_index, *%s, %d);' % (
                marshall_name, pointer_data)
        elif pointer_ref_type == struct_helper.PointerRefType.STRING:
            marshall_call = 'marshall_string(buf, &buf_index, %s);' % marshall_name
            unmarshall_call = 'unmarshall_string(buf, &buf_index, *%s);' % marshall_name
        elif pointer_ref_type == struct_helper.PointerRefType.PRIMITIVE:
            marshall_call = 'marshall_prim(buf, &buf_index, %s);' % (
                marshall_name)
            unmarshall_call = 'unmarshall_prim(buf, &buf_index, *%s);' % (
                marshall_name)

        marshall_function.AddLine(marshall_call)
        unmarshall_function.AddLine(unmarshall_call)

    for i in range(pointer_level - 1):
        marshall_function.AddLine('}')
        unmarshall_function.AddLine('}')


# main function that does the generation of the marshalling of the data
# ext_*_to_arg(...) is converting data from the client to a extension. For example, if a function takes in 3 parameters, char*, int, int, ext_*_to_arg generates the function to convert these paramters to an extention
# ext_*_from_arg(...) is converting data (extension) sent from the client back into the correct parameters in the server. For example, get a extension, and would like to parse this extension back to char*, int, int
# ext_*_to_resp(...) is converting parameters processed by the server into a extension to be sent to the client (most of the time it's just a return value like an int, but the client could pass a pointer to be filled with data (TODO))
# ext_*_from_resp(...) converts the extension sent from the server back into the correct return values (like above, need to handle multiple returns)
# Also, generates the functions for marshalling/demarshalling of structs if required


# generate the code for clean up function, this is used for linked list (singular/circular are basically the same when freeing)
def generate_ll_cleaner(ident, sub_path, member_type, cleanup_func):
    if ident.split(' ')[1] != '2':
        next_pointer = str(sub_path[-1])
        head_ref = str(member_type) + 'head_ref'
        init_line = head_ref + ' = ' 'head' + ';'
        body_line = 'while (head_ref != NULL) {'
        body_line = body_line + '\n    head = head->%s;\n    free(head_ref);\n    head_ref = NULL;\n    head_ref = head;' % (
            next_pointer) + '\n  }'
    # circular linked list should be handled carefully tot avoid invalid free
    elif ident.split(' ')[1] == '2':
        next_pointer = str(sub_path[-1])
        head_ref = str(member_type) + 'head_ref'
        tmp = str(member_type) + 'tmp'
        init_line = head_ref + ' = ' 'head->next' + ';'
        init_line = init_line + '\n  ' + tmp + ' = head_ref;'
        body_line = 'while (head_ref != head) {'
        body_line = body_line + '\n    head_ref = head_ref->%s;\n    free(tmp);\n    tmp = NULL;\n    tmp = head_ref;' % (
            next_pointer) + '\n  }'
        body_line = body_line + '\n  free(head);'

    cleanup_func.AddLine(init_line)
    cleanup_func.AddLine(body_line)


def generate_bt_cleaner(sub_path, member_type, member_1, member_2,
                        cleanup_func):
    pointer_1 = str(member_1)
    pointer_2 = str(member_2)

    body_line = 'if (leaf->%s != NULL) {' % (pointer_1)
    body_line = body_line + '\n    bt_cleaner(leaf->%s)' % (
        pointer_1) + ';' + '\n  }'
    body_line = body_line + '\n  if (leaf->%s != NULL) {' % (pointer_2)
    body_line = body_line + '\n    bt_cleaner(leaf->%s)' % (
        pointer_2) + ';' + '\n  }'

    cleanup_func.AddLine(body_line)


# note that each function has a begin/end. These are hardcoded because they will stay the same for every function I wish to serialize
def GenerateCFunction(config, function, structs):
    print 'function is of type ' + function.__class__.__name__
    generate_functions = {}
    node = function.GetNode()

    print 'Generating: ' + function.GetEntireDeclaration()

    to_arg_name = 'ext_' + function.GetName() + '_to_arg'
    if config['to_arg_name'] != None:
        to_arg_name = config['to_arg_name']

    to_arg_prototype = 'struct extension_data ' + to_arg_name + '(' + function.GetArgsAsString(
    ) + ')'
    to_arg_function = struct_serializer.Function(to_arg_prototype)
    to_arg_begin = '''
struct extension_data data;
char* buf = data.buf;
size_t buf_index = 0;
'''
    to_arg_end = '''
data.bufc = buf_index;

return data;
'''
    to_arg_function.AddCLines(to_arg_begin)

    #add * to each argument
    arg_arguments = []
    args_split_string = function.GetArgsAsString().split(', ')
    for arg, arg_node in zip(args_split_string, node.get_arguments()):
        arg_name = arg_node.spelling
        arg_name_index = arg.rfind(arg_name)
        arg_name_with_pointer = arg[:arg_name_index] + '*' + arg[
            arg_name_index:]
        arg_arguments.append(arg_name_with_pointer)

    from_arg_arguments = ', '.join(arg_arguments)

    #add struct extenstion_data data to function
    from_arg_arguments = 'struct extension_data data, ' + from_arg_arguments

    from_arg_name = 'ext_' + function.GetName() + '_from_arg'
    if config['from_arg_name'] != None:
        from_arg_name = config['from_arg_name']

    from_arg_prototype = 'void ' + from_arg_name + '(' + from_arg_arguments + ')'
    from_arg_function = struct_serializer.Function(from_arg_prototype)
    from_arg_begin = '''
char* buf = data.buf;
size_t buf_index = 0;
'''
    from_arg_function.AddCLines(from_arg_begin)

    #TODO multiple return results
    to_resp_name = 'ext_%s_to_resp' % (function.GetName())
    to_resp_prototype = 'struct extension_data %s(int result)' % (to_resp_name)
    to_resp_function = struct_serializer.Function(to_resp_prototype)
    to_resp_begin = '''
struct extension_data data;
char* buf= data.buf;
size_t buf_index = 0;

marshall_prim(buf, &buf_index, result);

data.bufc = buf_index;
return data;
'''
    to_resp_function.AddCLines(to_resp_begin)

    from_resp_name = 'ext_%s_from_resp' % (function.GetName())
    from_resp_prototype = 'int %s(struct extension_data data)' % (
        from_resp_name)
    from_resp_function = struct_serializer.Function(from_resp_prototype)
    from_resp_begin = '''
int result;

char* buf = data.buf;
size_t buf_index = 0;

unmarshall_prim(buf, &buf_index, result);
return result;
'''

    from_resp_function.AddCLines(from_resp_begin)

    #main parent interaction with marshalling
    ext_name = 'ext_%s' % (function.GetName())
    ext_prototype = 'struct extension_data ext_%s(struct extension_data data)' % (function.GetName())
    ext_function = struct_serializer.Function(ext_prototype)


    # generating the cleanup function along with ext_, as this is where leaks happen
    cleanup_prototype = None
    for arg in args_split_string:
        if 'Prim' in arg or 'StructWithString' in arg or 'Node' in arg:
            cleanup_prototype = 'void clean_up(%s)' % (arg)
            cleanup_function = struct_serializer.Function(cleanup_prototype)
            cleanup_name = 'clean_up'
            break

    #generate local variables to be parsed
    #TODO what if no name
    for arg in args_split_string:
            ext_function.AddLine(arg + ';');

    #call original function
    #gather arguments, each called with address of the variable
    ext_argument_pointers = []
    for arg_node in node.get_arguments():
            arg_name = arg_node.spelling
            ext_argument_pointers.append('&' + arg_name)

    ext_argument_pointer = ', '.join(ext_argument_pointers)
    ext_unmarshall_arg = 'ext_%s_from_arg(data, %s);' % (function.GetName(), ext_argument_pointer)
    ext_function.AddLine(ext_unmarshall_arg)

    #return value
    #TODO get return value
    ext_arguments = []
    for arg_node in node.get_arguments():
            arg_name = arg_node.spelling
            ext_arguments.append(arg_name)
    ext_argument = ', '.join(ext_arguments)

    return_var = 'return_value'
    return_var_decl = function.GetReturnType() + ' ' + return_var + ';'
    ext_call_real_function = '%s = %s(%s);' % ( return_var, function.GetName(), ext_argument)
    ext_call_to_resp = 'struct extension_data result = ext_%s_to_resp( %s );' % (function.GetName(), return_var )
    ext_return_result = 'return result;'

    ext_function.AddLine(return_var_decl)
    ext_function.AddLine(ext_call_real_function)
    ext_function.AddLine(ext_call_to_resp)

    # adding clean up line into the ext_ function
    if cleanup_prototype != None:
        for i in ext_arguments:
            ext_cleanup = 'clean_up(%s);' % (i)
            ext_function.AddLine(ext_cleanup)

    ext_function.AddLine(ext_return_result)

    already_parsed_structs = []

    #generate the marshall calls in the functions
    for arg in node.get_arguments():
        arg_type = arg.get_definition().type.spelling  #arg.type.spelling   #gives the type of the argument e.g. TypedefPrimitiveStruct *
        arg_name = arg.spelling  #gives the name of the argument e.g. s

        base_arg_node = GetBaseNode(arg)

        #generate marshall call

        #get underlying type (remove pointers)
        #TODO remove qualifiers
        base_type_no_space = 'prim'
        if struct_helper.VerifyIfStructNode(base_arg_node):
            base_type = arg_type.replace('*', '')
            #remove space
            if base_type[-1] == ' ':
                base_type = base_type[:-1]
            base_type_no_space = base_type.replace(' ', '_')
            base_type_remove_struct = base_type.replace(
                'struct ', '')  #e.g. TypedefPrimitiveStruct OR PrimitiveStruct
            log('\nstructs: ')
            log(structs)

            #marshall struct args
            if not base_type_remove_struct in already_parsed_structs:
                already_parsed_structs.append(base_type_remove_struct)
                struct = structs[base_type_remove_struct]

                new_generated_structs, p_paths = struct_serializer.GenerateC(
                    structs, struct)
                generate_functions.update(new_generated_structs)

            #base_type_no_space = base_type_no_space.replace('struct_', '')

            deference_type = arg_name
            # check if the argument is passed by value
            # instead take the address of the object
            if arg_type.count('*') == 0:
                deference_type = '&' + deference_type
            marshall_call = 'marshall_%s(buf, &buf_index, %s);' % (
                base_type_no_space, deference_type
            )  # marshall_TypedefPrimitiveStruct(buf, &buf_index, s);

            deference_type = arg_name
            # check if the argument is passed by value
            # instead don't reference the pointer (because it will become passing the struct by value)
            if arg_type.count('*') != 0:
                deference_type = '*' + deference_type
            unmarshall_call = 'unmarshall_%s(buf, &buf_index, %s);' % (
                base_type_no_space, deference_type)

            to_arg_function.AddLine(marshall_call)
            from_arg_function.AddLine(unmarshall_call)

        elif arg_type.count('*') == 0:
            deference_type = arg_name
            marshall_call = 'marshall_%s(buf, &buf_index, %s);' % (
                base_type_no_space, deference_type
            )  # marshall_TypedefPrimitiveStruct(buf, &buf_index, s);

            deference_type = '*' + arg_name
            unmarshall_call = 'unmarshall_%s(buf, &buf_index, %s);' % (
                base_type_no_space, deference_type)

            to_arg_function.AddLine(marshall_call)
            from_arg_function.AddLine(unmarshall_call)
        else:
            GeneratePrimitivePointerMarshallCode(arg_type, arg_name,
                                                 to_arg_function,
                                                 from_arg_function)

    if cleanup_prototype != None:
        have_ds = False
        ref_path = str(ext_arguments[0])
        for i in p_paths:
            if len(i) == 0:
                continue
            # detect a data structure, and generate code to clean it up i.e. ['^ 1', mtype, name]:
            elif '^' in i[0]:
                have_ds = True
                ds_ident = i[0].split(' ')
                # linked lists (singular/circular/doubly):
                # 1: singular, 2: circular: 3: doubly
                if ds_ident[1] == '1' or ds_ident[1] == '2' or ds_ident[
                        1] == '3':
                    ident = i.pop(0)
                    mtype = i.pop(0)
                    ll_cleaner_proto = 'void ll_cleaner(%s)' % (str(mtype) +
                                                                'head')
                    ll_cleaner_function = struct_serializer.Function(
                        ll_cleaner_proto)
                    ll_cleaner_name = 'll_cleaner'

                    generate_ll_cleaner(ident, i, mtype, ll_cleaner_function)
                    generate_functions[ll_cleaner_name] = ll_cleaner_function

                    ref_path = '->'.join(i[:-1])
                    if ref_path:
                        ref_path = ext_arguments[0] + '->' + ref_path
                    else:
                        ref_path = ext_arguments[0]

                    cleanup_function.AddLine('ll_cleaner(%s);' % (ref_path))
                    continue

                # TODO: binary tree:
                elif ds_ident[1] == '4':
                    ident = i.pop(0)
                    mtype = i.pop(0)

                    member_1 = i.pop(-1)
                    member_2 = i.pop(-1)

                    bt_cleaner_proto = 'void bt_cleaner(%s)' % (str(mtype) +
                                                                'leaf')
                    bt_cleaner_function = struct_serializer.Function(
                        bt_cleaner_proto)
                    bt_cleaner_name = 'bt_cleaner'

                    generate_bt_cleaner(i, mtype, member_1, member_2,
                                        bt_cleaner_function)
                    generate_functions[bt_cleaner_name] = bt_cleaner_function

                    ref_path = '->'.join(i)
                    if ref_path:
                        ref_path = ext_arguments[0] + '->' + ref_path
                    else:
                        ref_path = ext_arguments[0]

                    cleanup_function.AddLine('bt_cleaner(%s);' % (ref_path))
                    continue

            sublv_path = '->'.join(i)
            cleanup_function.AddLine('free(%s->%s);' %
                                     (ext_arguments[0], sublv_path))
        if not have_ds or len(ref_path.split('->')) > 1:
            cleanup_function.AddLine('if (%s != NULL) free(%s);' %
                                     (ext_arguments[0], ext_arguments[0]))
        generate_functions[cleanup_name] = cleanup_function

    to_arg_function.AddCLines(to_arg_end)

    generate_functions[to_arg_name] = to_arg_function
    generate_functions[from_arg_name] = from_arg_function

    if not config['exclude_to_resp']:
        generate_functions[to_resp_name] = to_resp_function
    if not config['exclude_from_resp']:
        generate_functions[from_resp_name] = from_resp_function
    if not config['exclude_ext_name']:
        generate_functions[ext_name] = ext_function

    return generate_functions

default_config = {
    'exclude_to_resp': False,
    'exclude_from_resp': False,
    'exclude_ext_name': False,
    'to_arg_name': None,
    'from_arg_name': None,
    'include_boilerplate': False
}


# Returns two things: (header_file, interface_code)
def RunSerializer(files, config=default_config):
    functions = SerializeFunctions(files, config)

    function = SelectFunction(functions)
    #get all structs in files
    structs = struct_serializer.SerializeStructs(files)
    generated_functions = GenerateCFunction(config, function, structs)

    output = ''
    header = ''

    # The below line is made optional. It can now also be called from setup.py
    # The reason is when we have multiple functions to generate interface.c from,
    # then, we won't want this to be included everytime.
    # see beep_compartment/test/setup.py
    if config['include_boilerplate']:
        output += struct_serializer.c_boiler_plate

    #print prototype
    for name, f in generated_functions.items():
        header += '\n' + f.Generate().split('\n')[0] + ';\n'

    for name, f in generated_functions.items():
        if len(f.extra) > 0:
            header += '\n' + f.extra.replace('\n', '') + '\n'

    #take out unmarshall* calls
    for name, f in generated_functions.items():
        print name
        if 'struct ' in name:
            output += f.Generate()
            del generated_functions[name]

    #take out marshall* calls
    for name, f in generated_functions.items():
        if 'unstruct ' in name:
            output += f.Generate()
            del generated_functions[name]

    #take out marshall/unmarshall calls for typedef functions
    #if the name doesn't contain 'ext', then its for marshalling/unmarshalling struct
    for name, f in generated_functions.items():
        if 'ext' not in name:
            output += f.Generate()
            del generated_functions[name]

    for name, f in generated_functions.items():
        output += f.Generate()

    return (header, output)


if __name__ == '__main__':
    clang.cindex.Config.set_library_file(
        '/usr/lib/x86_64-linux-gnu/libclang-3.8.so.1')

    parser = argparse.ArgumentParser()

    parser.add_argument('files', nargs='+')
    parser.add_argument('exclude_to_resp', type=bool, default=False)
    parser.add_argument('exclude_from_resp', type=bool, default=False)
    parser.add_argument('exclude_ext_name', type=bool, default=False)
    parser.add_argument('to_arg_name', default=None)
    parser.add_argument('from_arg_name', default=None)
    parser.add_argument('include_boilerplate', type=bool, default=False)
    args = parser.parse_args()

    config = {
        'exclude_to_resp': args.exclude_to_resp,
        'exclude_from_resp': args.exclude_from_resp,
        'exclude_ext_name': args.exclude_ext_name,
        'to_arg_name': args.to_arg_name,
        'from_arg_name': args.from_arg_name,
        'include_boilerplate': args.include_boilerplate
    }

    functions = SerializeFunctions(args.files, config)

    pp = pprint.PrettyPrinter(indent=2)
    for nm, f in functions.items():
        pp.pprint([nm, f, f.GetNode().extent])

    function = SelectFunction(functions)
    #get all structs in files
    structs = struct_serializer.SerializeStructs(args.files)
    generated_functions = GenerateCFunction(config, function, structs)

    #print struct_serializer.c_boiler_plate

    #print prototype
    #for name, f in generated_functions.items():
    #   print '\n' + f.Generate().split('\n')[0] + ';\n'

    #for name, f in generated_functions.items():
    #   print f.Generate()
