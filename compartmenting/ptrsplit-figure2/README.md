# Format String Vulnerability
From figure-2 of PtrSplit paper.
## Vulnerability
In `toy-vuln.c` line 15, `printf(str);` is used instead of the safer `printf("%s", str)`, exposing a format string vulnerability. 
* Attackers can examine content of the stack with carefully designed format string input. 
  * For example: `str = "%5$s"` can expose the 5th element on the program's stack.
* In our toy program's case, attacker can use malicious string input to examine the `key` that encrypts `plaintext`.

## Compile and exploit
1. Compile the executables with `make`, and you will see two programs: `vuln` and `patched`.
   * If you just want the vulnerable one, use `make vuln`
2. Run the vulnerable with `./vuln`
3. When prompted to "Enter username", type in `%13$s`, and hit enter.
4. You should see the output: "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", which is the value of the `key`.

## Libcompart Patch
We use libcompart to contain the value `key`, so that the initialization and usage of `key` variable is always in the privileged compartment. In other words, the true value of the `key` is never initialized in the unpriviledged process. To test this patch:
1. Run the patched version of the program with `sudo ./patched`. (To drop privilege, we need to run as root.)
2. When prompted to "Enter username", type in `%13$s`, and hit enter.
3. You should see the output: "(null)".
  
## Expected Output
##### The Unpatched Version:
```
vagrant@ubuntu-xenial:/chopchop/compartmenting/ptrsplit-figure2$ ./vuln
Enter username: %13$s
aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
, welcome!
Enter plaintext: john
Cipher text: b e 9 f 6b 
```

##### The Patched Version:
```
vagrant@ubuntu-xenial:/chopchop/compartmenting/ptrsplit-figure2$ sudo ./patched
<42> initialising
<11> starting ptrsplit
<44> ptrsplit to priv
<45> (monitor) call to priv
<43> starting sub priv
<46> (monitor) return from priv
Enter username: %13$s
(null)
, welcome!
Enter plaintext: john
<44> ptrsplit to priv
<45> (monitor) call to priv
<46> (monitor) return from priv
Cipher text: b e 9 f 6b 
<50> terminated: ptrsplit
<38> communication break: between (monitor) and ptrsplit
<50> terminated: priv
<37> all children dead
```
