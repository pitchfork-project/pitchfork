#include "compart_api.h"
#include "combin.h"
#define NO_COMPARTS 2

extern struct compart comparts[NO_COMPARTS];
struct extension_id * init_key_ext;
struct extension_id * encrypt_ext;

/**************************
 * MARK: Interface used for toy
 * ******/ 

/* This runs on priv, patching `void initkey (int sz)` */
struct extension_data ext_init_key(struct extension_data data);

/* This is called by compartment `toy` */
struct extension_data ext_init_key_to_arg(int sz);

/* This runs on priv, patching `encrypt (char * plaintext, int sz)` */
struct extension_data ext_encrypt(struct extension_data data);

/* This is called by compartment `toy` */
struct extension_data ext_encrypt_to_arg(char * plaintext, int sz);

void exp_encrpt_from_resp(struct extension_data data);
