```
$ make -C ../libcompart/ clean
...
$ make -C ../libcompart/
...
$ make
...
$ sudo ./hello_compartment
<11> starting other compartment
<11> starting hello compartment
started other compartment
Old value: -5
After adding ten to old value
New value: 5
<36> ending
<38> pipe broken
```
