### Libcompart Patch for Varnish
+ Reference:
  + https://varnish-cache.org/security/VSV00002.html
  + https://github.com/varnishcache/varnish-cache/pull/2429
  + https://docs.varnish-software.com/security/VSV00002/
+ Vulnerability:
  + A wrong `if` statement in the varnishd source code means that when copying data into an object, the size of object may be larger than the size of data to be copied. (e.g. when data is only 10 bytes, the target object is 30 bytes, then varnish will intend to copy 20 bytes data). Thus, it may incur segmentation fault or data leakage.
+ What the patch does:
  + The unprivileged process marshalls safe content of `VSB_data(synth_body)` to the privileged process.
  + The privileged process returns safe content and correct length of copy to the unprivileged process.

### How to Exploit
  + run `./install.sh -unpatch` (automatic) 
  + output obtained (part of the full output):
    ```
    **** s1    0.5 Accepting
    **** c1    0.5 rxhdr| HTTP/1.1 503 Backend fetch failed\r\n
    **** c1    0.5 rxhdr| Date: Thu, 25 Jul 2019 02:12:32 GMT\r\n
    **** c1    0.5 rxhdr| Server: Varnish\r\n
    **** c1    0.5 rxhdr| X-Varnish: 1001\r\n
    **** c1    0.5 rxhdr| Age: 0\r\n
    **** c1    0.5 rxhdr| Via: 1.1 varnish (Varnish/5.0)\r\n
    **** c1    0.5 rxhdr| Content-Length: 4096\r\n
    **** c1    0.5 rxhdr| Connection: keep-alive\r\n
    **** c1    0.5 rxhdr| \r\n
    **** c1    0.5 rxhdrlen = 194
    **** c1    0.5 http[ 0] | HTTP/1.1
    **** c1    0.5 http[ 1] | 503
    **** c1    0.5 http[ 2] | Backend fetch failed
    **** c1    0.5 http[ 3] | Date: Thu, 25 Jul 2019 02:12:32 GMT
    **** c1    0.5 http[ 4] | Server: Varnish
    **** c1    0.5 http[ 5] | X-Varnish: 1001
    **** c1    0.5 http[ 6] | Age: 0
    **** c1    0.5 http[ 7] | Via: 1.1 varnish (Varnish/5.0)
    **** c1    0.5 http[ 8] | Content-Length: 4096
    **** c1    0.5 http[ 9] | Connection: keep-alive
    **** c1    0.5 body| foo\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x95\x00\x00\x00\x00\x00\x00\x00\xa0\xa8'\t\x00\x00\x00\x00\xc0QN\x1a\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\xa0\t\x00t`\x7f\x00\x00\x00\x00\x03\xa0`\x7f\x00\x00\xd8\x00\x00\x00\x00\x10\x00\x00`\xa8>\x02\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x10\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x03\xa0`\x7f\x00\x000\n
    **** c1    0.5 body| \x00t`\x7f\x00\x00\x98\xa8>\x02\x00\x00\x00\x000\n
    **** c1    0.5 body| \x00t`\x7f\x00\x00\xb8\xaa>\x02\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x95\x00\x00\x00\x00\x00\x00\x00\xa0\xa8'\t\x00\x00\x00\x00\xc0QN\x1a\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00h\x00\x03\xa0`\x7f\x00\x000\n
    **** c1    0.5 body| \x00t`\x7f\x00\x00\x00\x10\x03\xa0`\x7f\x00\x00\x00\x00\x00\x00\x00\x10\x00\x00`\xa8>\x02\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x10\x00\x00\x00\x00\x00\x00\x00\x10\x00\x00\x00\x00\x00\x00\x00\x10\x03\xa0`\x7f\x00\x00 <@\x02\x00\x00\x00\x00\x00\n
    **** c1    0.5 body| \x00t`\x7f\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x10\n
    **** c1    0.5 body| \x00t`\x7f\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00Q\x05\x02\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00`b\xda\xa5`\x7f\x00\x00\x91\x04\x02\x00\x00\x00\x00\x00TZif2\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x01\x00\x00\x00\x04\xf8\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00UTC\x00\x00\x00\n
    **** c1    0.5 body| UTC0\n
    **** c1    0.5 body| \x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x01\x00\x00\x00\x04\xf8\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00UTC\x00\x00\x00\n
    **** c1    0.5 body| UTC0\n
    **** c1    0.5 body| \x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00...
    **** c1    0.5 bodylen = 4096
    **   c1    0.5 === expect resp.status == 503
    **** c1    0.5 EXPECT resp.status (503) == "503" match
    **   c1    0.5 === expect resp.http.content-length == 3
    ---- c1    0.5 EXPECT resp.http.content-length (4096) == "3" failed
    *    top   0.5 RESETTING after ../../../test.vtc
    **   s1    0.5 Waiting for server (4/-1)
    **** s1    0.5 macro undef s1_addr
    **** s1    0.5 macro undef s1_port
    **** s1    0.5 macro undef s1_sock
    **   v1    0.5 Wait
    ```
  + As shown above, the blank bytes (like "foo\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x95\x00\x00\x00\x00\x00\x00\x00\xa0\xa8'\t") indicates the vulnerability of this version of varnish
  + That is, the blank bytes is overvisit of server's memory by the client and may contain some sensitive information so that malicious user may visit the information that they are not supposed to see.

  
### How to Apply the Patch
  + run command `./install.sh`
  + then run `sudo -s` to get the sudo access (to apply the libcompart patch, root access is required)
  + run `export PATH=$PATH:$(pwd)/sbin`
  + run `./varnishtest test.vtc`
  + Expected Output:
    ```
    **   c1    0.4 Starting client
    **   c1    0.4 Waiting for client
    ***  c1    0.4 Connect to 127.0.0.1 42358
    ***  c1    0.4 connected fd 7 from 127.0.0.1 34912 to 127.0.0.1 42358
    **   c1    0.4 === txreq
    **** c1    0.4 txreq| GET / HTTP/1.1\r\n
    **** c1    0.4 txreq| \r\n
    **   c1    0.4 === rxresp
    ***  s1    0.4 accepted fd 5
    **   s1    0.4 === accept
    **** s1    0.4 Accepting
    ***  v1    0.4 debug| Info: Child (16090) said <11> starting varnish_libcompart\n
    ---- c1   30.5 HTTP rx timeout (fd:7 30000 ms)
    *    top  30.5 RESETTING after ../../../test.vtc
    **   s1   30.5 Waiting for server (4/-1)
    **** s1   30.5 macro undef s1_addr
    **** s1   30.5 macro undef s1_port
    **** s1   30.5 macro undef s1_sock
    **   v1   30.5 Wait
    **** v1   30.5 CLI TX| backend.list
    ***  v1   30.5 CLI RX  200
    **** v1   30.5 CLI RX| Backend name                   Admin      Probe                Last updated\n
    **** v1   30.5 CLI RX| vcl1.s1                        probe      Healthy (no probe)   Tue, 06 Aug 2019 10:31:42 GMT
    ***  v1   30.5 debug| Debug: Stopping Child\n
    ***  v1   31.5 debug| Info: Child (16090) ended\n
    ***  v1   31.5 debug| Info: Child (16090) said Child dies\n
    ***  v1   31.5 debug| Debug: Child cleanup complete\n
    **** v1   31.5 STDOUT poll 0x10
    **   v1   31.5 R 16078 Status: 0000 (u 0.060000 s 0.060000)
    *    top  31.5 TEST ../../../test.vtc FAILED
    ```
  + No memory content is leaked.

### Reproduction
  + Ubuntu 16.04.5 LTS
  + varnish-5.0.0
