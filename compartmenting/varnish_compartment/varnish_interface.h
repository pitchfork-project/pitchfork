#include "compart_api.h"
#define NO_COMPARTS 2
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include "signal.h"
#include <stdlib.h>
#include <sys/mman.h>
#include <errno.h>
#include <stdio.h>
#include <sys/wait.h>
#define KEY 5678

extern struct compart comparts[NO_COMPARTS];
struct extension_id * memcpy_ext;

/**************************
 * MARK: Interface used for cache_fetch
 * ******/ 

/* This runs on priv, patching memcpy(dest, src, len) */
struct extension_data ext_memcpy(struct extension_data data);

/* This is called by compartment "varnish" */
struct extension_data ext_memcpy_to_arg(void * dst, char* src, ssize_t l);

/* This does the memcpy in compartment "varnish" */
void ext_memcpy_from_resp(struct extension_data data);