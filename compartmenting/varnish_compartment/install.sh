#!/bin/bash
set -e

# make compart
make -C ../libcompart

# download
wget https://github.com/varnishcache/varnish-cache/archive/varnish-5.0.0.tar.gz
tar xzvf varnish-5.0.0.tar.gz
cd varnish-cache-varnish-5.0.0
./autogen.sh
./configure --prefix=$(pwd)/varnish-build/

# patch
patch=true
while [ -n "$1" ]
do 
    case "$1" in -unpatch)
      patch=false
    esac
    shift
done

if [ "$patch" = true ]
then
  cp ../varnish_interface.c bin/varnishd/cache/
  cp ../varnish_interface.h bin/varnishd/cache/
  patch bin/varnishd/cache/cache_fetch.c < ../cache_fetch.patch
  patch bin/varnishd/Makefile < ../Makefile.patch
fi

# make and install
make
make install

echo "to run the varnish, sudo access is required"
echo "run 'sudo -s' then:"
echo "run \"cd varnish-cache-varnish-5.0.0/varnish-build\""
echo "run \"export PATH=\$PATH:\$(pwd)/sbin\""
echo "run \"cd bin\""
echo "run \"./varnishtest ../../../test.vtc\""
echo "do not forget to exit the sudo access"
echo "===== Expected output ====="
echo "the expected output is a memory overflow error"
