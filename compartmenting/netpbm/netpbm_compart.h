#include "compart_api.h"
#ifdef LC_SEP
#include "combin_tcp.h"
#endif // LC_SEP

#define NO_COMPARTS 3
#ifdef LC_SEP
struct combin combins[NO_COMPARTS] =
  {{.address = "192.168.122.98", .port = 14910},
   {.address = "127.0.0.1", .port = 14910}};
#endif // LC_SEP

static struct compart comparts[NO_COMPARTS] = {
#ifdef LC_SEP
  {.name = "libtiff", .uid = 65534, .gid = 65534, .path = "/", .comms = &combins[0]},
  {.name = "cmdline_parse", .uid = 65534, .gid = 65534, .path = "/", .comms = &combins[1]},
  {.name = "netpbm", .uid = 65534, .gid = 65534, .path = "/", .comms = NULL}
#else
  {.name = "libtiff", .uid = 65534, .gid = 65534, .path = "/"},
  {.name = "cmdline_parse", .uid = 65534, .gid = 65534, .path = "/var/empty/"},
  {.name = "netpbm", .uid = 65534, .gid = 65534, .path = "/"}
#endif // LC_SEP
};

struct extension_id *convertTIFF_ext;
struct extension_data ext_convertTIFF(struct extension_data data);

struct extension_id *parseCommandLine_ext;
struct extension_data ext_parseCommandLine(struct extension_data data);
