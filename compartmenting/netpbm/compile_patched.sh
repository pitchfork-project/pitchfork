#!/bin/sh
set -e

if [ -z $CHOPCHOP_DIR ]
then
  CHOPCHOP_DIR=/home/nik/t/chopchop/
fi

echo "CHOPCHOP_DIR=${CHOPCHOP_DIR}"

cd ${CHOPCHOP_DIR}/compartmenting/netpbm/netpbm-10.73.28/converter/other/

# Might want to add -DDEMARSH_DEBUG
cc -c -DLIBCOMPART -DINCLUDE_PID -I${CHOPCHOP_DIR}/demarshalling/ -I${CHOPCHOP_DIR}/compartmenting/libcompart -I${CHOPCHOP_DIR}/compartmenting/im/build/include/ -I.  -Iimportinc -Iimportinc/netpbm -I${CHOPCHOP_DIR}/compartmenting/netpbm/netpbm-10.73.28/converter/other -I/usr/include/libpng16 -I${CHOPCHOP_DIR}/compartmenting/netpbm/netpbm-10.73.28/urt -DNDEBUG     -O3 -ffast-math  -pedantic -fno-common -Wall -Wno-uninitialized -Wmissing-declarations -Wimplicit -Wwrite-strings -Wmissing-prototypes -Wundef -Wno-unknown-pragmas  -o tifftopnm.o tifftopnm.c
cc -o tifftopnm tifftopnm.o tiff.o -ltiff -ljpeg -lz -L${CHOPCHOP_DIR}/compartmenting/netpbm/netpbm-10.73.28/lib/ -lnetpbm -lm -L${CHOPCHOP_DIR}/compartmenting/libcompart -lcompart

cp tifftopnm ${CHOPCHOP_DIR}/compartmenting/netpbm/tifftopnm_libcompart
