**NOTE** Scripts contain some hardcoded path defaults.

Compile using:
```
sh install.sh
```
Then run both the original and modified versions using:
```
sh run_test.sh -v -c -e /home/nik/t/chopchop/compartmenting/netpbm/tifftopnm_original
sh run_test.sh -v -c -e /home/nik/t/chopchop/compartmenting/netpbm/tifftopnm_libcompart
```
Difference in state between the two because compartmentalised version
doesn't synch state initialised by calling pm_init (from lib/libpm.c)
which in turn calls pm_setMessage() and sets pm_progname, leading to
difference in console output (but not in image conversion).

For separate binaries compile with -DLC_SEP to get the compart_netpbm
binary (renaming is manual at the moment), and further specify
LC_CMD to get compart_cmd or LC_TIFF to get compart_libtiff.
