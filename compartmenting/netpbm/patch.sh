#!/bin/sh
set -e

cp netpbm_compart.c netpbm_compart.h netpbm-10.73.28/converter/other/
patch netpbm-10.73.28/converter/other/tifftopnm.c tifftopnm.c_patch
sh compile_patched.sh
