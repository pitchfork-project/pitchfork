#!/bin/sh
# First parameter is either "original" or "libcompart"
# Second parameter is either "small" or "large"

#tifftopnm_libcompart
#tifftopnm_original
EXEC="tifftopnm_${1}"

PREFIX=/home/nik/t/chopchop/compartmenting
FROM="${PREFIX}/netpbm/test_${2}.tiff"
TO=~/test.pnm

sudo sh -c "LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${PREFIX}/netpbm/netpbm-10.73.28/lib/:${PREFIX}/libcompart ${EXEC} --quiet ${FROM} > ${TO}"

#CHECK="${TO}.png"
#LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${PREFIX}/netpbm/netpbm-10.73.28/lib/ {PREFIX}/netpbm/netpbm-10.73.28/converter/other/pnmtopng ${TO} > ${CHECK}
#ls -alh ${TO} ${CHECK}
